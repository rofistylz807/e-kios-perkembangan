@extends('component.dashboard.main')
@section('content')
<div class="card">
    <div class="card-header">
        Kontak
    </div>
    <div class="card-body">
        <form method="post" action="{{url('contact/update')}}" onsubmit="return validate('contact/validate')">
            @csrf
            <div class="form-group">
                <label>Email:</label>
                <input type="email" class="form-control" name="email" placeholder="Email" value="{{$data->email}}">
                <div class="alert alert-light fade show p-1" id="email" role="alert"></div>
            </div>
            <div class="form-group">
                <label>Telepon:</label>
                <input type="text" class="form-control"  placeholder="Telepon" name="telepon" value="{{$data->telepon}}">
                <div class="alert alert-light fade show p-1" id="telepon" role="alert"></div>
            </div>
            <div class="form-group">
                <label>Alamat:</label>
                <textarea class="form-control" placeholder="Alamat" name="alamat">{{$data->alamat}}</textarea>
                <div class="alert alert-light fade show p-1" id="alamat" role="alert"></div>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-success" value="Ubah">
            </div>
        </form>
    </div>
</div>
@endsection
