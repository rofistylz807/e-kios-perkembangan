@extends('component.dashboard.main')
@section('content')
<div class="card">
    <div class="card-header">Format Surat</div>
    <div class="card-body">
        <a class="btn btn-primary mb-2" href="{{url('formats/create')}}"><i class="bx bx-plus"></i></a>
        <div class="table-responsive">
            <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th width="80%">Nama</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($format as $key=>$value)
                    <tr>
                        <td>{{$value->name}}</td>
                        <td>
                            <a class="btn btn-success" href="{{url('formats/edit/'.Crypt::encryptString($value->id))}}"><i class="bx bx-edit"></i></a>
                            <button class="btn btn-danger" data-toggle="modal" data-target="#format-delete"><i class="bx bx-trash" onclick="del('{{$value->name}}','formats/del/{{Crypt::encryptString($value->id)}}')"></i></button>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal" id="format-delete" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog"
    aria-labelledby="ExtraLargeModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Hapus Format</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="clsdel()"> <span
                        aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Apakah anda yakin ingin menghapus <span class="del-title"></span>
            </div>
            <div class="modal-footer">
                <a class="btn btn-success del-url">Delete</a>
            
                <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="clsdel()">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection
