@if($code!='sign')
<tr id="{{$id}}">
    <td>
        <input type="text" class="form-control" name="{{$code}}[{{$id}}][variable]" required>
    </td>
    <td>
        <input type="text" class="form-control" name="{{$code}}[{{$id}}][name]" required>
    </td>
    <td>
        <select class="form-control" name="{{$code}}[{{$id}}][type]" required onchange="selectOpt(this,'{{$id}}')">
            <option value=""></option>
            <option value="text">Teks</option>
            <option value="number">Angka</option>
            <option value="select">Pilihan</option>
            <option value="textarea">Paragraf</option>
            <option value="date">Tanggal</option>
            <option value="file">File</option>
            @if($code=='response')
            <option value="name">Nama yang Bertandatangan</option>
            <option value="nip">NIP yang Bertandatangan</option>
            @endif
        </select>
    </td>
    <td>
        <input type="text" id="option_{{$id}}" class="form-control" placeholder="contoh: Laki-laki|Perempuan" name="{{$code}}[{{$id}}][option]" disabled>
    </td>
    <td>
        <input type="text" id="validate_{{$id}}" class="form-control" placeholder="contoh: required" name="{{$code}}[{{$id}}][validate]" disabled>
    </td>
    <td>
        <button class="btn btn-danger btn-sm" type="button" onclick="delform('{{$id}}')"><i class="bx bx-trash"></i></button>
    </td>
</tr>
@else
@foreach($sign as $kd=> $kv)
<div class="form-group">
    <label>{{strtoupper($kd)}}</label>
    <select class="form-control" name="{{$kd}}">
        <option value=""></option>
        @foreach($skpd as $value)
        <option value="{{$value->skpd_id}}">{{$value->skpd_name}}</option>
        @endforeach
    </select>
</div>
@endforeach
@endif