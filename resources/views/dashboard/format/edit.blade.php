@extends('component.dashboard.main')
@section('content')
<div class="card">
    <div class="card-header">
        Edit Format Surat
    </div>
    <div class="card-body">
        <form action="{{url('formats/update/'.Crypt::encryptString($format->id))}}" method="post" enctype="multipart/form-data" >
            @csrf
            <div class="form-group">
                <label>Nama</label>
                <input class="form-control" type="text" placeholder="Nama" name="name" value="{{$format->name}}">
            </div>
            <label class="mt-2">Form Permohonan</label>
            <div class="form-group">
                <div class="table-responsive">
                    <table class="table text-center">
                        <thead>
                            <tr>
                                <th class="text-center">
                                    Variabel
                                </th>
                                <th class="text-center">
                                    Nama
                                </th>
                                <th class="text-center">
                                    Jenis Form
                                </th>
                                <th class="text-center">
                                    Opsi
                                </th>
                                <th class="text-center">
                                    Validasi
                                </th>
                                <th class="text-center">
                                    <button class="btn btn-primary btn-sm" type="button" onclick="addform('add-form-body',1)"><i class="bx bx-plus"></i></button>
                                </th>
                            </tr>
                        </thead>
                        <tbody id="add-form-body">
                            @php $validate=json_decode($format->data,true)['validate']; @endphp
                            @foreach(json_decode($format->data,true)['form'] as $key=>$value)
                            @php $id=substr(md5(rand()), 0, 16); @endphp
                            <tr id="{{$id}}">
                                <td>
                                    <input type="text" class="form-control" name="request[{{$id}}][variable]" required value="{{$key}}">
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="request[{{$id}}][name]" required value="{{$value['name']}}">
                                </td>
                                <td>
                                    <select class="form-control" name="request[{{$id}}][type]" required onchange="selectOpt(this,'{{$id}}')">
                                        <option value=""></option>
                                        <option value="text" {{$value['field']=="text"?'selected':''}}>Teks</option>
                                        <option value="number" {{$value['field']=="number"?'selected':''}}>Angka</option>
                                        <option value="select" {{$value['field']=="select"?'selected':''}}>Pilihan</option>
                                        <option value="textarea" {{$value['field']=="textarea"?'selected':''}}>Paragraf</option>
                                        <option value="date" {{$value['field']=="date"?'selected':''}}>Tanggal</option>
                                        <option value="file" {{$value['field']=="file"?'selected':''}}>File</option>
                                    </select>
                                </td>
                                @if($value['field']=='select'||$value['field']=='name'||$value['field']=='nip')
                                <td>
                                    <input type="text" id="option_{{$id}}" class="form-control" placeholder="contoh: Laki-laki|Perempuan" name="request[{{$id}}][option]" required value="{{is_array($value['option'])?implode('|',$value['option']):$value['option']}}">
                                </td>
                                @else
                                <td>
                                    <input type="text" id="option_{{$id}}" class="form-control" placeholder="contoh: Laki-laki|Perempuan" name="request[{{$id}}][option]" disabled value="{{is_array($value['option'])?implode('|',$value['option']):$value['option']}}">
                                </td>
                                @endif
                                @if($value['field']!='file')
                                <td>
                                    <input type="text" id="validate_{{$id}}" class="form-control" placeholder="contoh: required" name="request[{{$id}}][validate]" value="{{array_key_exists($key,$validate)?$validate[$key]:""}}">
                                </td>
                                @else
                                <td>
                                    <input type="text" id="validate_{{$id}}" class="form-control" placeholder="contoh: required" name="request[{{$id}}][validate]" disabled value="{{array_key_exists($key,$validate)?$validate[$key]:""}}">
                                </td>
                                @endif
                                
                                <td>
                                    <button class="btn btn-danger btn-sm" type="button" onclick="delform('{{$id}}')"><i class="bx bx-trash"></i></button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <label class="mt-2">Form Tanggapan</label>
            <div class="form-group">
                <div class="table-responsive">
                    <table class="table text-center">
                        <thead>
                            <tr>
                                <th class="text-center">
                                    Variabel
                                </th>
                                <th class="text-center">
                                    Nama
                                </th>
                                <th class="text-center">
                                    Jenis Form
                                </th>
                                <th class="text-center">
                                    Opsi
                                </th>
                                <th class="text-center">
                                    Validasi
                                </th>
                                <th class="text-center">
                                    <button class="btn btn-primary btn-sm" type="button"><i class="bx bx-plus" onclick="addform('add-form-body-1',2)"></i></button>
                                </th>
                            </tr>
                        </thead>
                        <tbody id="add-form-body-1">
                            @php $validate_response=json_decode($format->data,true)['validate_response']; @endphp
                            @foreach(json_decode($format->data,true)['response'] as $key=>$value)
                            @php $id=substr(md5(rand()), 0, 16); @endphp
                            <tr id="{{$id}}">
                                <td>
                                    <input type="text" class="form-control" name="response[{{$id}}][variable]" required value="{{$key}}">
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="response[{{$id}}][name]" required value="{{$value['name']}}">
                                </td>
                                <td>
                                    <select class="form-control" name="response[{{$id}}][type]" required onchange="selectOpt(this,'{{$id}}')">
                                        <option value=""></option>
                                        <option value="text" {{$value['field']=="text"?'selected':''}}>Teks</option>
                                        <option value="number" {{$value['field']=="number"?'selected':''}}>Angka</option>
                                        <option value="select" {{$value['field']=="select"?'selected':''}}>Pilihan</option>
                                        <option value="textarea" {{$value['field']=="textarea"?'selected':''}}>Paragraf</option>
                                        <option value="date" {{$value['field']=="date"?'selected':''}}>Tanggal</option>
                                        <option value="file" {{$value['field']=="file"?'selected':''}}>File</option>
                                        <option value="name" {{$value['field']=="name"?'selected':''}}>Nama yang Bertandatangan</option>
                                        <option value="nip" {{$value['field']=="nip"?'selected':''}}>NIP yang Bertandatangan</option>
                                    </select>
                                </td>
                                @if($value['field']=='select'||$value['field']=='name'||$value['field']=='nip')
                                <td>
                                    <input type="text" id="option_{{$id}}" class="form-control" placeholder="contoh: Laki-laki|Perempuan" name="response[{{$id}}][option]" value="{{is_array($value['option'])?implode('|',$value['option']):$value['option']}}" required>
                                </td>
                                @else
                                <td>
                                    <input type="text" id="option_{{$id}}" class="form-control" placeholder="contoh: Laki-laki|Perempuan" name="response[{{$id}}][option]" disabled value="{{is_array($value['option'])?implode('|',$value['option']):$value['option']}}">
                                </td>
                                @endif
                                @if($value['field']!='file')
                                <td>
                                    <input type="text" id="validate_{{$id}}" class="form-control" placeholder="contoh: required" name="response[{{$id}}][validate]" value="{{array_key_exists($key,$validate_response)?$validate_response[$key]:""}}">
                                </td>
                                @else
                                <td>
                                    <input type="text" id="validate_{{$id}}" class="form-control" placeholder="contoh: required" name="response[{{$id}}][validate]" disabled value="{{array_key_exists($key,$validate_response)?$validate_response[$key]:""}}">
                                </td>
                                @endif
                                <td>
                                    <button class="btn btn-danger btn-sm" type="button" onclick="delform('{{$id}}')"><i class="bx bx-trash"></i></button>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="form-group">
                <label>Surat</label>
                <textarea class="form-control textarea" cols="30" rows="50" name="document">{{$format->document}}</textarea>
            </div>

            <hr>
            <div class="form-group">
                <input class="btn btn-success mt-2" type="submit" value="Simpan">
            </div>
        </form>
    </div>
</div>
@endsection
