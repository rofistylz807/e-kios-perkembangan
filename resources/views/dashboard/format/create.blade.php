@extends('component.dashboard.main')
@section('content')
<div class="card">
    <div class="card-header">
        Tambah Format Surat
    </div>
    <div class="card-body">
        <form action="{{url('formats/store')}}" method="post" enctype="multipart/form-data" >
            @csrf
            <div class="form-group">
                <label>Nama</label>
                <input class="form-control" type="text" placeholder="Nama" name="name">
            </div>
            <label class="mt-2">Form Permohonan</label>
            <div class="form-group">
                <div class="table-responsive">
                    <table class="table text-center">
                        <thead>
                            <tr>
                                <th class="text-center">
                                    Variabel
                                </th>
                                <th class="text-center">
                                    Nama
                                </th>
                                <th class="text-center">
                                    Jenis Form
                                </th>
                                <th class="text-center">
                                    Opsi
                                </th>
                                <th class="text-center">
                                    Validasi
                                </th>
                                <th class="text-center">
                                    <button class="btn btn-primary btn-sm" type="button" onclick="addform('add-form-body',1)"><i class="bx bx-plus"></i></button>
                                </th>
                            </tr>
                        </thead>
                        <tbody id="add-form-body">
                        </tbody>
                    </table>
                </div>
            </div>
            <label class="mt-2">Form Tanggapan</label>
            <div class="form-group">
                <div class="table-responsive">
                    <table class="table text-center">
                        <thead>
                            <tr>
                                <th class="text-center">
                                    Variabel
                                </th>
                                <th class="text-center">
                                    Nama
                                </th>
                                <th class="text-center">
                                    Jenis Form
                                </th>
                                <th class="text-center">
                                    Opsi
                                </th>
                                <th class="text-center">
                                    Validasi
                                </th>
                                <th class="text-center">
                                    <button class="btn btn-primary btn-sm" type="button"><i class="bx bx-plus" onclick="addform('add-form-body-1',2)"></i></button>
                                </th>
                            </tr>
                        </thead>
                        <tbody id="add-form-body-1">
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="form-group">
                <label>Surat</label>
                <textarea class="form-control textarea" cols="30" rows="50" name="document"></textarea>
            </div>

            <hr>
            <div class="form-group">
                <input class="btn btn-success mt-2" type="submit" value="Simpan">
            </div>
        </form>
    </div>
</div>
@endsection
