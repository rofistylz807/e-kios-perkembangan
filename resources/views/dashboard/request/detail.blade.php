<h2>{{$menu->title}}</h2>
<h5>{{$menu->description}}</h5>
<hr>
@if($req->status=="pending"&&$req->sign_id==NULL)
<form name="form" action="{{url('request/form/'.Crypt::encryptString($menu->id))}}"
    onsubmit="return validateFormReq('{{json_encode(json_decode($form->data,true)['validate'])}}')" method="post" enctype="multipart/form-data">
    <input type="hidden" value="{{Crypt::encryptString($req->id)}}" name="x">
    @csrf
    @foreach(json_decode($form->data,true)['form'] as $key=>$value)
    <div class="form-group">
        <label>{{$value['name']}} :</label>
        @if($value['field']=='text')
        <input type="text" name="{{$key}}" class="form-control" value="{{isset($data[$key])?$data[$key]:''}}">
        @elseif($value['field']=='number')
        <input type="number" name="{{$key}}" class="form-control" value="{{isset($data[$key])?$data[$key]:''}}">
        @elseif($value['field']=='file')
        <iframe src="{{url('file/'.$req->id.'/'.$data[$key])}}" frameBorder="0" scrolling="auto" height="500px" width="100%"></iframe>
        <input type="file" name="{{$key}}" class="form-control" value="{{isset($data[$key])?$data[$key]:''}}">
        @elseif($value['field']=='date')
        <input type="date" name="{{$key}}" class="form-control" value="{{isset($data[$key])?$data[$key]:''}}">
        @elseif($value['field']=='select')
        <select class="form-control" name="{{$key}}">
            <option></option>
            @foreach($value['option'] as $sk=>$sv)
            <option {{isset($data[$key])?$data[$key]==$sk?'selected':'':''}} value="{{$sk}}">{{$sv}}</option>
            @endforeach
        </select>
        @elseif($value['field']=='textarea')
        <textarea name="{{$key}}" class="form-control">{{isset($data[$key])?$data[$key]:''}}</textarea>
        @endif
        <div class="alert alert-light fade show p-1" id="{{$key}}" role="alert"></div>
    </div>
    @endforeach
    <div class="form-group mb-5 mt-3">
        <input class="btn btn-success" type="submit" value="Kirim">
    </div>
</form>
@else
<div class="card">
    <div class="card-header">
        @if($req->status=='done')
        <a class="btn btn-dark" target="_blank" href="{{url('pdf/'.Crypt::encryptString($req->id))}}"><i
                class="bx bx-printer"></i></a></div>
    @endif
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-striped">
                @foreach(json_decode($form->data,true)['form'] as $key=>$value)
                <tr>
                    <td width="20%">{{$value['name']}}</td>
                    <td>:</td>
                    <td>
                        @if($value['field']=="date")
                        {{convertDate($data[$key])}}
                        @elseif($value['field']=="file") 
                        <iframe src="{{url('file/'.$req->id.'/'.$data[$key])}}" frameBorder="0" scrolling="auto" height="500px" width="100%"></iframe>
                        @else 
                        {{$data[$key]}}
                        @endif
                    </td>
                </tr>
                @endforeach
            </table>
        </div>
    </div>
</div>
@endif
