@extends('component.dashboard.main')
@section('content')
<div class="card">
    <div class="card-header">Daftar Permohonan</div>
    <div class="card-body">

        <div class="table-responsive">
            <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Permohonan</th>
                        <th>Tanggal</th>
                        <th>Status</th>
                        <th>Tanggapan</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($req as $key=>$value)
                    <tr>
                        <td>{{$value->mdesc}}</td>
                        <td>{{convertDate($value->created_at)}}</td>
                        @if($value->status=="pending")
                        <td><span class="badge badge-secondary">Pending</span></td>
                        @elseif($value->status=="done")
                        <td><span class="badge badge-success">Selesai</span></td>
                        @elseif($value->status=="reject")
                        <td><span class="badge badge-danger">Ditolak</span></td>
                        @endif
                        <td>{{$value->description}}</td>
                        @if($value->survey=='0'&&$value->sign_id!='NULL'&&$value->status=='done')
                        <td><button class="btn btn-primary " data-toggle="modal" onclick="detailSurvey('{{url('survey/store/'.$value->id)}}')" data-target="#createsurvey"><i
                                    class="bx bx-message-alt-edit"></i></button></td>
                        @else
                        <td><button class="btn btn-success" data-toggle="modal"
                                onclick="detail('request/detail/{{$value->id}}/{{$value->request}}','req-detail-body')"
                                data-target="#reqDetail"><i class="bx bx-search"></i></button></td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal" id="reqDetail" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog"
    aria-labelledby="ExtraLargeModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detail Permohonan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                    onclick="cls('req-detail-body')"> <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body req-detail-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" onclick="cls('req-detail-body')"
                    data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="createsurvey" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog"
    aria-labelledby="ExtraLargeModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="ExtraLargeModelLabel">Silahkan Lakukan Pengisian Survey</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span
                        aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="survey-form" action="{{url('survey/store')}}" enctype="multipart/form-data" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <label>Jenis Kelamin : </label>
                        <select class="form-control" name="jenis_kelamin" required>
                            <option value=""></option>
                            <option value="L">Laki-Laki</option>
                            <option value="P">Perempuan</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Pendidikan :</label>
                        <select class="form-control" name="pendidikan" required>
                            <option value=""></option>
                            <option value="SD">SD</option>
                            <option value="SMP">SMP</option>
                            <option value="SMA">SMA</option>
                            <option value="S1">S1</option>
                            <option value="S2">S2</option>
                            <option value="S3">S3</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Pekerjaan :</label>
                        <select class="form-control" name="pekerjaan" required>
                            <option value=""></option>
                            <option value="PNS">PNS</option>
                            <option value="TNI">TNI</option>
                            <option value="POLRI">POLRI</option>
                            <option value="SWASTA">SWASTA</option>
                            <option value="WIRAUSAHA">WIRAUSAHA</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Jenis Layanan Yang Di Terima :</label>
                        <input type="text" class="form-control" name="jenis_layanan"
                            placeholder="Masukkan Layanan Yang Diterima" required />
                    </div>
                    <hr>
                    <div class="form-group">
                        <label>1. Bagaimana pendapat saudara tentang kesesuaian persyaratan
                            pelayanan dengan jenis pelayanannya.</label>
                        <p><input type="radio" name="ans1" value="1" /> Tidak Sesuai</p>
                        <p><input type="radio" name="ans1" value="2" /> Kurang Sesuai</p>
                        <p><input type="radio" name="ans1" value="3" /> Sesuai</p>
                        <p><input type="radio" name="ans1" value="4" /> Sangat Sesuai</p>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label>2. Bagaimana pemahaman Saudara tentang kemudahan
                            prosedur pelayanan di unit ini.</label>
                        <p><input type="radio" name="ans2" value="1" /> Tidak Mudah</p>
                        <p><input type="radio" name="ans2" value="2" /> Kurang Mudah</p>
                        <p><input type="radio" name="ans2" value="3" /> Mudah</p>
                        <p><input type="radio" name="ans2" value="4" /> Sangat Mudah</p>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label>3. Bagaimana pendapat Saudara tentang kecepatan
                            waktu dalam memberikan pelayanan..</label>
                        <p><input type="radio" name="ans3" value="1" /> Tidak Cepat</p>
                        <p><input type="radio" name="ans3" value="2" /> Kurang Cepat</p>
                        <p><input type="radio" name="ans3" value="3" /> Cepat</p>
                        <p><input type="radio" name="ans3" value="4" /> Sangat Cepat</p>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label>4. Bagaimana pendapat Saudara tentang kewajaran
                            biaya/tarif dalam pelayanan.</label>
                        <p><input type="radio" name="ans4" value="1" /> Sangat Mahal</p>
                        <p><input type="radio" name="ans4" value="2" /> Cukup Mahal</p>
                        <p><input type="radio" name="ans4" value="3" /> Murah</p>
                        <p><input type="radio" name="ans4" value="4" /> Gratis</p>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label>5. Bagaimana pendapat Saudara tentang kesesuaian
                            produk pelayanan antara yang tercantum dalam
                            standar pelayanan dengan hasil yang diberikan.</label>
                        <p><input type="radio" name="ans5" value="1" /> Tidak Sesuai
                        </p>
                        <p><input type="radio" name="ans5" value="2" /> Kurang
                            Sesuai</p>
                        <p><input type="radio" name="ans5" value="3" /> Sesuai</p>
                        <p><input type="radio" name="ans5" value="4" /> Sangat
                            Sesuai</p>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label>6. Bagaimana pendapat Saudara tentang kompetensi/
                            kemampuan petugas dalam pelayanan.</label>
                        <p><input type="radio" name="ans6" value="1" /> Tidak
                            Kompeten</p>
                        <p><input type="radio" name="ans6" value="2" /> Kurang
                            Kompeten</p>
                        <p><input type="radio" name="ans6" value="3" /> Kompeten
                        </p>
                        <p><input type="radio" name="ans6" value="4" /> Sangat
                            Kompeten</p>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label>7. Bagamana pendapat saudara perilaku petugas
                            dalam
                            pelayanan terkait kesopanan dan
                            keramahan</label>
                        <p><input type="radio" name="ans7" value="1" />
                            Tidak sopan dan ramah</p>
                        <p><input type="radio" name="ans7" value="2" />
                            Kurang sopan dan ramah</p>
                        <p><input type="radio" name="ans7" value="3" />
                            Sopan dan ramah</p>
                        <p><input type="radio" name="ans7" value="4" />
                            Sangat sopan dan ramah</p>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label>8. Bagaimana pendapat Saudara tentang
                            kualitas sarana
                            dan prasarana</label>
                        <p><input type="radio" name="ans8" value="1" />
                            Buruk</p>
                        <p><input type="radio" name="ans8" value="2" />
                            Cukup</p>
                        <p><input type="radio" name="ans8" value="3" />
                            Baik</p>
                        <p><input type="radio" name="ans8" value="4" />
                            Sangat Baik</p>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label>9. Bagaimana pendapat Saudara tentang
                            penanganan
                            pengaduan pengguna layanan</label>
                        <p><input type="radio" name="ans9" value="1" />
                            Tidak Ada</p>
                        <p><input type="radio" name="ans9" value="2" /> Ada
                            tetapi tidak
                            berfungsi</p>
                        <p><input type="radio" name="ans9" value="3" />
                            berfungsi kurang
                            maksimal</p>
                        <p><input type="radio" name="ans9" value="4" />
                            Dikelola dengan baik
                        </p>
                    </div>


                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Kirim</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
