<div id="loader" class="center"></div>
<h2>{{$menu->title}}</h2>
<h5>{{$menu->description}}</h5>
<hr>
<div class="card">
    <div class="card-header">
    </div>
    <div class="card-body">
        <iframe src="{{url('pdf/'.$id.'.pdf')}}" frameBorder="0" scrolling="auto" height="500px" width="100%"></iframe>
    </div>
</div>
<form name="form" id="sign-form" action="{{url('sign/store/'.Crypt::encryptString($req->id))}}" method="post">
    @csrf
    <div class="alert alert-light fade show p-1" id="sign-validate" role="alert"></div>
    <div class="form-group">
        <label>NAMA:</label>
        <input type="text" name="name" value="{{Auth::User()->name}}" class="form-control" readonly>
    </div>
    <div class="form-group">
        {{-- <label>NIK:</label> --}}
        {{-- <input type="text" name="nip" value="{{$identity['nip']}}" class="form-control" readonly> --}}
        <input type="text" hidden name="nik" value="{{Auth::User()->nik}}" class="form-control">
    </div>
    <div class="form-group">
        <label>Passphrase:</label>
        <input type="password" name="passphrase" class="form-control">
    </div>
    <div class="form-group mb-5 mt-3">
        {{-- <input type="submit" id="sign-btn" onsubmit="dis()" class="btn btn-success" value="Tanda Tangan"> --}}
        <button class="btn btn-success" type="submit" id="sign-1">Tanda Tangan</button>
    </div>
</form>
