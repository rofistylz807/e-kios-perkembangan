@extends('component.dashboard.main')
@section('content')
<div class="card">
    <div class="card-header">Riwayat Tanda Tangan</div>
    <div class="card-body">
        <div class="table-responsive">
            <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Permohonan</th>
                        <th>Tanggal</th>
                        <th>Pesan</th>
                        <th>Status</th>
                        <th>Tanggapan</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($log as $key=>$value)
                    <tr>
                        <td>{{$value->menu}}</td>
                        <td>{{convertDate($value->date)}}</td>
                        <td>{{$value->message}}</td>
                        @if($value->status=="done")
                        <td><span class="badge badge-success">Selesai</span></td>
                        @elseif($value->status=="reject")
                        <td><span class="badge badge-danger">Ditolak</span></td>
                        @endif
                        <td>{{$value->response}}</td>
                        {{-- <td><button class="btn btn-success" data-toggle="modal" onclick="detail('sign/detail/{{$value->id}}','req-detail-body')" data-target="#reqDetail"><i class="bx bx-search"></i></button></td> --}}
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal" id="reqDetail" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog"
    aria-labelledby="ExtraLargeModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Form Tanggapan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="cls('req-detail-body')"> <span
                        aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body req-detail-body">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="cls('req-detail-body')">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection
