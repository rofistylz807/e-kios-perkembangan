@extends('component.dashboard.main')

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="example2" class="table table-striped table-bordered" style="width:100%">
                        <thead>
                            <tr>
                                <th>Tanggal</th>
                                <th>Jenis Kelamin</th>
                                <th>Pendidikan</th>
                                <th>Pekerjaan</th>
                                <th>Jenis Layanan</th>
                       
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($dataSurvey as $s)
                            <tr>
                                <td>{{convertDate($s->created_at)}}</td>
                                <td>{{$s->jenis_kelamin}}</td>
                                <td>{{$s->pendidikan}}</td>
                                <td>{{$s->pekerjaan}}</td>
                                <td>{{$s->jenis_layanan}}</td>
                           
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            
            <div class="col-md-6">
                
                <div class="card">
                    <p style="padding:20px;" >Bagaimana pendapat Saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya.</p>
                    <div class="card-body">
                      
                        <canvas id="myChart" class="chart" ></canvas>
                    </div>
                </div>
            </div>

                <div class="col-md-6">
                    <div class="card">
                        <p style="padding:20px;" >Bagaimana pemahaman Saudara tentang kemudahan prosedur pelayanan di unit ini.</p>
                        <div class="card-body">
                            <canvas id="myChart2" class="chart" ></canvas>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card">
                        <p style="padding:20px;" >Bagaimana pendapat Saudara tentang kecepatan
                            waktu dalam memberikan pelayanan.</p>
                        <div class="card-body">
                            <canvas id="myChart3" class="chart" ></canvas>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card">
                        <p style="padding:20px;" >Bagaimana pendapat Saudara tentang kewajaran
                            biaya/tarif dalam pelayanan.</p>
                        <div class="card-body">
                            <canvas id="myChart4" class="chart" ></canvas>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card">
                        <p style="padding:20px;" >Bagaimana pendapat Saudara tentang kesesuaian
                            produk pelayanan antara yang tercantum dalam
                            standar pelayanan dengan hasil yang diberikan.</p>
                        <div class="card-body">
                            <canvas id="myChart5" class="chart" ></canvas>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card">
                        <p style="padding:20px;" >Bagaimana pendapat Saudara tentang kompetensi/
                            kemampuan petugas dalam pelayanan.</p>
                        <div class="card-body">
                            <canvas id="myChart6" class="chart" ></canvas>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card">
                        <p style="padding:20px;" >Bagaimana pendapat saudara perilaku petugas dalam
                            pelayanan terkait kesopanan dan keramahan.</p>
                        <div class="card-body">
                            <canvas id="myChart7" class="chart" ></canvas>
                        </div>
                    </div>
                </div>

                
                <div class="col-md-6">
                    <div class="card">
                        <p style="padding:20px;" >Bagaimana pendapat Saudara tentang kualitas sarana
                            dan prasarana.</p>
                        <div class="card-body">
                            <canvas id="myChart8" class="chart" ></canvas>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="card">
                        <p style="padding:20px;" >Bagaimana pendapat Saudara tentang penanganan
                            pengaduan pengguna layanan.</p>
                        <div class="card-body">
                            <canvas id="myChart9" class="chart" ></canvas>
                        </div>
                    </div>
                </div>
            {{-- <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div id="chart2" class="chart" title="Kemudahan pelayanan ( tidak rumit/ tidak birokratis)"
                            data="{{json_encode($data['ans2'])}}"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div id="chart3" class="chart" title="Kecepatan pelayanan"
                            data="{{json_encode($data['ans3'])}}"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div id="chart4" class="chart" title="Respon petugas pelayanan"
                            data="{{json_encode($data['ans4'])}}"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div id="chart5" class="chart" title="Sikap Petugas Pelayanan"
                            data="{{json_encode($data['ans5'])}}"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div id="chart6" class="chart" title="Penjelasan yang diberikan petugas pelayanan"
                            data="{{json_encode($data['ans6'])}}"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div id="chart7" class="chart" title="Sarana Sarana yang digunakan dalam pelayanan"
                            data="{{json_encode($data['ans7'])}}"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div id="chart8" class="chart" title="Fasilitas Fasilitas yang diterima dalam pelayanan"
                            data="{{json_encode($data['ans8'])}}"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div id="chart9" class="chart" title="Kebersihan tempat pelayanan"
                            data="{{json_encode($data['ans9'])}}"></div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-body">
                        <div id="chart10" class="chart" title="Kenyamanan tempat pelayanan"
                            data="{{json_encode($data['ans10'])}}"></div>
                    </div>
                </div>
            </div> --}}
        </div>
    </div>

</div>
</div>
<script src="https://cdn.jsdelivr.net/npm/chart.js@3.3.2/dist/chart.min.js"></script>
<script>
    var labels = [
        
        "Tidak Sesuai ({{ $tidaksesuai }})",
        "Kurang Sesuai ({{ $kurangsesuai }})",
        "Sesuai ({{ $sesuai }})",
        "Sangat Sesuai ({{ $sangatsesuai }})",
    ];
    var data = {
        
        labels: labels,
        
        datasets: [{
            label: 'Data Permohonan',
      
            backgroundColor: ['Red','Yellow', 'Green', 'Blue'],
            data: ["{{ $tidaksesuai }}", "{{ $kurangsesuai }}", "{{ $sesuai }}" , "{{ $sangatsesuai }}"
            ],
        }]
    };

    
    var config = {
        
        type: 'doughnut',
        data:data,
        options: {
            plugins: {
            title: {
                display: false,
                titleFontSize: 50,
                text: 'Bagaimana pendapat Saudara tentang kesesuaian persyaratan pelayanan dengan jenis pelayanannya'
            }
        }
    }
          
    };
    new Chart(
        document.getElementById('myChart'),
        config
    );

</script>

{{-- pertanyaan2 --}}
<script>
    var labels = [
        
        "Tidak Mudah ({{ $tidakmudah }})",
        "Kurang Mudah ({{ $kurangmudah }})",
        "Mudah ({{ $mudah }})",
        "Sangat Mudah ({{ $sangatmudah }})",
    ];
    var data = {
        
        labels: labels,
        
        datasets: [{
            label: 'Data Permohonan',
      
            backgroundColor: ['Red','Yellow', 'Green', 'Blue'],
            data: ["{{ $tidakmudah }}", "{{ $kurangmudah }}", "{{ $mudah }}" , "{{ $sangatmudah }}"
            ],
        }]
    };

    
    var config = {
        
        type: 'doughnut',
        data:data,
        options: {
            plugins: {
            title: {
                display: false,
                text: 'Bagaimana pemahaman Saudara tentang kemudahan prosedur pelayanan di unit ini.'
            }
        }
    }
          
    };
    new Chart(
        document.getElementById('myChart2'),
        config
    );

</script>


{{-- pertanyaan3 --}}
<script>
    var labels = [
        
        "Tidak Cepat ({{ $tidakcepat }})",
        "Kurang Cepat ({{ $kurangcepat }})",
        "Cepat ({{ $cepat }})",
        "Sangat Cepat ({{ $sangatcepat }})",
    ];
    var data = {
        
        labels: labels,
        
        datasets: [{
            label: '',
      
            backgroundColor: ['Red','Yellow', 'Green', 'Blue'],
            data: ["{{ $tidakcepat }}", "{{ $kurangcepat }}", "{{ $cepat }}" , "{{ $sangatcepat }}"
            ],
        }]
    };

    
    var config = {
        
        type: 'doughnut',
        data:data,
        options: {
            plugins: {
            title: {
                display: false,
                text: ''
            }
        }
    }
          
    };
    new Chart(
        document.getElementById('myChart3'),
        config
    );

</script>

{{-- pertanyaan4 --}}
<script>
    var labels = [
        
        "Sangat Mahal ({{ $sangatmahal }})",
        "Cukup Mahal ({{ $cukupmahal }})",
        "Murah ({{ $murah }})",
        "Gratis ({{ $gratis }})",
    ];
    var data = {
        
        labels: labels,
        
        datasets: [{
            label: '',
      
            backgroundColor: ['Red','Yellow', 'Green', 'Blue'],
            data: ["{{ $sangatmahal }}", "{{ $cukupmahal }}", "{{ $murah }}" , "{{ $gratis }}"
            ],
        }]
    };

    
    var config = {
        
        type: 'doughnut',
        data:data,
        options: {
            plugins: {
            title: {
                display: false,
                text: ''
            }
        }
    }
          
    };
    new Chart(
        document.getElementById('myChart4'),
        config
    );

</script>

{{-- pertanyaan5 --}}
<script>
    var labels = [
        
        "Tidak Sesuai ({{ $tidaksesuailima }})",
        "Kurang Sesuai ({{ $kurangsesuailima }})",
        "Sesuai ({{ $sesuailima }})",
        "Sangat Sesuai ({{ $sangatsesuailima }})",
    ];
    var data = {
        
        labels: labels,
        
        datasets: [{
            label: '',
      
            backgroundColor: ['Red','Yellow', 'Green', 'Blue'],
            data: ["{{ $tidaksesuailima }}", "{{ $kurangsesuailima }}", "{{ $sesuailima }}" , "{{ $sangatsesuailima }}"
            ],
        }]
    };

    
    var config = {
        
        type: 'doughnut',
        data:data,
        options: {
            plugins: {
            title: {
                display: false,
                text: ''
            }
        }
    }
          
    };
    new Chart(
        document.getElementById('myChart5'),
        config
    );

</script>

{{-- pertanyaan6 --}}
<script>
    var labels = [
        
        "Tidak Kompeten ({{ $tidakkompeten }})",
        "Kurang Kompeten ({{ $kurangkompeten }})",
        "Kompeten ({{ $kompeten }})",
        "Sangat Kompeten ({{ $sangatkompeten }})",
    ];
    var data = {
        
        labels: labels,
        
        datasets: [{
            label: '',
      
            backgroundColor: ['Red','Yellow', 'Green', 'Blue'],
            data: ["{{ $tidakkompeten }}", "{{ $kurangkompeten }}", "{{ $kompeten }}" , "{{ $sangatkompeten }}"
            ],
        }]
    };

    
    var config = {
        
        type: 'doughnut',
        data:data,
        options: {
            plugins: {
            title: {
                display: false,
                text: ''
            }
        }
    }
          
    };
    new Chart(
        document.getElementById('myChart6'),
        config
    );

</script>

{{-- pertanyaan7 --}}
<script>
    var labels = [
        
        "Tidak sopan dan ramah ({{ $tidaksopan }})",
        "Kurang sopan dan ramah ({{ $kurangsopan }})",
        "Sopan dan ramah ({{ $sopan }})",
        "Sangat sopan dan ramah ({{ $sangatsopan }})",
    ];
    var data = {
        
        labels: labels,
        
        datasets: [{
            label: '',
      
            backgroundColor: ['Red','Yellow', 'Green', 'Blue'],
            data: ["{{ $tidaksopan }}", "{{ $kurangsopan }}", "{{ $sopan }}" , "{{ $sangatsopan }}"
            ],
        }]
    };

    
    var config = {
        
        type: 'doughnut',
        data:data,
        options: {
            plugins: {
            title: {
                display: false,
                text: ''
            }
        }
    }
          
    };
    new Chart(
        document.getElementById('myChart7'),
        config
    );

</script>

{{-- pertanyaan8 --}}
<script>
    var labels = [
        
        "Buruk ({{ $buruk }})",
        "Cukup ({{ $cukup }})",
        "Baik ({{ $baik }})",
        "Sangat Baik ({{ $sangatbaik }})",
    ];
    var data = {
        
        labels: labels,
        
        datasets: [{
            label: '',
      
            backgroundColor: ['Red','Yellow', 'Green', 'Blue'],
            data: ["{{ $buruk }}", "{{ $cukup }}", "{{ $baik }}" , "{{ $sangatbaik }}"
            ],
        }]
    };

    
    var config = {
        
        type: 'doughnut',
        data:data,
        options: {
            plugins: {
            title: {
                display: false,
                text: ''
            }
        }
    }
          
    };
    new Chart(
        document.getElementById('myChart8'),
        config
    );

</script>

{{-- pertanyaan9 --}}
<script>
    var labels = [
        
        "Tidak ada ({{ $tidakada }})",
        "Ada tetapi tidak berfungsi ({{ $ada }})",
        "Berfungsi kurang maksimal ({{ $berfungsi }})",
        "Dikelola dengan baik. ({{ $dikelola }})",
    ];
    var data = {
        
        labels: labels,
        
        datasets: [{
            label: '',
      
            backgroundColor: ['Red','Yellow', 'Green', 'Blue'],
            data: ["{{ $tidakada }}", "{{ $ada }}", "{{ $berfungsi }}" , "{{ $dikelola }}"
            ],
        }]
    };

    
    var config = {
        
        type: 'doughnut',
        data:data,
        options: {
            plugins: {
            title: {
                display: false,
                text: ''
            }
        }
    }
          
    };
    new Chart(
        document.getElementById('myChart9'),
        config
    );

</script>
@endsection

