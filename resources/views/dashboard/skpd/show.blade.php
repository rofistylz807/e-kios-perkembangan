

<form enctype="multipart/form-data" action="{{url('skpd/store/'.
Crypt::encryptString(json_encode(['status'=>isset($skpd['skpd_id'])?2:1,
'id'=>isset($skpd['skpd_id'])?$skpd['skpd_id']:0])))}}" method="post" 
onsubmit="return validate('skpd/validate/{{Crypt::encryptString(json_encode(['status'=>isset($skpd['skpd_id'])?2:1,'id'=>isset($skpd['skpd_id'])?$skpd['skpd_id']:0]))}}')" >
    @csrf
    <div class="form-group mt-4">
        <label>Nama</label>
        <input type="text" class="form-control" placeholder="Nama" name="nama" value="{{isset($skpd['skpd_name'])?$skpd['skpd_name']:''}}"/>
        <div class="alert alert-light fade show p-1" id="nama" role="alert">
        </div>
    </div>
    <div class="form-group mt-4">
        <label>Kop Surat</label>
        <input class="form-control" type="file" placeholder="Gambar" name="img" accept="image/*">
        <br>
        {{-- <img id="category-img-tag" width="500px" src="{{asset('img/'.$skpd->img)}}"alt=""> --}}

{{-- <br>
        @if($skpd->img!='')
        <img src={{$skpd->img}} id="category-img-tag" width="500px" />   <!--for preview purpose -->
        @else
        <img src="#" id="category-img-tag" width="200px" />
        @endif --}}
        <div class="alert alert-light fade show p-1" id="img" role="alert">
        </div>
    </div>
    <div class="form-group mt-4">
        <label>Email</label>
        <input type="email" class="form-control" placeholder="Email" name="email" value="{{isset($skpd['skpd_email'])?$skpd['skpd_email']:''}}"/>
        <div class="alert alert-light fade show p-1" id="email" role="alert">
        </div>
    </div>
    <div class="form-group mt-4">
        <label>Nomor Telepon</label>
        <input type="text" class="form-control" placeholder="Nomor Telepon" name="nomor_telepon" value="{{isset($skpd['skpd_phone'])?$skpd['skpd_phone']:''}}"/>
        <div class="alert alert-light fade show p-1" id="nomor_telepon" role="alert">
        </div>
    </div>
    <div class="form-group mt-4">
        <label>Faksimili</label>
        <input type="text" class="form-control" placeholder="Faksimili" name="faximili" value="{{isset($skpd['skpd_fax'])?$skpd['skpd_fax']:''}}"/>
        <div class="alert alert-light fade show p-1" id="faximili" role="alert">
        </div>
    </div>
    <div class="form-group mt-4">
        <label>Alamat</label>
        <textarea class="form-control" placeholder="Alamat" name="alamat">{{isset($skpd['skpd_address'])?$skpd['skpd_address']:''}}</textarea>
        <div class="alert alert-light fade show p-1" id="alamat" role="alert">
        </div>
    </div>
    <div class="form-group mt-4">
        <label>Kecamatan</label>
        <input type="text" class="form-control" placeholder="Kecamatan" name="kecamatan" value="{{isset($skpd['skpd_sub_district'])?$skpd['skpd_sub_district']:''}}"/>
        <div class="alert alert-light fade show p-1" id="kecamatan" role="alert">
        </div>
    </div>
    <div class="form-group mt-4">
        <label>Kode Pos</label>
        <input type="text" class="form-control" placeholder="Kode Pos" name="kode_pos" value="{{isset($skpd['skpd_postal_code'])?$skpd['skpd_postal_code']:''}}"/>
        <div class="alert alert-light fade show p-1" id="kode_pos" role="alert">
        </div>
    </div>
    <div class="form-group mt-4">
        <label>Nama Kepala</label>
        <input type="text" class="form-control" placeholder="Nama Kepala" name="nama_kepala" value="{{isset($skpd['skpd_head_name'])?$skpd['skpd_head_name']:''}}"/>
        <div class="alert alert-light fade show p-1" id="nama_kepala" role="alert">
        </div>
    </div>
    <div class="form-group mt-4">
        <label>NIP Kepala</label>
        <input type="text" class="form-control" placeholder="NIP Kepala" name="nip_kepala" value="{{isset($skpd['skpd_head_nip'])?$skpd['skpd_head_nip']:''}}"/>
        <div class="alert alert-light fade show p-1" id="nip_kepala" role="alert">
        </div>
    </div>
    <div class="form-group mt-4">
        <label>Posisi Kepala</label>
        <input type="text" class="form-control" placeholder="Posisi Kepala" name="posisi_kepala" value="{{isset($skpd['skpd_head_position'])?$skpd['skpd_head_position']:''}}"/>
        <div class="alert alert-light fade show p-1" id="posisi_kepala" role="alert">
        </div>
    </div>
    <div class="form-group mt-4">
        <label>Pangkat Kepala</label>
        <input type="text" class="form-control" placeholder="Pangkat Kepala" name="pangkat_kepala" value="{{isset($skpd['skpd_head_level'])?$skpd['skpd_head_level']:''}}"/>
        <div class="alert alert-light fade show p-1" id="pangkat_kepala" role="alert">
        </div>
    </div>
    <button type="submit" class="btn btn-primary btn-block mt-4">SUBMIT</button>
</form>