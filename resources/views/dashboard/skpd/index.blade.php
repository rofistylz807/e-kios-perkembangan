@extends('component.dashboard.main')
@section('content')
<div class="card">
    <div class="card-header">SKPD</div>
    <div class="card-body">
        <button class="btn btn-primary mb-2" onclick="detail('skpd/form/{{Crypt::encryptString(json_encode(['status'=>1,'id'=>0]))}}','skpd-edit-body')" data-toggle="modal" data-target="#skpd-edit"><i class="bx bx-plus"></i></button>
        <div class="table-responsive">
            <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th>Nomor Telepon</th>
                        <th>Email</th>
                        <th>Kop Surat</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($skpd as $key=>$value)
                    <tr>
                        <td>{{$value->skpd_name}}</td>
                        <td>{{$value->skpd_phone}}</td>
                        <td>{{$value->skpd_email}}</td>
                        <td><img id="category-img-tag" width="300px" src="{{asset('img/'.$value->img)}}"alt=""></td>
                        {{-- <td>@if($value->img!='')
                            <img src={{$value->img}} id="category-img-tag" width="300px" />   <!--for preview purpose -->
                            @else
                            <img src="#" id="category-img-tag" width="200px" />
                            @endif</td> --}}
                        <td><button class="btn btn-success" data-toggle="modal" data-target="#skpd-edit" onclick="detail('skpd/form/{{Crypt::encryptString(json_encode(['status'=>0,'id'=>$value->skpd_id]))}}','skpd-edit-body')"><i class="bx bx-edit"></i></button>
                        <button class="btn btn-danger" data-toggle="modal" data-target="#skpd-delete"><i class="bx bx-trash" onclick="del('{{$value->skpd_name}}','skpd/del/{{Crypt::encryptString($value->skpd_id)}}')"></i></button></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal" id="skpd-edit" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog"
    aria-labelledby="ExtraLargeModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Form SKPD</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="cls('skpd-edit-body')"> <span
                        aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body skpd-edit-body">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="cls('skpd-edit-body')">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="skpd-delete" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog"
    aria-labelledby="ExtraLargeModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Hapus SKPD</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="clsdel()"> <span
                        aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Apakah anda yakin ingin menghapus <span class="del-title"></span>
            </div>
            <div class="modal-footer">
                <a class="btn btn-success del-url">Delete</a>
            
                <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="clsdel()">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection
