@foreach($sign as $kd=> $kv)
<div class="form-group">
    <label>{{strtoupper($kd)}}</label>
    <select class="form-control" name="sign[{{$kd}}][]" required>
        <option value=""></option>
        @foreach($skpd as $value)
        <option value="{{$value->skpd_id}}">{{$value->skpd_name}}</option>
        @endforeach
    </select>
</div>
@endforeach