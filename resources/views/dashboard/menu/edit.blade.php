@extends('component.dashboard.main')
@section('content')
<div class="card">
    <div class="card-header">
        Edit Menu
    </div>
    <div class="card-body">
        <form action="{{url('menus/update/'.Crypt::encryptString(json_encode(['menu'=>$menu->id_menu,'id'=>$id])))}})}}" method="post" enctype="multipart/form-data" >
            @csrf
            <div class="form-group">
                <label>Menu</label>
                <input class="form-control" type="text" placeholder="Menu" name="title" value="{{$menu->title}}">
            </div>
            <div class="form-group">
                <label>SKPD</label>
                <select class="form-control" name="skpd">
                    <option value=""></option>
                    @foreach($skpd as $value)
                    <option {{$value->skpd_id==$menu->skpd_id?"selected":""}} value="{{$value->skpd_id}}">{{$value->skpd_name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Deskripsi</label>
                <textarea class="form-control" placeholder="Deskripsi" name="description">{{$menu->description}}</textarea>
            </div>
            <div class="form-group">
                <label>Link</label>
                <input class="form-control" type="text" placeholder="Link" name="url" value="{{$menu->url}}">
            </div>
            <div class="form-group">
                <label>Gambar</label><br>
                <img src="{{$menu->img}}" alt="">
                <input class="form-control" type="file" placeholder="Gambar" name="img" accept="image/*">
            </div>
            <div class="form-group">
                <label>Format Surat</label>
                <select class="form-control" onchange="addform(this)" name="format">
                    <option value=""></option>
                    @foreach($format as $value)
                    <option {{$value->id==$menu->format_id?"selected":""}} value="{{$value->id}}">{{$value->name}}</option>
                    @endforeach
                </select>
            </div>
            <br>
            <div class="place-sign">
                @if(count($sign))
                @foreach($sign as $ds)
                    <div class="form-group">
                        <label>{{strtoupper($ds->keyword)}}</label>
                        <select class="form-control" name="sign[{{$ds->keyword}}][]" required>
                            <option value=""></option>
                            @foreach($skpd as $value)
                            <option {{$value->skpd_id==$ds->skpd_id?"selected":""}} value="{{$value->skpd_id}}">{{$value->skpd_name}}</option>
                            @endforeach
                        </select>
                    </div>
                @endforeach
                @endif
            </div>
            <hr>
            <div class="form-group">
                <input class="btn btn-success mt-2" type="submit" value="Simpan">
            </div>
        </form>
    </div>
</div>
@endsection
