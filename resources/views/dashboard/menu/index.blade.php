@extends('component.dashboard.main')
@section('content')
<div class="card">
    <div class="card-header">
        Menu
    </div>
    <div class="card-body">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                @foreach($breadcrumb as $key=>$value)
                <li class="breadcrumb-item"><a href="{{$value}}">{{$key}}</a></li>
                @endforeach
                <li class="breadcrumb-item active" aria-current="page">Data</li>
            </ol>
        </nav>

        @if (session('status_edit'))
        <div class="alert alert-warning alert-dismissible" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h6><i class="fas fa-edit"></i><b>Sukses!</b></h6>
            {{ session('status_edit') }}
        </div>
    @endif

        @if (session('status_delete'))
            <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
              <h6><i class="fas fa-trash"></i><b>Sukses!</b></h6>
                {{ session('status_delete') }}
            </div>
        @endif

        <a class="btn btn-primary mb-2" href="{{url('menus/create/'.$id)}}"><i class="bx bx-plus"></i></a>
        <div class="table-responsive">
            <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Menu</th>
                        <th>SKPD</th>
                        <th>Deskripsi</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($menu as $key=>$value)
                    <tr>
                        <td>{{$value->id}}</td>
                        <td><a href="{{url('menus/'.$value->id)}}">{{$value->title}}</a></td>
                        <td>{{$value->skpd_name}}</td>
                        <td>{{$value->description}}</td>
                        <td>
                            <a href="{{url('menus/edit/'.Crypt::encryptString(json_encode(['menu'=>$value->id,'id'=>$id])))}}" class="btn btn-success">
                            <i class="bx bx-edit"></i>
                            </a>
                            <button data-toggle="modal" data-target="#menu-delete" class="btn btn-danger" onclick="del('{{$value->title}}','menus/del/{{Crypt::encryptString($value->id)}}')">
                            <i class="bx bx-trash"></i>
                            </button>
                        </td>           
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal" id="quesDetail" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog"
    aria-labelledby="ExtraLargeModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detail Pertanyaan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"
                    onclick="cls('ques-detail-body')"> <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body ques-detail-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" onclick="cls('ques-detail-body')"
                    data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="menu-delete" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog"
    aria-labelledby="ExtraLargeModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Hapus Menu</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="clsdel()"> <span
                        aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Apakah anda yakin ingin menghapus <span class="del-title"></span>
            </div>
            <div class="modal-footer">
                <a class="btn btn-success del-url">Delete</a>
            
                <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="clsdel()">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection
