@extends('component.dashboard.main')
@section('content')
<div class="card">
    <div class="card-header">
        Tambah Menu
    </div>
    <div class="card-body">
        <form action="{{url('menus/store/'.$id)}}" method="post" enctype="multipart/form-data" >
            @csrf
            <div class="form-group">
                <label>Menu</label>
                <input class="form-control" type="text" placeholder="Menu" name="title">
            </div>
            <div class="form-group">
                <label>SKPD</label>
                <select class="form-control" name="skpd">
                    <option value=""></option>
                    @foreach($skpd as $value)
                    <option value="{{$value->skpd_id}}">{{$value->skpd_name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Deskripsi</label>
                <textarea class="form-control" placeholder="Deskripsi" name="description"></textarea>
            </div>
            <div class="form-group">
                <label>Link</label>
                <input class="form-control" type="text" placeholder="Link" name="url">
            </div>
            <div class="form-group">
                <label>Gambar</label>
                <input class="form-control" type="file" placeholder="Gambar" name="img" accept="image/*">
            </div>
            <div class="form-group">
                <label>Format Surat</label>
                <select class="form-control" onchange="addform(this)" name="format">
                    <option value=""></option>
                    @foreach($format as $value)
                    <option value="{{$value->id}}">{{$value->name}}</option>
                    @endforeach
                </select>
            </div>
            <br>
            <div class="place-sign">
            </div>
            <hr>
            <div class="form-group">
                <input class="btn btn-success mt-2" type="submit" value="Simpan">
            </div>
        </form>
    </div>
</div>
@endsection
