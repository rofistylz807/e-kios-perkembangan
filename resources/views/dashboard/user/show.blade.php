<form action="{{url('user/store/'.Crypt::encryptString(json_encode(['status'=>isset($user['id'])?2:1,'id'=>isset($user['id'])?$user['id']:0])))}}" method="post" onsubmit="return validate('user/validate/{{Crypt::encryptString(json_encode(['status'=>isset($user['id'])?2:1,'id'=>isset($user['id'])?$user['id']:0]))}}')" >
    @csrf
    <div class="form-group mt-4">
        <label>Username</label>
        <input type="text" class="form-control" placeholder="Username" name="username" value="{{isset($user['username'])?$user['username']:''}}"/>
        <div class="alert alert-light fade show p-1" id="username" role="alert">
        </div>
    </div>
    <div class="form-group mt-4">
        <label>NIK</label>
        <input type="text" class="form-control" placeholder="NIK" name="nik" value="{{isset($user['nik'])?$user['nik']:''}}"/>
        <div class="alert alert-light fade show p-1" id="nik" role="alert">
        </div>
    </div>
    <div class="form-group mt-4">
        <label>Nama Lengkap</label>
        <input type="text" class="form-control" placeholder="Nama Lengkap" name="nama" value="{{isset($user['name'])?$user['name']:''}}"/>
        <div class="alert alert-light fade show p-1" id="nama" role="alert"></div>
    </div>
    <div class="form-group mt-4">
        <label>Email</label>
        <input type="text" class="form-control" placeholder="Email" name="email" value="{{isset($user['email'])?$user['email']:''}}"/>
        <div class="alert alert-light fade show p-1" id="email" role="alert"></div>
    </div>
    <div class="form-group mt-4">
        <label>Nomor HP</label>
        <input type="number" class="form-control" placeholder="Nomor HP" name="nomor_hp" value="{{isset($user['phone'])?$user['phone']:''}}"/>
        <div class="alert alert-light fade show p-1" id="nomor_hp" role="alert"></div>
    </div>
    <div class="form-group mt-4">
        <label>SKPD</label>
        <select name="skpd" class="form-control">
            <option value=""></option>
            @foreach($skpd as $value)
                <option value="{{$value->skpd_id}}" {{isset($user['skpd_id'])?$user['skpd_id']==$value->skpd_id?'selected':'':''}}>{{$value->skpd_name}}</option>
            @endforeach
        </select>
        <div class="alert alert-light fade show p-1" id="skpd" role="alert"></div>
    </div>
    <div class="form-group mt-4">
        <label>Role</label>
        <select name="role" class="form-control">
            <option value=""></option>
            <option value="admin" {{isset($user['role'])?$user['role']=='admin'?'selected':'':''}}>Admin</option>
            <option value="user" {{isset($user['role'])?$user['role']=='user'?'selected':'':''}}>User</option>
            <option value="opd" {{isset($user['role'])?$user['role']=='opd'?'selected':'':''}}>SKPD</option>
        </select>
        <div class="alert alert-light fade show p-1" id="role" role="alert"></div>
    </div>
    <div class="form-group">
        <label>Password</label>
        <input type="password" class="form-control" placeholder="Password" name="password" />
        <div class="alert alert-light fade show p-1" id="password" role="alert">
        </div>
    </div>
    <button type="submit" class="btn btn-primary btn-block mt-4">SUBMIT</button>
</form>