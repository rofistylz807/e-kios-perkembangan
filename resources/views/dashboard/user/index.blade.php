@extends('component.dashboard.main')
@section('content')
<div class="card">
    <div class="card-header">User</div>
    <div class="card-body">
        <button class="btn btn-primary mb-2" onclick="detail('user/form/{{Crypt::encryptString(json_encode(['status'=>1,'id'=>0]))}}','user-edit-body')" data-toggle="modal" data-target="#user-edit"><i class="bx bx-plus"></i></button>
        <div class="table-responsive">
            <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Nama</th>
                        <th>NIK</th>
                        <th>Role</th>
                        <th>No HP</th>
                        <th>SKPD</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($user as $key=>$value)
                    <tr>
                        <td>{{$value->name}}</td>
                        <td>{{$value->nik}}</td>
                        <td>{{strtoupper($value->role)}}</td>
                        <td>{{$value->phone}}</td>
                        <td>{{$value->skpd_name}}</td>
                        <td><button class="btn btn-success" data-toggle="modal" data-target="#user-edit" onclick="detail('user/form/{{Crypt::encryptString(json_encode(['status'=>0,'id'=>$value->id]))}}','user-edit-body')"><i class="bx bx-edit"></i></button>
                        @if(Auth::User()->id!=$value->id)
                        <button class="btn btn-danger" data-toggle="modal" data-target="#user-delete"><i class="bx bx-trash" onclick="del('{{$value->username}}','user/del/{{Crypt::encryptString($value->id)}}')"></i></button></td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<div class="modal" id="user-edit" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog"
    aria-labelledby="ExtraLargeModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Form User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="cls('user-edit-body')"> <span
                        aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body user-edit-body">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="cls('user-edit-body')">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal" id="user-delete" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog"
    aria-labelledby="ExtraLargeModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Hapus User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="clsdel()"> <span
                        aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Apakah anda yakin ingin menghapus <span class="del-title"></span>
            </div>
            <div class="modal-footer">
                <a class="btn btn-success del-url">Delete</a>
            
                <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="clsdel()">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection
