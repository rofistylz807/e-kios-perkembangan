@extends('component.dashboard.main')
@section('content')
<div class="card">
    <div class="card-header">Daftar Pertanyaan</div>
    <div class="card-body">
        <div class="table-responsive">
            <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Pertanyaan</th>
                        <th>Tanggapan</th>
                        @if(Auth::User()->role=="admin")
                        <th></th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                    @foreach($question as $key=>$value)
                    <tr>
                        <td>{{$value->message}}</td>
                        <td>{{$value->response}}</td>
                        @if(Auth::User()->role=="admin")
                        <td><button class="btn btn-success" data-toggle="modal" data-target="#quesDetail" onclick="detail('question/detail/{{Crypt::encryptString($value->id)}}','ques-detail-body')"><i class="bx bx-search"></i></button></td>
                        @endif
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="modal" id="quesDetail" data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog"
    aria-labelledby="ExtraLargeModelLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Detail Pertanyaan</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="cls('ques-detail-body')"> <span
                        aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body ques-detail-body">
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" onclick="cls('ques-detail-body')" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection
