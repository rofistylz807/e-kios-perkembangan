
<form name="form" action="{{url('question/update/'.Crypt::encryptString($question->id))}}"
    onsubmit="return validate('question/validate/response')" method="post">
    @csrf
    <div class="form-group">
        <label>Tanggapan:</label>
        <textarea name="tanggapan" class="form-control">{{$question->response}}</textarea>
        <div class="alert alert-light fade show p-1" id="tanggapan" role="alert"></div>
    </div>
    <div class="form-group mb-5 mt-3">
        <input class="btn btn-success" type="submit" value="Kirim">
    </div>
</form>
