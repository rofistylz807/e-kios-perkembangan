@extends('component.dashboard.main')
@section('content')
<div class="card">
    <div class="card-body">
        <ul class="nav nav-tabs">
            <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#profile"><span
                        class="p-tab-name">Profil</span><i class='bx bx-donate-blood font-24 d-sm-none'></i></a>
            </li>
            @if(Auth::User()->role=="admin"||Auth::User()->role!="opd"||Auth::User()->role!="user")
            <li class="nav-item"> <a class="nav-link" id="profile-tab" data-toggle="tab" href="#password"><span
                        class="p-tab-name">Password</span><i class='bx bxs-user-rectangle font-24 d-sm-none'></i></a>
            </li>
            @endif
        </ul>
    </div>
</div>
<div class="card">
    <div class="card-body">
        <div class="tab-content">
            <div class="tab-pane fade show active" id="profile">
                <div class="card shadow-none border mb-0">
                    <div class="card-body">
                        <form method="post" action="{{url('setting/update')}}" id="form-profile" onsubmit="return validationSetting('form-profile')" >
                        @csrf
                        @if(Auth::User()->role!="user"||Auth::User()->role!="opd")
                            <div class="form-group">
                                <input type="hidden" name="x" value="0">
                                <label>Username:</label>
                                <input type="text" class="form-control" value="{{Auth::User()->username}}" name="username" placeholder="Username">
                                <div class="alert alert-light fade show p-1" id="username" role="alert"></div>
                            </div>
                        @endif
                            <div class="form-group">
                                <input type="hidden" name="x" value="0">
                                <label>NIK:</label>
                                <input type="text" class="form-control" value="{{Auth::User()->nik}}" name="nik" placeholder="NIK">
                                <div class="alert alert-light fade show p-1" id="nik" role="alert"></div>
                            </div>
                            <div class="form-group">
                                <label>Nama:</label>
                                <input type="text" class="form-control" value="{{Auth::User()->name}}" placeholder="Nama" name="nama">
                                <div class="alert alert-light fade show p-1" id="nama" role="alert"></div>
                            </div>
                            <div class="form-group">
                                <label>Email:</label>
                                <input type="email" class="form-control" placeholder="Email" value="{{Auth::User()->email}}" name="email">
                                <div class="alert alert-light fade show p-1" id="email" role="alert"></div>
                            </div>
                            <div class="form-group">
                                <label>Nomor HP:</label>
                                <input type="number" class="form-control" value="{{Auth::User()->phone}}" placeholder="Nomor HP" name="nomor_hp">
                                <div class="alert alert-light fade show p-1" id="nomor_hp" role="alert"></div>
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-success" value="Ubah">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @if(Auth::User()->role=="admin"||Auth::User()->role!="user"||Auth::User()->role!="opd")
            <div class="tab-pane fade" id="password">
                <div class="card shadow-none border mb-0">
                    <div class="card-body">
                        <form method="post" action="{{url('setting/update')}}" id="form-password" onsubmit="return validationSetting('form-password')">
                        @csrf
                            <div class="form-group">
                            <input type="hidden" name="x" value="1">
                                <label>Password Lama:</label>
                                <input type="password" class="form-control" placeholder="Password Lama" name="password_lama">
                                <div class="alert alert-light fade show p-1" id="password_lama" role="alert"></div>
                            </div>
                            <div class="form-group">
                                <label>Password Baru:</label>
                                <input type="password" class="form-control" placeholder="Password Baru" name="password_baru">
                                <div class="alert alert-light fade show p-1" id="password_baru" role="alert"></div>
                            </div>
                            <div class="form-group">
                                <label>Konfirmasi Password Baru:</label>
                                <input type="password" class="form-control" placeholder="Konfirmasi Password Baru" name="konfirmasi_password_baru">
                                <div class="alert alert-light fade show p-1" id="konfirmasi_password_baru" role="alert"></div>
                            </div>
                            <div class="form-group">
                                <input type="submit" class="btn btn-success" value="Ubah">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>
@endsection
