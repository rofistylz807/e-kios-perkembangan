<p style="text-align: center;"><span style="text-decoration: underline;"><strong>SURAT KETERANGAN</strong></span></p>
<p style="text-align: center;">Nomor :{{$data['nomor_surat']}}</p>
<p style="text-align: left;">Yang bertanda tangan dibawah ini :</p>
<table style="width: 100%; border-collapse: collapse; border-style: hidden; height: 36px;" border="1">
<tbody>
<tr style="height: 18px;">
<td style="width: 24.0644%; height: 18px; border-style: none;">&nbsp; &nbsp; &nbsp;Nama</td>
<td style="width: 71.2324%; height: 18px; border-style: none;">:{{$data['nama_ttd']}}</td>
</tr>
<tr style="height: 18px;">
<td style="width: 24.0644%; height: 18px; border-style: none;">&nbsp; &nbsp; &nbsp;Jabatan</td>
<td style="width: 71.2324%; height: 18px; border-style: none;">:{{$data['jabatan_ttd']}}</td>
</tr>
</tbody>
</table>
<p style="text-align: left;">Menerangkan dengan sebenarnya bahwa&nbsp; :</p>
<table style="height: 144px; width: 100%; border-collapse: collapse; border-style: hidden;" border="1">
<tbody>
<tr style="height: 18px;">
<td style="width: 25.7122%; height: 18px; border-style: none;">&nbsp; &nbsp; &nbsp; Nama</td>
<td style="width: 77.6176%; height: 18px; border-style: none;">:{{$data['nama']}}</td>
</tr>
<tr style="height: 18px;">
<td style="width: 25.7122%; height: 18px; border-style: none;">&nbsp; &nbsp; &nbsp; NIK</td>
<td style="width: 77.6176%; height: 18px; border-style: none;">:{{$data['nik']}}</td>
</tr>
<tr style="height: 18px;">
<td style="width: 25.7122%; height: 18px; border-style: none;">&nbsp; &nbsp; &nbsp; Tempat/Tanggal Lahir</td>
<td style="width: 77.6176%; height: 18px; border-style: none;">:{{$data['tempat_lahir']}}, {{$data['tanggal_lahir']}}</td>
</tr>
<tr style="height: 18px;">
<td style="width: 25.7122%; height: 18px; border-style: none;">&nbsp; &nbsp; &nbsp; Jenis Kelamin</td>
<td style="width: 77.6176%; height: 18px; border-style: none;">:{{$data['jenis_kelamin']}}</td>
</tr>
<tr style="height: 18px;">
<td style="width: 25.7122%; height: 18px; border-style: none;">&nbsp; &nbsp; &nbsp; Agama</td>
<td style="width: 77.6176%; height: 18px; border-style: none;">:{{$data['agama']}}</td>
</tr>
<tr style="height: 18px;">
<td style="width: 25.7122%; height: 18px; border-style: none;">&nbsp; &nbsp; &nbsp; Pekerjaan</td>
<td style="width: 77.6176%; height: 18px; border-style: none;">:{{$data['pekerjaan']}}</td>
</tr>
<tr style="height: 18px;">
<td style="width: 25.7122%; height: 18px; border-style: none;">&nbsp; &nbsp; &nbsp; Alamat</td>
<td style="width: 77.6176%; height: 18px; border-style: none;">:{{$data['alamat']}}</td>
</tr>
<tr style="height: 18px;">
<td style="width: 25.7122%; height: 18px; border-style: none;">&nbsp; &nbsp; &nbsp; NO. HP</td>
<td style="width: 77.6176%; height: 18px; border-style: none;">:{{$data['no_hp']}}</td>
</tr>
</tbody>
</table>
<p style="text-align: justify;">&nbsp; &nbsp; &nbsp;Berdasarkan Surat Pengantar RT. {{$data['nomor_rt']}} . Nomor . {{$data['nomor_surat_rt']}} dan Surat Pernyataan yang bersangkutan dan dengan menerangkan bahwa {{$data['nama']}} dengan NIK {{$data['nik']}} yang tercantum dalam Kartu Tanda Penduduk (KTP) dan Kartu Keluarga, dan tercantum pada &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;. nama &hellip;&hellip;&hellip;&hellip;&hellip;&hellip;&hellip; adalah <em><u>ORANG YANG SAMA.</u></em></p>
<p style="text-align: justify;">&nbsp; &nbsp;Demikian Surat Keterangan ini dibuat dengan sebenarnya dan diberikan kepada yang bersangkutan untuk dapat dipergunakan sebagaimana mestinya.</p>
<table style="height: 37px; width: 100%; border-collapse: collapse; border-style: hidden;" border="1">
<tbody>
<tr style="height: 18px;">
<td style="width: 56.9173%; height: 19px; border-style: none;">&nbsp;</td>
<td style="width: 16.9583%; height: 19px; border-style: none;">Dibuat Di</td>
<td style="width: 1.5448%; height: 19px; border-style: none;">:</td>
<td style="width: 31.0848%; border-style: none; height: 19px;">{{$data['dibuat_di']}}</td>
</tr>
<tr style="height: 18px;">
<td style="width: 56.9173%; border-style: none; height: 18px;">&nbsp;</td>
<td style="width: 16.9583%; border-style: none; height: 18px;">Pada Tanggal</td>
<td style="width: 1.5448%; border-style: none; height: 18px;">:</td>
<td style="width: 31.0848%; border-style: none; height: 18px;">{{$data['pada_tanggal']}}</td>
</tr>
</tbody>
</table>
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; {{$data['jabatan_ttd']}}</p>
<p>&nbsp;</p>
<p style="text-align: center;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <span style="text-decoration: underline;">{{$data['nama_ttd']}}</span></p>
<p style="text-align: center;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; NIP. {{$data['nip_ttd']}}</p>