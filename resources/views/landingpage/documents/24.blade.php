<table style="height: 1396px; width: 774px;">
<tbody>
<tr style="height: 23.5px;">
<td style="width: 764px; height: 23.5px;" colspan="6"><img src="{{asset('documents/'.$id.'/kopsurat.png')}}" alt="" width="100%" /><img src="C:\xampp\htdocs\ekios\ekios\public\documents/24/af0a8_muarajawa.png" alt="" width="760" height="143" /></td>
</tr>
<tr style="height: 5px;">
<td style="width: 764px; height: 5px;" colspan="6">
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Muara Jawa, <strong><span style="text-decoration: underline;">{{convertDate(date('Y-m-d'))}}</span></strong>&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</p>
<p>&nbsp; &nbsp; Nomor &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; : {{$data['nomor_surat']}}&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Kepada</p>
<p>&nbsp; &nbsp; Perihal &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;:&nbsp;<strong><u>Dispensasi Nikah</u></strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Yth.&nbsp;&nbsp;&nbsp; Bapak Kepala KUA</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Kecamatan Muara Jawa</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Di &ndash;</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<strong><u>MUARA JAWA</u></strong></p>
</td>
</tr>
<tr style="height: 12px;">
<td style="width: 20.0938px; height: 12px;">&nbsp;</td>
<td style="width: 723.828px; height: 12px; text-align: justify;" colspan="4">&nbsp;
<p>&nbsp; &nbsp; &nbsp; &nbsp;Sesuai &nbsp;dengan &nbsp;&nbsp;permohonan &nbsp;Kepala &nbsp;KUA Kecamatan &nbsp;Muara &nbsp;Jawa Nomor : {{$data['nomor_surat_kua']}} Tanggal {{$data['tanggal_surat_kua']}} , atas pemeriksaan terhadap calon pengantin :</p>
</td>
<td style="width: 8.07812px; height: 12px;">&nbsp;</td>
</tr>
<tr style="height: 12px;">
<td style="width: 20.0938px; height: 12px;">&nbsp;</td>
<td style="width: 723.828px; height: 12px; text-align: justify;" colspan="4">&nbsp;</td>
<td style="width: 8.07812px; height: 12px;">&nbsp;</td>
</tr>
<tr style="height: 12px;">
<td style="width: 20.0938px; height: 12px;">&nbsp;</td>
<td style="width: 723.828px; height: 12px; text-align: justify;" colspan="4"><strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;1. Calon Suami :</strong></td>
<td style="width: 8.07812px; height: 12px;">&nbsp;</td>
</tr>
<tr style="height: 23px;">
<td style="width: 20.0938px; height: 23px;">&nbsp;</td>
<td style="width: 29.125px; height: 23px;">&nbsp;</td>
<td style="width: 186.75px; height: 23px;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Nama</td>
<td style="width: 14.0625px; height: 23px;">:</td>
<td style="width: 475.891px; height: 23px;"><strong>{{$data['nama_calon_suami']}}</strong></td>
</tr>
<tr style="height: 23px;">
<td style="width: 20.0938px; height: 23px;">&nbsp;</td>
<td style="width: 29.125px; height: 23px;">&nbsp;</td>
<td style="width: 186.75px; height: 23px;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Bin</td>
<td style="width: 14.0625px; height: 23px;">:</td>
<td style="width: 475.891px; height: 23px;">{{$data['bin_calon_suami']}}</td>
</tr>
<tr style="height: 23px;">
<td style="width: 20.0938px; height: 23px;">&nbsp;</td>
<td style="width: 29.125px; height: 23px;">&nbsp;</td>
<td style="width: 186.75px; height: 23px;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Tempat/Tgl Lahir</td>
<td style="width: 14.0625px; height: 23px;">:</td>
<td style="width: 475.891px; height: 23px;">{{$data['tempat_lahir_calon_suami']}},{{convertDate(date('Y-m-d',strtotime($data['tanggal_lahir_calon_suami'])))}}</td>
</tr>
<tr style="height: 24px;">
<td style="width: 20.0938px; height: 24px;">&nbsp;</td>
<td style="width: 29.125px; height: 24px;">&nbsp;</td>
<td style="width: 186.75px; height: 24px;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; NIK</td>
<td style="width: 14.0625px; height: 24px;">:&nbsp;</td>
<td style="width: 475.891px; height: 24px;">{{$data['nik_calon_suami']}}</td>
</tr>
<tr style="height: 24px;">
<td style="width: 20.0938px; height: 24px;">&nbsp;</td>
<td style="width: 29.125px; height: 24px;">&nbsp;</td>
<td style="width: 186.75px; height: 24px;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Agama&nbsp;</td>
<td style="width: 14.0625px; height: 24px;">:&nbsp;</td>
<td style="width: 475.891px; height: 24px;">{{$data['agama_calon_suami']}}</td>
</tr>
<tr style="height: 24px;">
<td style="width: 20.0938px; height: 24px;">&nbsp;</td>
<td style="width: 29.125px; height: 24px;">&nbsp;</td>
<td style="width: 186.75px; height: 24px;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Pekerjaan</td>
<td style="width: 14.0625px; height: 24px;">:&nbsp;</td>
<td style="width: 475.891px; height: 24px;">{{$data['pekerjaan_calon_suami']}}</td>
</tr>
<tr style="height: 23px;">
<td style="width: 20.0938px; height: 23px;">&nbsp;</td>
<td style="width: 29.125px; height: 23px;">&nbsp;</td>
<td style="width: 186.75px; height: 23px;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Alamat</td>
<td style="width: 14.0625px; height: 23px;">:</td>
<td style="width: 475.891px; height: 23px;">{{$data['alamat_calon_suami']}}</td>
</tr>
<tr style="height: 12px;">
<td style="width: 20.0938px; height: 12px;">&nbsp;</td>
<td style="width: 723.828px; height: 12px; text-align: justify;" colspan="4">&nbsp;</td>
<td style="width: 8.07812px; height: 12px;">&nbsp;</td>
</tr>
<tr style="height: 12px;">
<td style="width: 20.0938px; height: 12px;">&nbsp;</td>
<td style="width: 723.828px; height: 12px; text-align: justify;" colspan="4"><strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;2. Calon Istri :</strong></td>
<td style="width: 8.07812px; height: 12px;">&nbsp;</td>
</tr>
<tr style="height: 23px;">
<td style="width: 20.0938px; height: 23px;">&nbsp;</td>
<td style="width: 29.125px; height: 23px;">&nbsp;</td>
<td style="width: 186.75px; height: 23px;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Nama</td>
<td style="width: 14.0625px; height: 23px;">:</td>
<td style="width: 475.891px; height: 23px;"><strong>{{$data['nama_calon_istri']}}</strong></td>
</tr>
<tr style="height: 23px;">
<td style="width: 20.0938px; height: 23px;">&nbsp;</td>
<td style="width: 29.125px; height: 23px;">&nbsp;</td>
<td style="width: 186.75px; height: 23px;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Bin</td>
<td style="width: 14.0625px; height: 23px;">:</td>
<td style="width: 475.891px; height: 23px;">{{$data['binti_calon_istri']}}</td>
</tr>
<tr style="height: 23px;">
<td style="width: 20.0938px; height: 23px;">&nbsp;</td>
<td style="width: 29.125px; height: 23px;">&nbsp;</td>
<td style="width: 186.75px; height: 23px;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Tempat/Tgl Lahir</td>
<td style="width: 14.0625px; height: 23px;">:</td>
<td style="width: 475.891px; height: 23px;">&nbsp;{{$data['tempat_lahir_calon_istri']}},{{convertDate(date('Y-m-d',strtotime($data['tanggal_lahir_calon_istri'])))}}</td>
</tr>
<tr style="height: 24px;">
<td style="width: 20.0938px; height: 24px;">&nbsp;</td>
<td style="width: 29.125px; height: 24px;">&nbsp;</td>
<td style="width: 186.75px; height: 24px;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; NIK</td>
<td style="width: 14.0625px; height: 24px;">:&nbsp;</td>
<td style="width: 475.891px; height: 24px;">{{$data['nik_calon_istri']}}</td>
</tr>
<tr style="height: 24px;">
<td style="width: 20.0938px; height: 24px;">&nbsp;</td>
<td style="width: 29.125px; height: 24px;">&nbsp;</td>
<td style="width: 186.75px; height: 24px;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Agama&nbsp;</td>
<td style="width: 14.0625px; height: 24px;">:&nbsp;</td>
<td style="width: 475.891px; height: 24px;">{{$data['agama_calon_istri']}}</td>
</tr>
<tr style="height: 24px;">
<td style="width: 20.0938px; height: 24px;">&nbsp;</td>
<td style="width: 29.125px; height: 24px;">&nbsp;</td>
<td style="width: 186.75px; height: 24px;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Pekerjaan</td>
<td style="width: 14.0625px; height: 24px;">:&nbsp;</td>
<td style="width: 475.891px; height: 24px;">{{$data['pekerjaan_calon_istri']}}</td>
</tr>
<tr style="height: 23px;">
<td style="width: 20.0938px; height: 23px;">&nbsp;</td>
<td style="width: 29.125px; height: 23px;">&nbsp;</td>
<td style="width: 186.75px; height: 23px;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Alamat</td>
<td style="width: 14.0625px; height: 23px;">:</td>
<td style="width: 475.891px; height: 23px;">{{$data['alamat_calon_istri']}}</td>
</tr>
<tr style="height: 25px;">
<td style="width: 20.0938px; height: 25px;">&nbsp;</td>
<td style="width: 723.828px; height: 25px;" colspan="4">&nbsp;</td>
<td style="width: 8.07812px; height: 25px;">&nbsp;</td>
</tr>
<tr style="height: 47px;">
<td style="width: 20.0938px; height: 47px;">&nbsp;</td>
<td style="width: 723.828px; height: 47px;" colspan="4">
<p>{{$data['penjelasan_surat']}}</p>
</td>
<td style="width: 8.07812px; height: 47px;">&nbsp;</td>
</tr>
<tr style="height: 47px;">
<td style="width: 20.0938px; height: 47px;">&nbsp;</td>
<td style="width: 723.828px; height: 47px;" colspan="4">
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Demikian Dispensasi Nikah ini diberikan untuk dapat dipergunakan sebagaimana mestinya.</p>
</td>
<td style="width: 8.07812px; height: 47px;">&nbsp;</td>
</tr>
<tr style="height: 5px;">
<td style="width: 20.0938px; height: 5px;">&nbsp;</td>
<td style="width: 241.938px; height: 5px;" colspan="3">&nbsp;</td>
<td style="width: 475.891px; height: 5px; text-align: center;">&nbsp;</td>
<td style="width: 8.07812px; height: 5px;">&nbsp;</td>
</tr>
<tr style="height: 5px;">
<td style="width: 20.0938px; height: 5px;">&nbsp;</td>
<td style="width: 241.938px; height: 5px;" colspan="3">&nbsp;</td>
<td style="width: 475.891px; height: 5px; text-align: center;">
<p><strong>a.n. CAMAT MUARA JAWA</strong></p>
<p><strong>{{$data['jabatan_ttd']}}</strong></p>
</td>
<td style="width: 8.07812px; height: 5px;">&nbsp;</td>
</tr>
<tr style="height: 18px;">
<td style="width: 20.0938px; height: 18px;">&nbsp;</td>
<td style="width: 241.938px; height: 18px;" colspan="3">&nbsp;</td>
<td style="width: 475.891px; height: 18px; text-align: center;">
<p><strong>&nbsp;</strong></p>
<p><strong>&nbsp;</strong></p>
</td>
<td style="width: 8.07812px; height: 18px;">&nbsp;</td>
</tr>
<tr style="height: 18px;">
<td style="width: 20.0938px; height: 18px;">&nbsp;</td>
<td style="width: 241.938px; height: 18px;" colspan="3">&nbsp;</td>
<td style="width: 475.891px; height: 18px; text-align: center;">
<p><span style="text-decoration: underline;"><strong>{{$data['nama_ttd']}}</strong></span></p>
<p><strong>NIP. {{$data['nip_ttd']}}</strong></p>
</td>
<td style="width: 8.07812px; height: 18px;">&nbsp;</td>
</tr>
</tbody>
</table>