<table style="height: 1210px; width: 702px;">
<tbody>
<tr style="height: 23px;">
<td style="width: 692px; height: 23px;" colspan="6"><img src="" alt="" width="100%" /></td>
</tr>
<tr style="height: 5px;">
<td style="width: 692px; height: 5px; text-align: center;" colspan="6">
<p><strong><u>SURAT KETERANGAN USAHA</u></strong></p>
<p style="text-align: left;">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Nomor : {{{{$data['nomor_surat']}}}}</p>
</td>
</tr>
<tr style="height: 12.5px;">
<td style="width: 10px; height: 12.5px;">&nbsp;</td>
<td style="width: 659.938px; height: 12.5px; text-align: justify;" colspan="4">&nbsp;</td>
<td style="width: 10.0625px; height: 12.5px;">&nbsp;</td>
</tr>
<tr style="height: 12px;">
<td style="width: 10px; height: 12px;">&nbsp;</td>
<td style="width: 659.938px; height: 12px; text-align: justify;" colspan="4"><strong>Yang Bertanda Tangan Dibawah Ini :</strong></td>
<td style="width: 10.0625px; height: 12px;">&nbsp;</td>
</tr>
<tr style="height: 23px;">
<td style="width: 10px; height: 23px;">&nbsp;</td>
<td style="width: 15.0156px; height: 23px;">&nbsp;</td>
<td style="width: 188.188px; height: 23px;">&nbsp; &nbsp; &nbsp; Nama</td>
<td style="width: 11.3125px; height: 23px;">:</td>
<td style="width: 427.422px; height: 23px;">{{{{$data['nama_ttd']}}}}</td>
</tr>
<tr style="height: 23px;">
<td style="width: 10px; height: 23px;">&nbsp;</td>
<td style="width: 15.0156px; height: 23px;">&nbsp;</td>
<td style="width: 188.188px; height: 23px;">&nbsp; &nbsp; &nbsp; Jabatan</td>
<td style="width: 11.3125px; height: 23px;">:</td>
<td style="width: 427.422px; height: 23px;">{{{{$data['jabatan_ttd']}}}}</td>
</tr>
<tr style="height: 12px;">
<td style="width: 10px; height: 12px;">&nbsp;</td>
<td style="width: 659.938px; height: 12px; text-align: justify;" colspan="4">&nbsp;</td>
<td style="width: 10.0625px; height: 12px;">&nbsp;</td>
</tr>
<tr style="height: 12px;">
<td style="width: 10px; height: 12px;">&nbsp;</td>
<td style="width: 659.938px; height: 12px; text-align: justify;" colspan="4">Berdasarkan Surat Pengantar RT. {{{{$data['rt']}}}} Nomor :{{$data['nomor_dari_rt']}}}}, Tanggal {{{{$data['tanggal_dari_rt']}}}} Dengan ini menerangkan dengan sebenarnya bahwa :</td>
<td style="width: 10.0625px; height: 12px;">&nbsp;</td>
</tr>
<tr style="height: 23px;">
<td style="width: 10px; height: 23px;">&nbsp;</td>
<td style="width: 15.0156px; height: 23px;">&nbsp;</td>
<td style="width: 188.188px; height: 23px;">&nbsp; &nbsp; &nbsp; Nama</td>
<td style="width: 11.3125px; height: 23px;">:</td>
<td style="width: 427.422px; height: 23px;">{{{{$data['nama']}}}}</td>
</tr>
<tr style="height: 23px;">
<td style="width: 10px; height: 23px;">&nbsp;</td>
<td style="width: 15.0156px; height: 23px;">&nbsp;</td>
<td style="width: 188.188px; height: 23px;">&nbsp; &nbsp; &nbsp; NIK</td>
<td style="width: 11.3125px; height: 23px;">:</td>
<td style="width: 427.422px; height: 23px;">{{{{$data['nik']}}}}</td>
</tr>
<tr style="height: 23px;">
<td style="width: 10px; height: 23px;">&nbsp;</td>
<td style="width: 15.0156px; height: 23px;">&nbsp;</td>
<td style="width: 188.188px; height: 23px;">&nbsp; &nbsp; &nbsp; Tempat/Tanggal Lahir</td>
<td style="width: 11.3125px; height: 23px;">:</td>
<td style="width: 427.422px; height: 23px;">&nbsp;{{{{$data['tempat_lahir']}}}},{{{{convertDate(date('Y-m-d',strtotime($data['tanggal_lahir'])))}}}}</td>
</tr>
<tr style="height: 24px;">
<td style="width: 10px; height: 24px;">&nbsp;</td>
<td style="width: 15.0156px; height: 24px;">&nbsp;</td>
<td style="width: 188.188px; height: 24px;">&nbsp; &nbsp; &nbsp; Jenis Kelamin</td>
<td style="width: 11.3125px; height: 24px;">:&nbsp;</td>
<td style="width: 427.422px; height: 24px;">{{{{$data['jenis_kelamin']}}}}</td>
</tr>
<tr style="height: 24px;">
<td style="width: 10px; height: 24px;">&nbsp;</td>
<td style="width: 15.0156px; height: 24px;">&nbsp;</td>
<td style="width: 188.188px; height: 24px;">&nbsp; &nbsp; &nbsp; Agama&nbsp;</td>
<td style="width: 11.3125px; height: 24px;">:&nbsp;</td>
<td style="width: 427.422px; height: 24px;">{{{{$data['agama']}}}}</td>
</tr>
<tr style="height: 23px;">
<td style="width: 10px; height: 23px;">&nbsp;</td>
<td style="width: 15.0156px; height: 23px;">&nbsp;</td>
<td style="width: 188.188px; height: 23px;">&nbsp; &nbsp; &nbsp; Alamat</td>
<td style="width: 11.3125px; height: 23px;">:</td>
<td style="width: 427.422px; height: 23px;">{{{{$data['alamat']}}}}</td>
</tr>
<tr style="height: 25px;">
<td style="width: 10px; height: 25px;">&nbsp;</td>
<td style="width: 659.938px; height: 25px;" colspan="4">&nbsp;</td>
<td style="width: 10.0625px; height: 25px;">&nbsp;</td>
</tr>
<tr style="height: 25px;">
<td style="width: 10px; height: 25px;">&nbsp;</td>
<td style="width: 659.938px; height: 25px;" colspan="4">
<p>Yang bersangkutan memang benar ada memiliki usaha :</p>
</td>
<td style="width: 10.0625px; height: 25px;">&nbsp;</td>
</tr>
<tr style="height: 25px;">
<td style="width: 10px; height: 25px;">&nbsp;</td>
<td style="width: 659.938px; height: 25px;" colspan="4">
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Nama Usaha&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: {{{{$data['nama_usaha']}}}}</p>
</td>
<td style="width: 10.0625px; height: 25px;">&nbsp;</td>
</tr>
<tr style="height: 25px;">
<td style="width: 10px; height: 25px;">&nbsp;</td>
<td style="width: 659.938px; height: 25px;" colspan="4">
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Jenis Usaha&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; : {{{{$data['jenis_usaha']}}}}</p>
</td>
<td style="width: 10.0625px; height: 25px;">&nbsp;</td>
</tr>
<tr style="height: 25px;">
<td style="width: 10px; height: 25px;">&nbsp;</td>
<td style="width: 659.938px; height: 25px;" colspan="4">
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Alamat Usaha&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: {{{{$data['alamat_usaha']}}}}</p>
</td>
<td style="width: 10.0625px; height: 25px;">&nbsp;</td>
</tr>
<tr style="height: 47px;">
<td style="width: 10px; height: 47px;">&nbsp;</td>
<td style="width: 659.938px; height: 47px;" colspan="4">&nbsp;</td>
<td style="width: 10.0625px; height: 47px;">&nbsp;</td>
</tr>
<tr style="height: 47px;">
<td style="width: 10px; height: 47px;">&nbsp;</td>
<td style="width: 659.938px; height: 47px;" colspan="4">
<p>&nbsp; &nbsp; &nbsp; &nbsp; Demikian Surat Keterangan Usaha ini dibuat dengan sebenarnya dan diberikan kepada yang bersangkutan untuk dapat dipergunakan sebagaimana mestinya.</p>
</td>
<td style="width: 10.0625px; height: 47px;">&nbsp;</td>
</tr>
<tr style="height: 5px;">
<td style="width: 10px; height: 5px;">&nbsp;</td>
<td style="width: 226.516px; height: 5px;" colspan="3">&nbsp;</td>
<td style="width: 427.422px; height: 5px; text-align: center;">&nbsp;</td>
<td style="width: 10.0625px; height: 5px;">&nbsp;</td>
</tr>
<tr style="height: 5px;">
<td style="width: 10px; height: 5px;">&nbsp;</td>
<td style="width: 226.516px; height: 5px;" colspan="3">&nbsp;</td>
<td style="width: 427.422px; height: 5px; text-align: left;">
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Dibuat di&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;: {{{{$data['dibuat_di']}}}}</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<span style="text-decoration: underline;">Pada tanggal&nbsp; &nbsp; : {{{{$data['tanggal_surat_dibuat']}}}} </span>&nbsp;</p>
<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <strong>{{{{$data['jabatan_ttd']}}}}</strong></p>
</td>
<td style="width: 10.0625px; height: 5px;">&nbsp;</td>
</tr>
<tr style="height: 18px;">
<td style="width: 10px; height: 18px;">&nbsp;</td>
<td style="width: 226.516px; height: 18px;" colspan="3">&nbsp;</td>
<td style="width: 427.422px; height: 18px; text-align: center;">
<p><strong>&nbsp;</strong></p>
<p><strong>&nbsp;</strong></p>
</td>
<td style="width: 10.0625px; height: 18px;">&nbsp;</td>
</tr>
<tr style="height: 18px;">
<td style="width: 10px; height: 18px;">&nbsp;</td>
<td style="width: 226.516px; height: 18px;" colspan="3">&nbsp;</td>
<td style="width: 427.422px; height: 18px; text-align: center;">
<p style="text-align: left;"><strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<span style="text-decoration: underline;">{{{{$data['nama_ttd']}}}}</span></strong></p>
<p style="text-align: left;"><strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; NIP.&nbsp;{{{{$data['nip_ttd']}}}}</strong></p>
</td>
<td style="width: 10.0625px; height: 18px;">&nbsp;</td>
</tr>
</tbody>
</table>