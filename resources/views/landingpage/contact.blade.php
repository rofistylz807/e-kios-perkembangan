@extends('component.landingpage.main')
@section('content')
<div class="col-12">
    <div class="row">
        <div class="col-12">
            <p>
              
                    <h4><b>Punya Pertanyaan, Saran Dan Masukan Untuk Aplikasi DiSAPA I-DAMAN Kutai Kartanegara ?</b></h4>
                
            </p>
            <hr>
        </div>
        <form class="col-md-6" action="{{url('question/store')}}" method="post"
            onsubmit="return validateUrl('question/validate')">
            @csrf
            <div class="form-group">
                <label><h5>Silahkan Di Sampaikan Melalui Kolom Dibawah ini :</h5></label>
                <textarea class="form-control" placeholder="Pesan" name="pesan" cols='30' rows='10'></textarea>
                <div class="alert alert-light fade show p-1" id="pesan" hidden role="alert"></div>
            </div>
            <br>
            <input type="submit" class="btn btn-success" value="Kirim">
        </form>
        <div class="col-lg-4">
            <div class="media contact-info">
                <span class="contact-info__icon"><i class="ti-tablet"></i></span>
                <div class="media-body">
                    <h4>Alamat :</h4>
                    JALAN WOLTER MONGINSIDI NOMOR 01 TENGGARONG
                    {{-- <p>{{$data->alamat}}</p> --}}
                </div>
            </div>
            <hr>
            <div class="media contact-info">
                <span class="contact-info__icon"><i class="ti-tablet"></i></span>
                <div class="media-body">
                    <h4>Hubungi Kami :</h4>
                    (0541) 2090020-28
                    {{-- <p>{{$data->telepon}}</p> --}}
                </div>
            </div>
            <hr>
            <div class="media contact-info">
                <span class="contact-info__icon"><i class="ti-email"></i></span>
                <div class="media-body">
                    <h4>Email :</h4>
                    bagianorganisasi1020@gmail.com
                    {{-- <p>{{$data->email}}</p> --}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
