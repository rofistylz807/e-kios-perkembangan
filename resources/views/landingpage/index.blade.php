@extends('component.landingpage.main')
@section('content')
@foreach($menu as $value)
<div class="col-lg-4 mb-5">
    <a href="{{$value->url}}">
        <div class="card bg-white border-0 h-100">
           <br>
            <div class="card-body pt-0 pt-lg-0 row">
                <div class="col-md-12 bg-icon">
                   <center> <img src="{{$value->img}}" width="50%"></center>
                </div>
                <div class="">
                    <br>
                    <h2 class="fs-5 fw-bold text-warning font-icon">{{$value->title}}</h2>
                    <p class="mb-0 text-dark ">{{$value->description}}
                    </p>
                </div>

            </div>
        </div>
    </a>
</div>
@endforeach
@endsection
