@extends('component.landingpage.main')
@section('content')
<h2>{{$menu->title}}</h2>
<h5>{{$menu->description}}</h5>
<hr>
<form  name="form" action="{{url('form/'.$menu->id)}}"
    onsubmit="return validateForm('{{json_encode(json_decode($form->data,true)['validate'])}}')" method="post" enctype="multipart/form-data">
    @csrf
    <br>
    @foreach(json_decode($form->data,true)['form'] as $key=>$value)
    <div class="form-group ">
        <label><b>{{strtoupper($value['name'])}}:</b></label>
        @if($value['field']=='text')
        <input type="text" name="{{$key}}" class="form-control border-primary border-bold" style="border-width:3px !important;">
        @elseif($value['field']=='number')
        <input type="number" name="{{$key}}" class="form-control border-primary border-bold" style="border-width:3px !important;">
        @elseif($value['field']=='date')
        <input type="date" name="{{$key}}" class="form-control border-primary border-bold" style="border-width:3px !important;">
        @elseif($value['field']=='file')
        <input type="file" name="{{$key}}" class="form-control border-primary border-bold file" style="border-width:3px !important;" accept="application/pdf" required>
        @elseif($value['field']=='select')
        <select class="form-control border-primary border-bold" style="border-width:3px !important;" name="{{$key}}">
            <option></option>
            @foreach($value['option'] as $sk=>$sv)
            <option value="{{$sk}}">{{$sv}}</option>
            @endforeach
        </select>
        @elseif($value['field']=='textarea')
        <textarea name="{{$key}}" class="form-control border-primary border-bold" style="border-width:3px !important;"></textarea>
        @endif
        <div class="alert alert-transparent fade show p-1" id="{{$key}}" role="alert"></div>
    </div>
    @endforeach
    <div class="form-group mb-5 mt-3">
        <input class="btn btn-success" type="submit" value="Kirim">
    </div>
</form>
@endsection
