@extends('component.login.main')
@section('content')
<div class="wrapper">
    <div class="section-authentication">
        <div class="container-fluid">
            <div class="card mb-0">
                <div class="card-body p-0">
                    <div class="row no-gutters">
                        <div class="col-12 col-lg-5 col-xl-4 d-flex align-items-stretch">
                            <div class="card mb-0 shadow-none bg-transparent w-100 login-card rounded-0">
                                <div class="card-body p-md-5">
                                    <img src="{{asset('img/E-KIOS Kutai Kartanegara.png')}}" width="180" alt="" />
                                    <h4 class="mt-5"><strong>Selamat Datang</strong></h4>
                                    <form action="{{url('admin/login')}}" method="post">
                                        @csrf
                                        <div class="form-group mt-4">
                                            <label>Username</label>
                                            <input type="text" name="username" class="form-control"
                                                placeholder="Masukan Username" />
                                        </div>
                                        <div class="form-group">
                                            <label>Password</label>
                                            <input type="password" name="password" class="form-control"
                                                placeholder="Masukan Password" />
                                        </div>
                                        <div class="form-row">
                                            <div class="form-group col text-right">
                                                <a href="{{url('forgotpassword')}}"><i
                                                        class='bx bxs-key mr-2'></i>Lupa Password?</a>
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-block mt-3"><i
                                                class='bx bxs-lock mr-1'></i>Login</button>
                                    </form>
                                    <a href="{{url('/')}}" class="btn btn-success btn-block mt-4">Kembali Ke Halaman Utama</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-7 col-xl-8 d-flex align-items-stretch">
                            <div class="card mb-0 shadow-none bg-transparent w-100 rounded-0">
                                <div class="card-body p-md-5">
                                    <div class="text-center"><img
                                            src="{{asset('landingpage/10.png')}}"
                                            class="img-fluid" alt="" /></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer bg-transparent px-md-5">
                    <div class="d-flex align-items-center justify-content-between flex-wrap">
                        <ul class="list-inline mb-0">
                        </ul>
                        <p class="mb-0"> <b>Copyright © 2021 Hak Cipta Bagian Organisasi Sekretariat Daerah Dikembangkan Bersama Diskominfo Kukar</b>
                        </p>
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item"><a href="{{url('contact')}}">Kontak</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
