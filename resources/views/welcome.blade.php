<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Document</title>
</head>
<body>
    <div id="apppage">
        @{{ message }} <br>
        <form onsubmit="return false;" @submit="name">
            <input type="text" name="passphrase" placeholder="pass" v-model="form.passphrase"> <br>
            <input type="file" name="file" @change="(e) => onChangeFile(e, 'file')">
            <button type="submit" v-if="!loading">UPLOAD</button>
            <button type="submit" disabled v-else>LOADING DATA</button>
        </form>
    </div>
    
    <script src="{{asset('synadmin/assets/js/jquery.min.js')}}"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue@2.6.14/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js"></script>
    <script>
        var app = new Vue({
            el: '#apppage',
            data() {
                return {
                    loading: false,
                    form: {
                        file: '',
                        passphrase: '',
                    },
                    file: '',
                    message: '',
                };
            },
            methods:{
                async name() {
                    try {
                        this.loading = true;
                        let formData = new FormData();
                        const reGenerate = Object.keys(this.form);
                        formData.append('file', this.form.file);
                        formData.append('passphrase', this.form.passphrase);
                        const data = await axios({
                            method: 'post',
                            url: "{{ url('/upload/bsre') }}",
                            data: formData,
                            headers: {
                                'Content-Type': `multipart/form-data ; boundary=${formData._boundary}`,
                            }
                        });
                        const result = data;
                        console.log(result)
                        if (result.data.message) {
                            this.message = result.data.message+' ID Dokumen '+result.data.id_dokumen
                        }
                        this.loading = false;
                    } catch (error) {
                        console.log(error);
                        this.loading = false;
                    }
                },
                onChangeFile(e, formTarget){
                    const self = this;
                    let file = e.target.files[0];
                    if(file){
                        self.form[formTarget] = file;
                        var reader = new FileReader();
                        reader.onload = function(e) {
                            let generateFile = self.file;
                            generateFile[formTarget] = true;
                            self.file = generateFile;
                        }
                        reader.readAsDataURL(file);
                    }
                }
            }
        })
    </script>
</body>
</html>