<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>E-Kios Kutai Kartanegara</title>
    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="{{asset('synadmin/assets/images/favicon-32x32.png')}}" />
    <!-- Bootstrap icons-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="{{asset('landingpage/css/styles.css')}}" rel="stylesheet" />
</head>

<body style="background-image: url(../landingpage/belakang2.jpg); background-size: cover;
background-position: center center;
background-attachment: fixed; ">

    {{-- <div class="wrap" style="background-image: url(../img/6.jpg); width:500px; height:100px; background-size: cover;
    background-position: center center;
    background-attachment: fixed; ">
        <div class="container"> --}}
            <center>
            <img src="{{asset('landingpage/10.png')}}" style=" width: 100%;
            height: 60%;  "/></center>
            {{-- <div class="row">
                <div class="col-md-6 d-flex align-items-center">
                    <p class="mb-0 phone pl-md-2"> --}}
                        {{-- <a href="#" class="mr-2"><span class="fa fa-phone mr-1"></span> +00 1234 567</a> 
                        <a href="#"><span class="fa fa-paper-plane mr-1"></span> youremail@email.com</a> --}}
                    {{-- </p>
                </div>
                <div class="col-md-6 d-flex justify-content-md-end">
                    <div class="social-media">
                    <p class="mb-0 d-flex">
                        <a href="#" class="d-flex align-items-center justify-content-center"></a>
                        <a href="#" class="d-flex align-items-center justify-content-center"></a>
                        <a href="#" class="d-flex align-items-center justify-content-center"></a> --}}
                        {{-- <a href="#" class="d-flex align-items-center justify-content-center"><span class="fa fa-dribbble"><i class="sr-only">Dribbble</i></span></a> --}}
                    {{-- </p>
            </div>
           
                </div>
            </div>
        </div>
    </div> --}}
    <!-- Responsive navbar-->
    <nav class="navbar navbar-expand-lg navbar-dark"  >
        <div class="container-fluid">
            <a href="{{url('/')}}"><img src="{{asset('landingpage/E-KIOS Kutai Kartanegara3.png')}}" width="300" alt="" /></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ms-auto mb-2 mb-lg-0">
                    <li class="nav-item"><b><a class="nav-link text-dark" href="{{url(Auth::check()?'dashboard':'login')}}">{{Auth::check()?strtoupper(Auth::User()->name):"Login"}}</a></b></li>
                    <!-- <li class="nav-item"><a class="nav-link" href="#!">About</a></li> -->
                    <li class="nav-item"><b><a class="nav-link text-dark" href="{{url('contact')}}">Kontak</a></b></li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Page Content-->
    <section class="pt-4"  >
        
        <div class="px-lg-5">
            <!-- Page Features-->
            <div class="row w-100 container-fluid">
                @yield('content')
            </div>
        </div>
    </section>
    <!-- Footer-->
    <footer class="footer">
        {{-- <div class="download_brochure">
            <div class="container">
                <div class="bordered_1px">
                        <div class="row">
                                <div class="col-lg-6 col-md-6">
                                        <div class="footer_logo">
                                                <a href="#">
                                                    <img src="img/footer_logo.png" alt="">
                                                </a>
                                            </div>
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <div class="download_btn">
                                        <a href="#"> <img src="img/icon/down.svg" alt=""> Download Brochure</a>
                                    </div>
                                </div>
                            </div>
                </div>
                
            </div>
        </div> --}}
        <br>
        <div class="footer_top">
            <div class="container">
              <div class="footer_border"></div>
                <div class="row">
                    <div class="col-xl-12">
                        
                       
            <p class="m-0 text-center"><b>Di Dukung Oleh :</b> <img src="{{asset('img/bsre.png')}}" height="60"></p>
                     <hr>
                    </div>
                </div>
                </div>
            </div>
        </div>
        <div class="copy-right_text">
            <div class="container">
                <div class="footer_border"></div>
                <div class="row">
                    <div class="col-xl-12">
                        <p class="copy_right text-center">
                            <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            <b>Copyright © 2021 Hak Cipta Bagian Organisasi Sekretariat Daerah Dikembangkan Bersama Diskominfo Kukar</b>
      <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                        </p>
                    </div>
                </div>
            </div>
        </div>
      </footer>
    {{-- <footer class="py-2 foot">
        <div class="container">
            
            <p class="m-0 text-center">Di Dukung Oleh : <img src="{{asset('img/bsre.png')}}" height="60"></p>
<hr>
            <p class="m-0 text-center">Copyright © 2021 Hak Cipta Bagian Organisasi Sekretariat Daerah Dikembangkan Bersama Diskominfo Kukar</p>
            
        </div>
    </footer> --}}
    <!-- Bootstrap core JS-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"></script>
    <script src="{{asset('synadmin/assets/js/jquery.min.js')}}"></script>
	<script src="{{asset('plugins/sweetalert/sweetalert.min.js')}}"></script>
    <!-- Core theme JS-->
    <script src="{{asset('landingpage/js/scripts.js')}}"></script>
    <script>
		@if(Session::has('alert'))
		swal({
			title: "{{ json_decode(Session::get('alert'),true)['data'] }}",
			icon: "{{ json_decode(Session::get('alert'),true)['status'] }}",
		});
		@endif
	</script>
</body>

</html>
