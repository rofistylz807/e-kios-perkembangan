<!DOCTYPE html>
<html>

<head>
    <style>
        .button-container {
            text-align: center;
        }

        .button {
            position: relative;
            background: currentColor;
            border: 1px solid currentColor;
            font-size: 1.1rem;
            color: #0baa0b;
            margin: 3rem 0;
            padding: 0.75rem 3rem;
            cursor: pointer;

            overflow: hidden;
            box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12);
        }

        .button span {
            color: #fff;
            position: relative;
            z-index: 1;
        }

        .circle {
            width: auto;
            height: 550px;
            box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 3px 1px -2px rgba(0, 0, 0, 0.2), 0 1px 5px 0 rgba(0, 0, 0, 0.12);
            background-color: #ffffff;
            border-radius: 20% 20% 20% 20%;
            float: center;
        }

    </style>

</head>

<body>
    <center>
        <div class="circle">
            <img width="300px" height="100px" src="{{asset('img/E-KIOS Kutai Kartanegara.png')}}">
            <br>
            <h2>
                <font face="calibri">Reset Password Akun DiSAPA I-DAMAN</font>
            </h2>
            <hr color="green">
            <p>
                <font size="5%" face="calibri">Kami telah menerima permintaan kamu untuk reset password akun DiSAPA I-DAMAN.
                    Silahkan konfirmasi lewat tombol dibawah ini!<font face="calibri">
            </p>
            <br>
            <a href="{{$url}}" class="button"><span>Reset Password</span></a>

            <p>
                <font size="5%" face="calibri"> Abaikan email ini jika kamu tidak pernah meminta untuk reset password.
                    <font face="calibri">
            </p>

        </div>

        <p>Copyright © 2021 Hak Cipta Bagian Organisasi Sekretariat Daerah Dikembangkan Bersama Diskominfo Kukar</p>
    </center>
