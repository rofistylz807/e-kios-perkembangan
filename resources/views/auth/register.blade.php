@extends('component.login.main')
@section('content')
<div class="wrapper">
    <div class="section-authentication">
        <div class="container-fluid">
            <div class="card mb-0">
                <div class="card-body p-0">
                    <div class="row no-gutters">
                        <div class="col-12 col-lg-5 col-xl-4 d-flex align-items-stretch">
                            <div class="card mb-0 shadow-none bg-transparent w-100 login-card rounded-0">
                                <div class="card-body p-md-5">
                                    <img src="{{asset('landingpage/E-KIOS Kutai Kartanegara3.png')}}" width="180" alt="" />
                                    <img src="{{asset('img/bsre.png')}}" height="60">
                                    <h4 class="mt-5"><strong>Pendaftaran</strong></h4>
                                    <form action="{{url('register')}}" method="post"
                                        onsubmit="return validateRegister()">
                                        @csrf
                                        <div class="form-group mt-4">
                                            <label>NIK</label>
                                            <input type="text" class="form-control" placeholder="NIK" name="nik" />
                                            <div class="alert alert-light fade show p-1" id="nik" role="alert">
                                            </div>
                                        </div>
                                        <div class="form-group mt-4">
                                            <label>Nama Lengkap</label>
                                            <input type="text" class="form-control" placeholder="Nama Lengkap"
                                                name="nama" />
                                            <div class="alert alert-light fade show p-1" id="nama" role="alert"></div>
                                        </div>
                                        <div class="form-group mt-4">
                                            <label>Nomor HP</label>
                                            <input type="number" class="form-control" placeholder="Nomor HP"
                                                name="nomor_hp" />
                                            <div class="alert alert-light fade show p-1" id="nomor_hp" role="alert">
                                            </div>
                                        </div>
                                        <button type="submit" class="btn btn-primary btn-block mt-4">Daftar</button>
                                    </form>
                                    <div class="text-center mt-4">
                                        <p class="mb-0">Sudah Memiliki Akun? <a href="{{url('login')}}">Login</a>
                                        </p>
                                    </div>

                                    <a href="{{url('/')}}" class="btn btn-success btn-block mt-4">Kembali Ke Halaman
                                        Utama</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-lg-7 col-xl-8 d-flex align-items-stretch">
                            <div class="card mb-0 shadow-none bg-transparent w-100 rounded-0">
                                <img src="{{asset('synadmin/assets/images/login-images/auth-img-8.png')}}"
                                    class="card-img-top" alt="" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer bg-transparent px-md-5">
                    <div class="d-flex align-items-center justify-content-between flex-wrap">
                        <ul class="list-inline mb-0">
                        </ul>
                        <p class="mb-0">Copyright © 2021 Hak Cipta Bagian Organisasi Sekretariat Daerah Dikembangkan Bersama Diskominfo Kukar
                        </p>
                        <ul class="list-inline mb-0">
                            <li class="list-inline-item"><a href="{{url('contact')}}">Kontak</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
