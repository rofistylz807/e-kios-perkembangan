<!DOCTYPE html>
<html lang="id">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title>E-Kios Kutai Kartanegara</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!--favicon-->
    <link rel="icon" href="{{asset('synadmin/assets/images/favicon-32x32.png')}}">
    <!--plugins-->
    <link href="{{asset('synadmin/assets/plugins/datatable/css/dataTables.bootstrap4.min.css')}}" rel="stylesheet"
        type="text/css">

    <link href="{{asset('synadmin/assets/plugins/simplebar/css/simplebar.css')}}" rel="stylesheet" />
    <link href="{{asset('synadmin/assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css')}}" rel="stylesheet" />
    <link href="{{asset('synadmin/assets/plugins/metismenu/css/metisMenu.min.css')}}" rel="stylesheet" />
    <!-- loader-->
    <link href="{{asset('synadmin/assets/css/pace.min.css')}}" rel="stylesheet" />
    <script src="{{asset('synadmin/assets/js/pace.min.js')}}"></script>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('synadmin/assets/css/bootstrap.min.css')}}" />
    <!-- Icons CSS -->
    <link rel="stylesheet" href="{{asset('synadmin/assets/css/icons.css')}}" />
    <!-- App CSS -->
    <link rel="stylesheet" href="{{asset('synadmin/assets/css/app.css')}}" />
    <link rel="stylesheet" href="{{asset('synadmin/assets/css/dark-style.css')}}" />
    <link href="{{asset('synadmin/assets/plugins/select2/css/select2.min.css')}}" rel="stylesheet" />
    <link href="{{asset('synadmin/assets/plugins/select2/css/select2-bootstrap4.css')}}" rel="stylesheet" />
</head>

<body>
    <!-- wrapper -->
    <div class="wrapper">
        <!--header-->
        <header class="top-header">
            <nav class="navbar navbar-expand">
                <div class="left-topbar d-flex align-items-center">
                    <a href="javaScript:;" class="toggle-btn"> <i class="bx bx-menu"></i>
                    </a>
                    <div class="logo-dark">
                        <img src="{{asset('img/E-KIOS Kutai Kartanegara.png')}}" class="logo-icon" alt="">
                    </div>
                </div>
                <div class="flex-grow-1 search-bar">
                    <div class="input-group">
                        <div class="input-group-prepend search-arrow-back">
                            <button class="btn btn-search-back" type="button"><i class="bx bx-arrow-back"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="right-topbar ml-auto">
                    <ul class="navbar-nav">
                        <li class="nav-item dropdown dropdown-user-profile">
                            <a class="nav-link dropdown-toggle dropdown-toggle-nocaret" href="javaScript:;"
                                data-toggle="dropdown">
                                <div class="media user-box align-items-center">
                                    <div class="media-body user-info">
                                        <p class="user-name mb-0"><i class="bx bx-user"></i></p>
                                    </div>
                                </div>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="{{url('setting')}}"><i
                                        class="bx bx-cog"></i><span>Pengaturan</span></a>
                                <a class="dropdown-item" href="{{url('/')}}"><i
                                        class="bx bx-tachometer"></i><span>Halaman Utama</span></a>


                                <div class="dropdown-divider mb-0"></div> <a class="dropdown-item"
                                    href="{{url('logout')}}"><i class="bx bx-power-off"></i><span>Logout</span></a>
                            </div>
                        </li>
                        
                    </ul>
                </div>
            </nav>
        </header>
        <!--end header-->
        <!--page-wrapper-->
        <div class="page-wrapper">
            <!--sidebar-wrapper-->
            <div class="sidebar-wrapper" data-simplebar="true">
                <div class="sidebar-header">
                    <a href="javaScript:;" class="toggle-btn"> <i class="bx bx-menu"></i>
                    </a>
                    <div class="logo-dark">
                        <img src="{{asset('img/E-KIOS Kutai Kartanegara.png')}}" class="logo-icon-2" alt="">
                    </div>
                </div>
                <!--navigation-->
                <ul class="metismenu" id="menu">
                    <li class="menu-label">Menu</li>
                    <li>
                        <a href="{{url('request')}}">
                            <div class="parent-icon"><i class="bx bx-group"></i>
                            </div>
                            <div class="menu-title">Permohonan</div>
                        </a>
                    </li>
                    <li>
                        <a href="{{url('question')}}">
                            <div class="parent-icon"><i class="bx bx-group"></i>
                            </div>
                            <div class="menu-title">Pertanyaan</div>
                        </a>
                    </li>
                    <!-- skpd only -->
                    @if(Auth::User()->skpd_id!='')
                    <li>
                        <a class="has-arrow" href="javaScript:;">
                            <div class="parent-icon"><i class="bx bx-group"></i>
                            </div>
                            <div class="menu-title">Tanggapan</div>
                        </a>
                        <ul class="">
                            <li> <a href="{{url('response')}}"><i class="bx bx-right-arrow-alt"></i>Permohonan</a>
                            </li>
                            <li> <a href="{{url('response/history')}}"><i class="bx bx-right-arrow-alt"></i>Riwayat</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="has-arrow" href="javaScript:;">
                            <div class="parent-icon"><i class="bx bx-group"></i>
                            </div>
                            <div class="menu-title">Tanda Tangan</div>
                        </a>
                        <ul class="">
                            <li> <a href="{{url('sign')}}"><i class="bx bx-right-arrow-alt"></i>Permohonan</a>
                            </li>
                            <li> <a href="{{url('sign/history')}}"><i class="bx bx-right-arrow-alt"></i>Riwayat</a>
                            </li>
                        </ul>
                    </li>
                    @endif
                    <!-- admin only -->
                    @if(Auth::User()->role=='admin')
                    <li>
                        <a class="has-arrow" href="javaScript:;">
                            <div class="parent-icon"><i class="bx bx-group"></i>
                            </div>
                            <div class="menu-title">Admin</div>
                        </a>
                        <ul class="">
                            <li> <a href="{{url('formats')}}"><i class="bx bx-right-arrow-alt"></i>Format Surat</a>
                            </li>
                            <li> <a href="{{url('menus')}}"><i class="bx bx-right-arrow-alt"></i>Menu</a>
                            </li>
                            <li> <a href="{{url('user')}}"><i class="bx bx-right-arrow-alt"></i>User</a>
                            </li>
                            <li> <a href="{{url('skpd')}}"><i class="bx bx-right-arrow-alt"></i>SKPD</a>
                            </li>
                            <li> <a href="{{url('contact/edit')}}"><i class="bx bx-right-arrow-alt"></i>Kontak</a>
                            </li>
                            <li> <a href="{{url('survey')}}"><i class="bx bx-right-arrow-alt"></i>Survey</a>
                            </li>
                        </ul>
                    </li>
                    @endif
                </ul>
                <!--end navigation-->
            </div>
            <!--end sidebar-wrapper-->
            <!--page-content-wrapper-->
            <div class="page-content-wrapper">
                <div class="page-content">
                    @yield('content')

                </div>
            </div>
            <!--end page-content-wrapper-->
        </div>
        <!--end page-wrapper-->
        <!--start overlay-->
        <div class="overlay toggle-btn-mobile"></div>
        <!--end overlay-->
        <!--footer -->
        <div class="footer">
            <p class="mb-0">Copyright © 2021 Hak Cipta Bagian Organisasi Sekretariat Daerah Dikembangkan Bersama Diskominfo Kukar
            </p>
        </div>
        <!-- end footer -->
    </div>
    <!-- end wrapper -->



    <!-- JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{asset('synadmin/assets/js/jquery.min.js')}}"></script>
    <script src="{{asset('synadmin/assets/js/popper.min.js')}}"></script>
    <script src="{{asset('synadmin/assets/js/bootstrap.min.js')}}"></script>
    <!--plugins-->
    <script src="{{asset('synadmin/assets/plugins/simplebar/js/simplebar.min.js')}}"></script>
    <script src="{{asset('synadmin/assets/plugins/metismenu/js/metisMenu.min.js')}}"></script>
    <script src="{{asset('synadmin/assets/plugins/perfect-scrollbar/js/perfect-scrollbar.js')}}"></script>
    <script src="{{asset('synadmin/assets/plugins/select2/js/select2.min.js')}}"></script>
    <script src="{{asset('plugins/sweetalert/sweetalert.min.js')}}"></script>
    {{-- <script src="https://cdn.tiny.cloud/1/2xtsdv1vhupvql9bsm03jy0o4kodi2pufmqqd6713kj3rjgh/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script> --}}
    <script src=//cloud.tinymce.com/stable/tinymce.min.js> </script> </script> 

    <!--Data Tables js-->
    <script src = "{{asset('synadmin/assets/plugins/datatable/js/jquery.dataTables.min.js')}}"></script>

    <!-- App JS -->
    <script src="{{asset('synadmin/assets/js/app.js')}}"></script>
    <script src="{{asset('synadmin/assets/js/script.js')}}"></script>
    @if(isset($js))
    <script src="{{asset($js)}}"></script>
    @endif
    <script>
        @if(Session::has('alert'))
        swal({
            title: "{{ json_decode(Session::get('alert'),true)['data'] }}",
            icon: "{{ json_decode(Session::get('alert'),true)['status'] }}",
        });
        @endif

    </script>
    <style>
        .hidden {
            display: none;
        }

    </style>
    @yield('script')
</body>

</html>
