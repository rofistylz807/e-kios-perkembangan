@extends('component.login.main')
@section('content')
<div class="wrapper bg-forgot">
    <div class="authentication-forgot d-flex align-items-center justify-content-center">
        <div class="card shadow-lg forgot-box">
            <div class="card-body p-md-5">
                <div class="text-center">
                    <img src="{{asset('synadmin/assets/images/icons/forgot.png')}}" width="150" alt="" />
                </div>
                <form action="{{url('/forgotpassword/store/'.$code)}}" method="post">
                    @csrf
                    <div class="form-group">
                        <label>Password Baru</label>
                        <input type="password" class="form-control form-control-lg" placeholder="Password Baru" name="password"/>
                    </div>
                    <div class="form-group">
                        <label>Konfirmasi Password Baru</label>
                        <input type="password" class="form-control form-control-lg" placeholder="Konfirmasi Password Baru" name="konfirmasi_password"/>
                    </div>
                    <button type="submit" class="btn btn-primary btn-lg btn-block">Kirim</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
