@extends('component.login.main')
@section('content')
<div class="wrapper bg-forgot">
    <div class="authentication-forgot d-flex align-items-center justify-content-center">
        <div class="card shadow-lg forgot-box">
            <div class="card-body p-md-5">
                <div class="text-center">
                    <img src="{{asset('synadmin/assets/images/icons/forgot.png')}}" width="150" alt="" />
                </div>
                <h4 class="mt-5 font-weight-bold">Lupa Password?</h4>
                <p class="text-muted">Masukan email yang sudah terdaftar untuk mengatur ulang password</p>
                <form action="{{url('/forgotpassword')}}" method="post">
                    @csrf
                    <div class="form-group mt-5">
                        <label>Email</label>
                        <input type="email" class="form-control form-control-lg" placeholder="email" name="email"/>
                    </div>
                    <button type="submit" class="btn btn-primary btn-lg btn-block">Kirim</button>
                </form>
                <a href="{{url('login')}}" class="btn btn-link btn-block"><i class='bx bx-arrow-back mr-1'></i>Kembali
                    ke Login</a>
            </div>
        </div>
    </div>
</div>
@endsection
