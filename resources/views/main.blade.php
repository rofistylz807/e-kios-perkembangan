<!DOCTYPE html>
<html lang=id">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title>E-KIOS Kutai Kartanegara</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <!--favicon-->
    <link rel="icon" href="{{asset('synadmin/assets/images/favicon-32x32.png')}}" type="image/png" />
    <!-- loader-->
    <link href="{{asset('synadmin/assets/css/pace.min.css')}}" rel="stylesheet" />
    <script src="{{asset('synadmin/assets/js/pace.min.js')}}"></script>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="{{asset('synadmin/assets/css/bootstrap.min.css')}}" />
    <!-- Icons CSS -->
    <link rel="stylesheet" href="{{asset('synadmin/assets/css/icons.css')}}" />
    <!-- App CSS -->
    <link rel="stylesheet" href="{{asset('synadmin/assets/css/app.css')}}" />
    <link rel="stylesheet" href="{{asset('synadmin/assets/css/dark-style.css')}}" />
</head>

<body>
    <!-- wrapper -->
    @yield('content')
    <!-- end wrapper -->

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{asset('synadmin/assets/js/jquery.min.js')}}"></script>
	<script src="{{asset('plugins/sweetalert/sweetalert.min.js')}}"></script>
	<script src="{{asset('synadmin/assets/js/script.js')}}"></script>
	<script>
		@if(Session::has('alert'))
		swal({
			title: "{{ json_decode(Session::get('alert'),true)['data'] }}",
			icon: "{{ json_decode(Session::get('alert'),true)['status'] }}",
		});
		@endif
        $(document).ready(function () {
			$("#show_hide_password a").on('click', function (event) {
				event.preventDefault();
				if ($('#show_hide_password input').attr("type") == "text") {
					$('#show_hide_password input').attr('type', 'password');
					$('#show_hide_password i').addClass("bx-hide");
					$('#show_hide_password i').removeClass("bx-show");
				} else if ($('#show_hide_password input').attr("type") == "password") {
					$('#show_hide_password input').attr('type', 'text');
					$('#show_hide_password i').removeClass("bx-hide");
					$('#show_hide_password i').addClass("bx-show");
				}
			});

            $("#show_hide_password_conf a").on('click', function (event) {
				event.preventDefault();
				if ($('#show_hide_password_conf input').attr("type") == "text") {
					$('#show_hide_password_conf input').attr('type', 'password');
					$('#show_hide_password_conf i').addClass("bx-hide");
					$('#show_hide_password_conf i').removeClass("bx-show");
				} else if ($('#show_hide_password_conf input').attr("type") == "password") {
					$('#show_hide_password_conf input').attr('type', 'text');
					$('#show_hide_password_conf i').removeClass("bx-hide");
					$('#show_hide_password_conf i').addClass("bx-show");
				}
			});
		});
	</script>


</body>

</html>
