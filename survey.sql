-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 31 Jul 2021 pada 00.49
-- Versi Server: 10.1.30-MariaDB
-- PHP Version: 7.2.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_ekios`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `survey`
--

CREATE TABLE `survey` (
  `id` int(11) UNSIGNED NOT NULL,
  `jenis_kelamin` varchar(255) DEFAULT NULL,
  `pendidikan` varchar(255) DEFAULT NULL,
  `pekerjaan` varchar(255) DEFAULT NULL,
  `jenis_layanan` varchar(255) DEFAULT NULL,
  `ans1` enum('1','2','3','4') DEFAULT NULL,
  `ans2` enum('1','2','3','4') DEFAULT NULL,
  `ans3` enum('1','2','3','4') DEFAULT NULL,
  `ans4` enum('1','2','3','4') DEFAULT NULL,
  `ans5` enum('1','2','3','4') DEFAULT NULL,
  `ans6` enum('1','2','3','4') DEFAULT NULL,
  `ans7` enum('1','2','3','4') DEFAULT NULL,
  `ans8` enum('1','2','3','4') DEFAULT NULL,
  `ans9` enum('1','2','3','4') DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `survey`
--

INSERT INTO `survey` (`id`, `jenis_kelamin`, `pendidikan`, `pekerjaan`, `jenis_layanan`, `ans1`, `ans2`, `ans3`, `ans4`, `ans5`, `ans6`, `ans7`, `ans8`, `ans9`, `created_at`, `updated_at`) VALUES
(2, 'L', 'SD', 'PNS', 'KTP', '1', '2', '1', '1', '4', '1', '1', '2', '3', '2021-07-30 00:13:58', '2021-07-30 00:13:58'),
(3, 'L', 'SMA', 'TNI', 'KTPSSS', '2', '2', '2', '2', '2', '3', '1', '1', '4', '2021-07-30 00:22:16', '2021-07-30 00:22:16'),
(4, 'P', 'S1', 'WIRAUSAHA', 'Surat Keterangan Penghasilan Orang Tua', '3', '3', '3', '3', '3', '3', '3', '3', '4', '2021-07-30 14:38:28', '2021-07-30 14:38:28');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `survey`
--
ALTER TABLE `survey`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `survey`
--
ALTER TABLE `survey`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
