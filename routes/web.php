<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/contact', 'HomeController@contact');
Route::get('/pdf/{id}', 'RequestController@pdf');


Route::group(['middleware' => 'auth'], function () {

    Route::get('/dashboard', 'DashboardController@index');
    Route::get('form/{id}', 'HomeController@goToForm');
    Route::post('validate', 'HomeController@getFormValidate');
    Route::post('form/{id}', 'HomeController@getForm');
    Route::get('logout', 'AuthController@logout');
    Route::get('setting', 'UserController@show');
    Route::post('setting/update', 'UserController@update');
    Route::post('setting/validate', 'UserController@validateSetting');

    Route::group(['prefix' => 'question'], function () {
        Route::get('/', 'DashboardController@question');
        Route::get('detail/{id}', 'DashboardController@questionDetail');
        Route::post('validate/response', 'DashboardController@validateResponse');
        Route::post('/store', 'DashboardController@questionStore');
        Route::post('/update/{id}', 'DashboardController@questionUpdate');
    });

    Route::group(['prefix' => 'request'], function () {
        Route::get('/', 'RequestController@index');
        Route::get('/detail/{id}/{data}', 'RequestController@show');
        Route::post('form/{id}', 'RequestController@update');
        Route::post('validate', 'RequestController@getFormValidate');
    });

    Route::group(['middleware' => 'skpd'], function () {
        Route::group(['prefix' => 'response'], function () {
            Route::get('/', 'ResponseController@index');
            Route::get('/history', 'ResponseController@history');
            Route::get('/form/{id}/{data}', 'ResponseController@edit');
            Route::post('form/{id}', 'ResponseController@update');
            Route::post('validate', 'ResponseController@getFormValidate');
            Route::post('checknip', 'ResponseController@checkNIP');
        });

        Route::group(['prefix' => 'sign'], function () {
            Route::get('/', 'SignController@index');
            Route::get('/history', 'SignController@history');
            Route::get('/detail/{id}', 'SignController@edit');
            Route::post('store/{id}', 'SignController@store');
        });

        

    });

    Route::group(['middleware' => 'admin'], function () {
        Route::group(['prefix' => 'user'], function () {
            Route::get('/', 'UserController@index');
            Route::get('/form/{data}', 'UserController@edit');
            Route::post('/validate/{data}', 'UserController@validateForm');
            Route::post('/store/{data}', 'UserController@store');
            Route::get('/del/{data}', 'UserController@destroy');
        });

        Route::group(['prefix' => 'skpd'], function () {
            Route::get('/', 'SkpdController@index');
            Route::get('/form/{data}', 'SkpdController@edit');
            Route::post('/validate/{data}', 'SkpdController@validateForm');
            Route::post('/store/{data}', 'SkpdController@store');
            Route::get('/del/{data}', 'SkpdController@destroy');
        });

        Route::group(['prefix' => 'formats'], function () {
            Route::get('/', 'FormatController@index');
            Route::get('/create', 'FormatController@create');
            Route::post('/store', 'FormatController@store');
            Route::post('/upload', 'FormatController@upload');
            Route::post('/edit/upload', 'FormatController@upload');
            Route::get('/edit/{id}', 'FormatController@edit');
            Route::post('/update/{id}', 'FormatController@update');
            Route::get('/form/add/{code}', 'FormatController@addForm');
            Route::get('/del/{id}', 'FormatController@destroy');
        });

        Route::group(['prefix' => 'contact'], function () {
            Route::get('/edit', 'DashboardController@contact');
            Route::post('/validate', 'DashboardController@validateContact');
            Route::post('/update', 'DashboardController@contactUpdate');
        });

        
        Route::group(['prefix' => 'menus'], function () {
            Route::get('/', 'MenuController@index');
            Route::get('/create', 'MenuController@create');
            Route::post('/store', 'MenuController@store');
            Route::post('/store/{id}', 'MenuController@store');
            Route::get('/create/{id}', 'MenuController@create');

            Route::get('/edit/{id}', 'MenuController@edit');
            Route::post('/update/{id}', 'MenuController@update');

            Route::get('/{id}', 'MenuController@index');
            Route::get('/form/add/{code}', 'MenuController@addForm');

            Route::get('/del/{id}', 'MenuController@destroy');
        });
    });

    Route::group(['prefix' => 'survey'], function () {
        Route::post('/store/{id}', 'SurveyController@store');
        Route::get('/', 'SurveyController@index');
        
    });
});

Route::group(['middleware' => 'guest'], function () {
    Route::group(['prefix' => 'admin'], function () {
        Route::get('/login', 'AuthController@goToLogin');
        Route::post('/login', 'AuthController@login');
    });
    Route::get('/login', 'AuthController@goToLoginUser');
    Route::post('/login', 'AuthController@loginUser');
    Route::get('/register', 'AuthController@goToRegister');
    Route::post('/register', 'AuthController@register');
    Route::post('/register/validate', 'AuthController@validateRegister');
    Route::get('/forgotpassword', 'AuthController@goToForgotPassword');
    Route::post('/forgotpassword', 'AuthController@forgotpassword');
    Route::get('/forgotpassword/verify/{email}', 'AuthController@verify');
    Route::post('/forgotpassword/store/{email}', 'AuthController@forgotpasswordStore');
    Route::get('/verifyemail', 'AuthController@verifyemail')->name('auth.verifyemail');
});

Route::get('/', 'HomeController@goToHome');
Route::get('/{id}', 'HomeController@goToHome');

Route::group(['prefix' => 'question'], function () {
    // Route::post('/store', 'DashboardController@questionStore');
    Route::post('/validate', 'DashboardController@validateQuestion');
});
