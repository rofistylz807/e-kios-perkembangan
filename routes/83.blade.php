<p style="text-align: center;"><strong><u><img src="../../documents/53/62dd1_kel_timbau.png" alt="" width="950" height="198" /></u></strong></p>
<p style="text-align: center;"><strong><u>SURAT KETERANGAN PENGHASILAN</u></strong></p>
<p style="text-align: center;">Nomor : {{$data['nomor_surat']}}</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify; padding-left: 40px;">Yang bertanda tangan dibawah ini :</p>
<p style="text-align: justify; padding-left: 80px;">Nama&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;:&nbsp; {{$data['nama_pejabat']}}</p>
<p style="text-align: justify; padding-left: 80px;">Jabatan&nbsp; &nbsp; &nbsp; &nbsp; :&nbsp; {{$data['nama_jabatan']}}</p>
<p style="text-align: justify;">&nbsp;</p>
<p style="text-align: justify; padding-left: 40px;">Menerangkan dengan sebenarnya bahwa :</p>
<table style="height: 209px; width: 100%; border-collapse: collapse; border-style: none;" border="1">
<tbody>
<tr style="height: 30px;">
<td style="width: 25.9118%; height: 28px; border-style: hidden; padding-left: 80px;">Nama Organisasi</td>
<td style="width: 1.71769%; height: 28px; border-style: hidden;">:</td>
<td style="width: 72.3704%; height: 28px; border-style: hidden;">{{$data['nama_org']}}</td>
</tr>
<tr style="height: 30px;">
<td style="width: 25.9118%; height: 29px; border-style: hidden; padding-left: 80px;">Nomor Akta Pendirian</td>
<td style="width: 1.71769%; height: 29px; border-style: hidden;">:</td>
<td style="width: 72.3704%; height: 29px; border-style: hidden;">{{$data['no_akta']}}</td>
</tr>
<tr style="height: 30px;">
<td style="width: 25.9118%; height: 30px; border-style: hidden; padding-left: 80px;">Alamat Sekretariat</td>
<td style="width: 1.71769%; height: 30px; border-style: hidden;">:</td>
<td style="width: 72.3704%; height: 30px; border-style: hidden;">{{$data['sekretariat']}}</td>
</tr>
<tr style="height: 30px;">
<td style="width: 25.9118%; height: 33px; border-style: hidden; padding-left: 80px;">Jenis Organisasi</td>
<td style="width: 1.71769%; height: 33px; border-style: hidden;">:</td>
<td style="width: 72.3704%; height: 33px; border-style: hidden;">{{$data['organisasi']}}</td>
</tr>
<tr style="height: 30px;">
<td style="width: 25.9118%; height: 32px; border-style: hidden; padding-left: 80px;">Status Bangunan</td>
<td style="width: 1.71769%; height: 32px; border-style: hidden;">:</td>
<td style="width: 72.3704%; height: 32px; border-style: hidden;">{{$data['bangunan']}}</td>
</tr>
<tr style="height: 30px;">
<td style="width: 25.9118%; height: 28px; border-style: hidden; padding-left: 80px;">Peruntukan Bangunan</td>
<td style="width: 1.71769%; height: 28px; border-style: hidden;">:</td>
<td style="width: 72.3704%; height: 28px; border-style: hidden;">Sekretariat</td>
</tr>
<tr style="height: 30px;">
<td style="width: 25.9118%; height: 29px; border-style: hidden; padding-left: 80px;">Nama Penanggung Jawab</td>
<td style="width: 1.71769%; height: 29px; border-style: hidden;">:</td>
<td style="width: 72.3704%; height: 29px; border-style: hidden;">{{$data['penanggung']}}</td>
</tr>
<tr style="height: 30px;">
<td style="width: 25.9118%; border-style: hidden; padding-left: 80px;">Jabatan pada Organisasi</td>
<td style="width: 1.71769%; border-style: hidden;">:</td>
<td style="width: 72.3704%; border-style: hidden;">{{$data['jabatan']}}</td>
</tr>
<tr style="height: 30px;">
<td style="width: 25.9118%; border-style: hidden; padding-left: 80px;">NIK Penanggung Jawab</td>
<td style="width: 1.71769%; border-style: hidden;">:</td>
<td style="width: 72.3704%; border-style: hidden;">{{$data['nik']}}</td>
</tr>
<tr style="height: 30px;">
<td style="width: 25.9118%; border-style: hidden; padding-left: 80px;">Alamat Penanggung Jawab</td>
<td style="width: 1.71769%; border-style: hidden;">:</td>
<td style="width: 72.3704%; border-style: hidden;">{{$data['alamat']}}</td>
</tr>
</tbody>
</table>
<p>&nbsp;</p>
<p style="padding-left: 40px; text-align: justify;">Berdasarkan Surat Pengantar RT {{$data['rt']}} Nomor {{$data['nomor_rt']}} Tanggal {{$data['tanggal_rt']}}, Organisasi/Kelompok tersebut diatas memang benar beralamat di Jalan&nbsp; {{$data['jalan']}} Kelurahan/Desa {{$data['kelurahan_desa']}} Kecamatan {{$data['kecamatan']}} Kabupaten Kutai Kartanegara Propinsi Kalimantan Timur.</p>
<p style="padding-left: 40px;">Demikian Surat Keterangan Domisili ini dibuat berdasarkan hasil pemeriksaan di lapangan, dan untuk dipergunakan sebagaimana mestinya.</p>
<p style="text-align: justify;">&nbsp;</p>
<table style="width: 100%; border-collapse: collapse; border-style: none; height: 77px;" border="1">
<tbody>
<tr style="height: 77px;">
<td style="width: 60.7958%; height: 77px; border-style: none;">&nbsp;</td>
<td style="width: 39.2042%; height: 77px; border-style: hidden;">
<p>Dibuat Di&nbsp; &nbsp; &nbsp; : {{$data['dibuat']}}</p>
<p><span style="text-decoration: underline;">Pada Tanggal : {{$data['tanggal_dibuat']}}</span></p>
</td>
</tr>
</tbody>
</table>
<table style="height: 240px; width: 100.636%; border-collapse: collapse; border-style: none;" border="1">
<tbody>
<tr style="height: 13px;">
<td style="width: 48.3387%; height: 13px; border-style: none;">&nbsp;</td>
<td style="width: 51.6613%; height: 13px; border-style: hidden;">
<p style="text-align: center;">{{$data['an']}}</p>
<p style="text-align: center;">{{$data['nama_jabatan']}}</p>
<p style="text-align: center;">&nbsp;</p>
<p style="text-align: center;">&nbsp;</p>
<p style="text-align: center;"><strong><span style="text-decoration: underline;">{{$data['nama_pejabat']}}</span></strong></p>
<p style="text-align: center;">NIP. {{$data['nip']}}</p>
</td>
</tr>
</tbody>
</table>
<p style="text-align: center;"><br /><br /></p>