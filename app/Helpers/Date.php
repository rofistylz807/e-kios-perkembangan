<?php

function convertDate($date = null,$view=null)
{
    $dateidn=[
        '0'=>'',
        '1'=>'Januari',
        '2'=>'Februari',
        '3'=>'Maret',
        '4'=>'April',
        '5'=>'Mei',
        '6'=>'Juni',
        '7'=>'Juli',
        '8'=>'Agustus',
        '9'=>'September',
        '10'=>'Oktober',
        '11'=>'November',
        '12'=>'Desember'
    ];
    if($view==null){
        return date('j', strtotime($date))." ".$dateidn[date('n', strtotime($date))]." ".date('Y', strtotime($date));
    }
    if($view=='m'){
        return $dateidn[date('n', strtotime($date))];
    }
}

function money($number){
	
	$money = number_format($number,0,',','.');
	return $money;
 
}

function counted($number) {
    if($number<0) {
        $res = "minus ". trim(denominator($number));
    } else {
        $res = trim(denominator($number));
    }     		
    return $res;
}

function denominator($number) {
    $number = abs($number);
    $word = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
    $temp = "";
    if ($number < 12) {
        $temp = " ". $word[$number];
    } else if ($number <20) {
        $temp = counted($number - 10). " Belas ";
    } else if ($number < 100) {
        $temp = counted($number/10)." Puluh ". counted($number % 10);
    } else if ($number < 200) {
        $temp = " Seratus " . counted($number - 100);
    } else if ($number < 1000) {
        $temp = counted($number/100) . " Ratus " . counted($number % 100);
    } else if ($number < 2000) {
        $temp = " Seribu " . counted($number - 1000);
    } else if ($number < 1000000) {
        $temp = counted($number/1000) . " Ribu " . counted($number % 1000);
    } else if ($number < 1000000000) {
        $temp = counted($number/1000000) . " Juta " . counted($number % 1000000);
    } else if ($number < 1000000000000) {
        $temp = counted($number/1000000000) . " Milyar " . counted(fmod($number,1000000000));
    } else if ($number < 1000000000000000) {
        $temp = counted($number/1000000000000) . " Trilyun " . counted(fmod($number,1000000000000));
    }     
    return $temp;
}
