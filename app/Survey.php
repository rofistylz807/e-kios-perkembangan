<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    protected $table = 'survey';
    protected $primaryKey = 'id';
    protected $fillable = [
        'jenis_kelamin',
        'pendidikan',
        'pekerjaan',
        'jenis_layanan',
        'ans1',
        'ans2',
        'ans3',
        'ans4',
        'ans5',
        'ans6',
        'ans7',
        'ans8',
        'ans9',
     
    ];
}
