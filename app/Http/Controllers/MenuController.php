<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Validator;
use Session;
use File;
use App\Model\Menu;
use App\Model\Format;
use App\Model\Sign;
use App\Model\skpd;
use App\Model\Req;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Storage;
use QrCode;
use DB;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id=null)
    {
        $data=$this->menu($id);
        $breadcrumb=[];
        for ($i=1;$i<count($data);$i++) {
            $breadcrumb[$data[$i]->title.' ('.$data[$i]->id.')']=url('menus/'.$data[$i]->id);
        }
        if ($id!=null) {
            $breadcrumb['Menu']=url('menus');
        }
        $breadcrumb=array_reverse($breadcrumb);
        $menu=$id==null?Menu::where('menu.level', 1)->leftjoin('ref_skpd', 'ref_skpd.skpd_id', '=', 'menu.skpd_id')->select('menu.*', 'ref_skpd.skpd_name')->get():Menu::where('menu.menu_id', $id)->leftjoin('ref_skpd', 'ref_skpd.skpd_id', '=', 'menu.skpd_id')->select('menu.*', 'ref_skpd.skpd_name')->get();
        $reqcount=Req::where('menu.skpd_id', Auth::User()->skpd_id)->where('request.status', 'pending')->where('request.sign_id', null)->join('menu', 'menu.id', '=', 'request.menu_id')->select('menu.*', 'request.*', 'menu.description as mdesc')->count();
        return view('dashboard.menu.index', ['breadcrumb'=>$breadcrumb,'id'=>$id], compact('menu','reqcount'));
    }

    public static function menu($id)
    {
        $ancestors = Menu::where('id', '=', $id)->get();
        if (count($ancestors)) {
            while ($ancestors->last()->menu_id !== null) {
                $parent = Menu::where('id', '=', $ancestors->last()->menu_id)->get();
                $ancestors = $ancestors->merge($parent);
            }
        }
        return $ancestors;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id=null)
    {
        $skpd=Skpd::all();
        $format=Format::all();
        $js='synadmin/assets/js/menu.js';
        $reqcount=Req::where('menu.skpd_id', Auth::User()->skpd_id)->where('request.status', 'pending')->where('request.sign_id', null)->join('menu', 'menu.id', '=', 'request.menu_id')->select('menu.*', 'request.*', 'menu.description as mdesc')->count();
        return view('dashboard.menu.create', ['js'=>$js,'id'=>$id], compact('skpd', 'format','reqcount'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id=null)
    {
        $data=[];
        $img='';
        if ($request->hasFile('img')) {
            $resorce= $request->file('img');
            $name_file   = substr(md5(mt_rand()), 0, 5).'_'.$resorce->getClientOriginalName();
            $resorce->move('menu', $name_file);
            $img=asset('menu/'.$name_file);
        }
            
        $menu=Menu::create([
            'menu_id'=>$id!=null?$id:null,
            'skpd_id'=>$request->skpd,
            'format_id'=>$request->format,
            'title'=>$request->title,
            'level'=>$id==null?1:null,
            'img'=>$img,
            'description'=>$request->description
        ]);
        if ($request->has('sign')) {
            foreach ($request->sign as $key => $value) {
                $sign=Sign::create([
                    'menu_id'=>$menu->id,
                    'skpd_id'=>$value[0],
                    'keyword'=>$key
                ]);
            }
            $url=url('/form/'.$menu->id);
        } else {
            $url=$request->url!=""?$request->url:url('/'.$menu->id);
        }
        
        Menu::where('id', $menu->id)->update([
            'url'=>$url,
        ]);
        

        return redirect('menus/'.$id)->with('alert', json_encode(['status'=>'success','data'=>'Menu Berhasil ditambah']));
    }

    public function edit($id=null)
    {
        try {
            $id=json_decode(Crypt::decryptString($id), true);
            $skpd=Skpd::all();
            $js='synadmin/assets/js/menu.js';
            $menu=Menu::leftjoin('format', 'format.id', '=', 'menu.format_id')->where('menu.id', $id['menu'])->select('menu.id as id_menu', 'menu.*', 'format.*')->first();
            // dd($menu);
            $sign=Sign::where('menu_id', $menu->id_menu)->get();
            $format=Format::all();
            $reqcount=Req::where('menu.skpd_id', Auth::User()->skpd_id)->where('request.status', 'pending')->where('request.sign_id', null)->join('menu', 'menu.id', '=', 'request.menu_id')->select('menu.*', 'request.*', 'menu.description as mdesc')->count();
            return view('dashboard.menu.edit', ['js'=>$js,'id'=>$id['id']], compact('menu', 'sign', 'skpd', 'format','reqcount'));
        } catch (DecryptException $e) {
            return redirect('menus')->with('alert', json_encode(['status'=>'warning','data'=>'Kesalahan Sistem']));
        }
    }

    public function update(Request $request, $id=null)
    {
        try {
            $id=json_decode(Crypt::decryptString($id), true);
            $menu=Menu::where('id', $id['menu'])->first();
            $data=[
                'skpd_id'=>$request->skpd,
                'format_id'=>$request->format,
                'title'=>$request->title,
                'description'=>$request->description,
            ];
            if ($request->hasFile('img')) {
                $filename=explode(url('menu').'/', $menu->img);
                if (count($filename)>1) {
                    if (File::exists(public_path('menu/'.$filename[1]))) {
                        File::delete(public_path('menu/'.$filename[1]));
                    }
                }
                $resorce= $request->file('img');
                $name_file   = substr(md5(mt_rand()), 0, 5).'_'.$resorce->getClientOriginalName();
                $resorce->move('menu', $name_file);
                $data['img']=asset('menu/'.$name_file);
            }
            if ($request->has('sign')) {
                $i=0;
                $sign=Sign::where('menu_id', $id['menu'])->get();
                if (count($sign)) {
                    foreach ($request->sign as $key => $value) {
                        if ($sign[$i]->skpd_id!=$value[0]) {
                            $sign=Sign::where('id', $sign[$i]->id)->update([
                                'menu_id'=>$menu->id,
                                'skpd_id'=>$value[0],
                                'keyword'=>$key
                            ]);
                        }
                        $i++;
                    }
                } else {
                    foreach ($request->sign as $key => $value) {
                        $sign=Sign::create([
                            'menu_id'=>$menu->id,
                            'skpd_id'=>$value[0],
                            'keyword'=>$key
                        ]);
                    }
                }
                
                $data['url']=url('/form/'.$menu->id);
            } else {
                Sign::where('menu_id', $id['menu'])->delete();
                $data['url']=$request->url!=""&&$request->url!=url('/'.$menu->id)&&$request->url!=url('form/'.$menu->id)?$request->url:url('/'.$menu->id);
            }
            
            $menu=Menu::where('id', $id['menu'])->update($data);
            return redirect('menus/'.$id['id'])->with('alert', json_encode(['status'=>'success','data'=>'Menu Berhasil ditambah']));
        } catch (DecryptException $e) {
            return redirect('menus')->with('alert', json_encode(['status'=>'warning','data'=>'Kesalahan Sistem']));
        }
    }

    public function addForm($code)
    {
        $format=Format::where('id', $code)->first();
        $skpd=Skpd::all();
        $sign=json_decode($format->sign);
        return view('dashboard.menu.add_form', ['sign'=>$sign], compact('skpd'));
    }

    public function destroy($id)
    {
        try {
            $id=Crypt::decryptString($id);
            $data=$this->menu($id);
            foreach ($data as $value) {
                $img=explode(url('menu').'/', $value->img);
                if (count($img)>0) {
                    if (file_exists(public_path('menu/'.$img[1]))) {
                        File::delete(public_path('menu/'.$img[1]));
                    }
                }
                if (File::exists(resource_path('views/landingpage/documents/'.$id.'.blade.php'))) {
                    File::delete(resource_path('views/landingpage/documents/'.$id.'.blade.php'));
                }
                Req::where('menu_id',$id)->delete();
                Sign::where('menu_id',$id)->delete();
                Menu::where('id',$id)->delete();
                return redirect()->back()->with('alert', json_encode(['status'=>'success','data'=>'Menu Berhasil dihapus']));
            }
        } catch (DecryptException $e) {
            return redirect('menus')->with('alert', json_encode(['status'=>'warning','data'=>'Kesalahan Sistem']));
        }
    }
}
