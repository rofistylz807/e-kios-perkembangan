<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Validator;
use Session;
use App\Model\Menu;
use App\Model\Format;
use App\Model\Http;
use App\Model\Req;
use App\Model\Sign;
use App\Model\Skpd;
use App\Model\Log;
use File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class SignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $req=Req::where('request.status', 'pending')->where('request.sign_id', Auth::User()->skpd_id)->join('menu', 'menu.id', '=', 'request.menu_id')->select('menu.*', 'request.*', 'menu.description as mdesc')->orderBy('request.id', 'DESC')->get();
        // dd(Auth::User()->skpd_id);
        $reqcount=Req::where('menu.skpd_id', Auth::User()->skpd_id)->where('request.status', 'pending')->where('request.sign_id', null)->join('menu', 'menu.id', '=', 'request.menu_id')->select('menu.*', 'request.*', 'menu.description as mdesc')->count();
        return view('dashboard.sign.index', compact('req','reqcount'));
    }

    public function history()
    {
        $log=Log::where('log.skpd_id', Auth::User()->skpd_id)->join('request', 'request.id', '=', 'log.request_id')->join('menu', 'menu.id', '=', 'request.menu_id')->select('menu.title as menu', 'log.created_at as date', 'log.status as status', 'log.response as response', 'log.message as message')->orderBy('log.id', 'DESC')->get();
        $reqcount=Req::where('menu.skpd_id', Auth::User()->skpd_id)->where('request.status', 'pending')->where('request.sign_id', null)->join('menu', 'menu.id', '=', 'request.menu_id')->select('menu.*', 'request.*', 'menu.description as mdesc')->count();
        return view('dashboard.sign.log', compact('log','reqcount'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id=null)
    {
        try {
            $identity=[];
            $req=Req::where('id', $id)->first();
            $menu=Menu::where('id', $req->menu_id)->first();
            $form=Format::where('id', $menu->format_id)->first();
            $data=json_decode(Crypt::decryptString($req->response), true);
            $sign=Sign::where('menu_id', $req->menu_id)->where('skpd_id', $req->sign_id)->first();
            $reqcount=Req::where('menu.skpd_id', Auth::User()->skpd_id)->where('request.status', 'pending')->where('request.sign_id', null)->join('menu', 'menu.id', '=', 'request.menu_id')->select('menu.*', 'request.*', 'menu.description as mdesc')->count();
            foreach (json_decode($form->data, true)['response'] as $key=>$value) {
                if ($value['option']==$sign->keyword) {
                    if ($value['field']=='name') {
                        $identity['name']=$data[$key];
                    }
                    if ($value['field']=='nip') {
                        $identity['nip']=$data[$key];
                    }
                }
            }
            return view('dashboard.sign.detail', ['id'=>$id,'identity'=>count($identity)?$identity:['nip'=>'','name'=>'']], compact('menu', 'req','reqcount'));
        } catch (DecryptException $e) {
        }
    }

    public function store(Request $request, $id)
    {
        try {
            //test e-sign
            // $request->passphrase='#1234Qwer*';
            // $nik='0803202100007062';
            $id=Crypt::decryptString($id);
            $req=Req::where('id', $id)->with('sign')->first();
            $nik=Http::getNIP($request->nip);
            if (array_key_exists('message', $nik)) {
                $nik='';
            } else {
                $nik=$nik['nik'];
            }
            $post=[
                'nik'=>$request->nik,
                'passphrase'=>$request->passphrase,
                'tampilan'=>'invisible',
                'jenis_response'=>'BASE64',
                'file'=>new \CURLFile(public_path('pdf/'.$id.'.pdf'), 'application/pdf', $id.'.pdf')
            ];
            $checkAPI=Http::postsign('https://ds-gw.kukarkab.go.id/api/sign/pdf', $post);
            if ($checkAPI) {
                if (array_key_exists('error', $checkAPI)) {
                    Log::create([
                        'request_id'=>$id,
                        'skpd_id'=>Auth::User()->skpd_id,
                        'response'=>json_encode($checkAPI),
                        'message'=>$checkAPI['error'],
                        'status'=>'reject'
                    ]);
                    return redirect('/sign')->with('alert', json_encode(['status'=>'warning','data'=>$checkAPI['error']]));
                }
    
                Log::create([
                    'request_id'=>$id,
                    'skpd_id'=>Auth::User()->skpd_id,
                    'response'=>json_encode($checkAPI),
                    'message'=>$checkAPI['message'],
                    'status'=>'done'
                ]);
    
                File::put(public_path().'/pdf/'.$id.'.pdf', base64_decode($checkAPI['base64_file'], true));
                $sign_count=(int)$req->sign_count+1;
                if (count($req->sign)-1>=$sign_count) {
                    Req::where('id', $id)->update([
                        'sign_count'=>$sign_count,
                        'sign_id'=>$req->sign[$sign_count]->skpd_id,
                        'description'=>'menunggu Tandatangan dari '. Skpd::where('skpd_id', $req->sign[$sign_count]->skpd_id)->first()->skpd_name
                    ]);
                } else {
                    Req::where('id', $id)->update([
                        'sign_count'=>$sign_count,
                        'sign_id'=>null,
                        'description'=>'',
                        'status'=>'done'
                    ]);
                }
            } else {
                return redirect('/sign')->with('alert', json_encode(['status'=>'warning','data'=>'Kesalahan Sistem']));
            }
            return redirect('/sign')->with('alert', json_encode(['status'=>'success','data'=>'Dokumen telah ditandatangan']));
        } catch (DecryptException $e) {
            return redirect('/sign')->with('alert', json_encode(['status'=>'warning','data'=>'Kesalahan Sistem']));
        }
        
    }
}
