<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Model\Skpd;
use App\Model\Req;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class SkpdController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $user=User::leftjoin('ref_skpd', 'ref_skpd.skpd_id', '=', 'users.skpd_id')->select('users.*', 'ref_skpd.*')->orderBy('users.id','DESC')->get();
        $skpd=Skpd::orderBy('skpd_id','DESC')->get();
        $reqcount=Req::where('menu.skpd_id', Auth::User()->skpd_id)->where('request.status', 'pending')->where('request.sign_id', null)->join('menu', 'menu.id', '=', 'request.menu_id')->select('menu.*', 'request.*', 'menu.description as mdesc')->count();
        return view('dashboard.skpd.index', compact('skpd','reqcount'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$data)
    {
        try {
            $data=Crypt::decryptString($data);
            $data=json_decode($data, true);
            
            $img='';
            if ($request->hasFile('img')) {
                $resorce= $request->file('img');
                $name_file   = time() . '.' . $resorce->getClientOriginalName();
                if (!file_exists(public_path('img/'.$name_file))) {
                    $resorce->move('img', $name_file);
                }
              
                $img = ($name_file);
            }
    
            if ($data['status']==1) {
                Skpd::create([
                    'skpd_name'=>$request->nama,
                    'skpd_address'=>$request->alamat,
                    'skpd_phone'=>$request->nomor_telepon,
                    'skpd_email'=>$request->email,
                    'skpd_fax'=>$request->faximili,
                    'skpd_postal_code'=>$request->kode_pos,
                    'skpd_sub_district'=>$request->kecamatan,
                    'skpd_head_name'=>$request->nama_kepala,
                    'skpd_head_nip'=>$request->nip_kepala,
                    'skpd_head_position'=>$request->posisi_kepala,
                    'skpd_head_level'=>$request->pangkat_kepala,
                    'img'=>$img,
                ]);
                // if ($request->hasFile('skpd_kop_surat')) {
                //     $gambar     = $request->file('skpd_kop_surat');
                //     $namagambar = time() . '.' . $gambar->getClientOriginalExtension();
                //     $destinationPath = public_path('/uploads/skpd_kop_surat');
                //     $gambar->move($destinationPath, $namagambar);
        
                //     $save['skpd_kop_surat'] = $namagambar;
                // }
            
                return redirect('/skpd')->with('alert', json_encode(['status'=>'success','data'=>'Data Berhasil Ditambah']));
            } else {
                $arr=[
                    'skpd_name'=>$request->nama,
                    'skpd_address'=>$request->alamat,
                    'skpd_phone'=>$request->nomor_telepon,
                    'skpd_email'=>$request->email,
                    'skpd_fax'=>$request->faximili,
                    'skpd_postal_code'=>$request->kode_pos,
                    'skpd_sub_district'=>$request->kecamatan,
                    'skpd_head_name'=>$request->nama_kepala,
                    'skpd_head_nip'=>$request->nip_kepala,
                    'skpd_head_position'=>$request->posisi_kepala,
                    'skpd_head_level'=>$request->pangkat_kepala,
                    'img'=>$img,
                    
                ];
                Skpd::where('skpd_id',$data['id'])->update($arr);
                
                return redirect('/skpd')->with('alert', json_encode(['status'=>'success','data'=>'Data Berhasil Diubah']));
            }
            
        } catch (DecryptException $e) {
            return redirect('/dashboard')->with('alert', json_encode(['status'=>'warning','data'=>'Kesalahan Sistem']));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($data)
    {
        try {
            $data=Crypt::decryptString($data);
            $data=json_decode($data, true);
            
            if ($data['status']==1) {
                return view('dashboard.skpd.show');
            } else {
                $skpd=Skpd::where('skpd_id', $data['id'])->first();
                return view('dashboard.skpd.show', compact('skpd'));
            }
        } catch (DecryptException $e) {
            return redirect('/dashboard')->with('alert', json_encode(['status'=>'warning','data'=>'Kesalahan Sistem']));
        }
    }

    public function validateForm(Request $request, $data)
    {
        try {
            $data=Crypt::decryptString($data);
            $data=json_decode($data, true);
            if($data['status']==1){
                $val=[
                    'nama'=>'required|unique:ref_skpd,skpd_name',
                    'alamat'=>'required',
                    'nomor_telepon'=>'required',
                    'email'=>'required|unique:ref_skpd,skpd_email',
                    'kode_pos'=>'required',
                    'kecamatan'=>'required',
                    'nama_kepala'=>'required',
                    'nip_kepala'=>'required',
                    'posisi_kepala'=>'required',
                    'pangkat_kepala'=>'required',
                    'img'   => 'mimes:png,jpeg,jpg,docx,pdf|max:5000',
                ];
                
             
            }else{
                $val=[
                    'nama'=>['required',Rule::unique('ref_skpd', 'skpd_name')->ignore($data['id'],'skpd_id')],
                    'alamat'=>'required',
                    'nomor_telepon'=>'required',
                    'email'=>['required',Rule::unique('ref_skpd', 'skpd_email')->ignore($data['id'],'skpd_id')],
                    'kode_pos'=>'required',
                    'kecamatan'=>'required',
                    'nama_kepala'=>'required',
                    'nip_kepala'=>'required',
                    'posisi_kepala'=>'required',
                    'pangkat_kepala'=>'required',
                    'img'   => 'mimes:png,jpeg,jpg,docx,pdf|max:5000',

                ];
            
            }
            $validator = Validator::make($request->all(),$val);
            if ($validator->fails()) {
                return json_encode(['status'=>false,'validation'=>$validator->errors()]);
            } else {
                return json_encode(['status'=>true]);
            }
        } catch (DecryptException $e) {
            return redirect('/dashboard')->with('alert', json_encode(['status'=>'warning','data'=>'Kesalahan Sistem']));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($data)
    {
        try {
            $data=Crypt::decryptString($data);
            $data=json_decode($data, true);
            Skpd::where('skpd_id',$data)->delete();
            return redirect('/skpd')->with('alert', json_encode(['status'=>'success','data'=>'Data sudah di Hapus']));
        } catch (DecryptException $e) {
            return redirect('/dashboard')->with('alert', json_encode(['status'=>'warning','data'=>'Kesalahan Sistem']));
        }
    }
}
