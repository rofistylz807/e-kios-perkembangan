<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Validator;
use Session;
use App\Model\Mails;
use App\Model\Http;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function goToLogin()
    {
        return view('auth.login');
    }

    public function goToLoginUser()
    {
        return view('auth.login_user');
    }

    public function goToRegister()
    {
        return view('auth.register');
    }

    public function goToForgotPassword()
    {
        return view('auth.forgotpassword');
    }

    public function login(Request $r)
    {
        $validator = Validator::make($r->all(), [
                'username' => 'required|exists:users,username',
                ]);
        if ($validator->fails()) {
            return redirect()->back()->with('alert', json_encode(['status'=>'warning','data'=>'Username Tidak Ditemukan']));
        }
        $user=User::where('username', $r->username)->first();
        if ($user->role=='user') {
            return redirect()->back()->with('alert', json_encode(['status'=>'warning','data'=>'Login Khusus Admin']));
        }
        if (Auth::attempt(['username' => $r->username, 'password' => $r->password])) {
            return redirect()->intended('/dashboard');
        }
        return redirect()->back()->with('alert', json_encode(['status'=>'warning','data'=>'Password Salah']));
    }

    public function loginUser(Request $r)
    {
        $database=false;
        $api=false;
        $validator = Validator::make($r->all(), [
                'nik' => 'required',
                ]);
        if ($validator->fails()) {
            // dd(123);
            $database=true;
            $checkAPI=Http::post('https://ws-ewarga.kukarkab.go.id/cek-nik', [
                'tmp_token' => 'maungapainsih',
                // 'nik' => $r->nik,
            ]);
            if (is_array($checkAPI)) {
                if (array_key_exists('NAMA_LGKP', $checkAPI)) {
                    User::create([
                        'name'=>$checkAPI['NAMA_LGKP'],
                        // 'nik'=>$r->nik,
                        'role'=>'user',
                    ]);
                }else{
                    $api=true;
                }
            } else {
                $api=true;
            }
        }

        if ($database && $api) {
            return redirect()->back()->with('alert', json_encode(['status'=>'warning','data'=>'NIK Tidak Ditemukan']));
        }
        $user=User::where('nik', $r->nik)->first();
        if ($user->role!='user') {
            return redirect()->back()->with('alert', json_encode(['status'=>'warning','data'=>'NIK Tidak Ditemukan']));
        }
        if (Auth::loginUsingId($user->id)) {
            return redirect()->intended('/dashboard');
        }
        return redirect()->back()->with('alert', json_encode(['status'=>'warning','data'=>'Kesalahan Sistem']));
    }

    public function logout()
    {
        $status=true;
        if (Auth::check()) {
            $status=Auth::User()->role=='user'?true:false;
            Auth::logout();
        }
        if ($status) {
            return redirect('login');
        } else {
            return redirect('admin/login');
        }
    }

    public function validateRegister(Request $request)
    {
        $data=$request->all();
        $validator = Validator::make($data, [
            'nik'=>'required',
            'nama'=>'required',
            'nomor_hp'=>'required|min:10|unique:users,phone',
            // 'password'=>'required|min:6',
            // 'konfirmasi_password'=>'required|min:6|same:password'
        ]);
        if ($validator->fails()) {
            return json_encode(['status'=>false,'validation'=>$validator->errors()]);
        } else {
            return json_encode(['status'=>true]);
        }
    }

    public function register(Request $r)
    {
        $user=User::create([
            'name'=>$r->nama,
            'nik'=>$r->nik,
            'phone'=>$r->nomor_hp,
            'role'=>'user',
            // 'password'=>Hash::make($r->password)
        ]);

        if ($user) {
            return redirect('/login')->with('alert', json_encode(['status'=>'success','data'=>'Registrasi Berhasil']));
        } else {
            return redirect('/register')->with('alert', json_encode(['status'=>'warning','data'=>'Kesalahan Sistem']));
        }
    }

    public function forgotpassword(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|exists:users,email',
            ]);
        if ($validator->fails()) {
            return redirect()->back()->with('alert', json_encode(['status'=>'error','data'=>'Email tidak terdaftar']));
        }
        Mails::sendEmail([
            'path'=>'auth.emailForgotPassword',
            'data'=>[
                'url'=>url('/').'/forgotpassword/verify/'.Crypt::encryptString($request->email),
            ],
            'emailto'=>$request->email,
            'emailfrom'=>'Ekios Kukar',
            'subject'=>'Lupa Password'
        ]);
        return redirect('admin/login')->with('alert', json_encode(['status'=>'success','data'=>'Silahkan cek email anda untuk proses penggantian password']));
    }

    public function verify($email)
    {
        try {
            $emails=Crypt::decryptString($email);
            $user=User::where('email', $emails)->first();
            if (!$user) {
                return redirect('forgotpassword')->with('alert', json_encode(['status'=>'error','data'=>'Masukan Email anda yang telah terdaftar']));
            }
        } catch (DecryptException $e) {
            return redirect('forgotpassword')->with('alert', json_encode(['status'=>'error','data'=>'Masukan Email anda yang telah terdaftar']));
        }
        return view('auth.forgotpasswordform', ['code' => $email]);
    }

    public function verifyemail()
    {
        $data = [
          'page_title'   => 'DiSAPA I-DAMAN',
          'sub_title'    => 'Kutai Kartanegara',
    
         
      ];
        return view('auth.emailforgotpassword');
    }


    public function forgotpasswordStore(Request $request, $email)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'password' => 'required|min:6',
            'konfirmasi_password'=>'required|min:6|same:password'
            ]);
        // dd($validator->fails());
        if ($validator->fails()) {
            return redirect()->back()->with('alert', json_encode(['status'=>'error','data'=>'password kurang dari 6 karakter atau password dan konfirmasi password tidak sama']));
        }
        try {
            $emails=Crypt::decryptString($email);
            $user=User::where('email', $emails)->update([
                'password'=>Hash::make($request->password)
            ]);
            if (!$user) {
                return redirect('forgotpassword')->with('alert', json_encode(['status'=>'warning','data'=>'Kesalahan Sistem']));
            }
        } catch (DecryptException $e) {
            return redirect('forgotpassword')->with('alert', json_encode(['status'=>'warning','data'=>'Kesalahan Sistem']));
        }
        return redirect('admin/login')->with('alert', json_encode(['status'=>'success','data'=>'Password Sudah diperbarui']));
    }
}
