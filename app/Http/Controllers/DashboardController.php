<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Validator;
use Session;
use App\Model\Menu;
use App\Model\Question;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use App\Model\Req;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $reqcount=Req::where('menu.skpd_id', Auth::User()->skpd_id)->where('request.status', 'pending')->where('request.sign_id', null)->join('menu', 'menu.id', '=', 'request.menu_id')->select('menu.*', 'request.*', 'menu.description as mdesc')->count();
        return view('dashboard.index', compact('reqcount'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function validateContact(Request $request)
    {
        $val=[
            'email'=>'required',
            'alamat'=>'required',
            'telepon'=>'required',
        ];
        $validator = Validator::make($request->all(), $val);
        if ($validator->fails()) {
            return json_encode(['status'=>false,'validation'=>$validator->errors()]);
        } else {
            return json_encode(['status'=>true]);
        }
    }

    public function validateQuestion(Request $request)
    {
        $val=[
            'pesan'=>'required',
        ];
        $validator = Validator::make($request->all(), $val);
        if ($validator->fails()) {
            return json_encode(['status'=>false,'validation'=>$validator->errors()]);
        } else {
            return json_encode(['status'=>true]);
        }
    }

    public function validateResponse(Request $request)
    {
        $val=[
            'tanggapan'=>'required',
        ];
        $validator = Validator::make($request->all(), $val);
        if ($validator->fails()) {
            return json_encode(['status'=>false,'validation'=>$validator->errors()]);
        } else {
            return json_encode(['status'=>true]);
        }
    }

    public function question()
    {
        if (Auth::User()->role=='admin') {
            $question=Question::orderBy('id', 'DESC')->get();
        } else {
            $question=Question::where('user_id', Auth::User()->id)->orderBy('id', 'DESC')->get();
        }
        $reqcount=Req::where('menu.skpd_id', Auth::User()->skpd_id)->where('request.status', 'pending')->where('request.sign_id', null)->join('menu', 'menu.id', '=', 'request.menu_id')->select('menu.*', 'request.*', 'menu.description as mdesc')->count();
        return view('dashboard.question.index', compact('question','reqcount'));
    }

    public function questionDetail($id)
    {
        try {
            $id=Crypt::decryptString($id);
            $question=Question::where('id', $id)->first();
            $reqcount=Req::where('menu.skpd_id', Auth::User()->skpd_id)->where('request.status', 'pending')->where('request.sign_id', null)->join('menu', 'menu.id', '=', 'request.menu_id')->select('menu.*', 'request.*', 'menu.description as mdesc')->count();
            return view('dashboard.question.detail', compact('question','reqcount'));
        } catch (DecryptException $e) {
            return redirect('/dashboard')->with('alert', json_encode(['status'=>'warning','data'=>'Kesalahan Sistem']));
        }
    }

    public function questionStore(Request $request)
    {
        // $question=Question::where('user_id',Auth::User()->id)->orderBy('id','DESC')->get();
        Question::create([
            'user_id'=>Auth::User()->id,
            'message'=>$request->pesan
        ]);
        return redirect('/question')->with('alert', json_encode(['status'=>'success','data'=>'Pesan Berhasil Dikirim']));
    }

    public function questionUpdate(Request $request, $id)
    {
        try {
            $id=Crypt::decryptString($id);
            Question::where('id', $id)->update([
                'response'=>$request->tanggapan
            ]);
            return redirect('/question')->with('alert', json_encode(['status'=>'success','data'=>'Data Berhasil Ubah']));
        } catch (DecryptException $e) {
            return redirect('/dashboard')->with('alert', json_encode(['status'=>'warning','data'=>'Kesalahan Sistem']));
        }
    }
    

    public function contact()
    {
        $data=json_decode(file_get_contents(base_path('resources/views/contact.json')));
        return view('dashboard.contact.index', ['data'=>$data]);
    }

    public function contactUpdate(Request $request)
    {
        $data=$request->except('_token');
        file_put_contents(base_path('resources/views/contact.json'), json_encode($data, true));
        return redirect('/contact/edit')->with('alert', json_encode(['status'=>'success','data'=>'Data Berhasil Diubah']));
    }
}
