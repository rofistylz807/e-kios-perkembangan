<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Validator;

class apiController extends Controller
{
    public function uploadBSRE(Request $request)
    {
        try {
          $validator = Validator::make($request->all(),[
              'file' => 'required',
              'passphrase' => 'required'
          ]);
          if ($validator->fails()) {
              return json_encode(['status'=>false,'validation'=>$validator->errors()]);
          }
          $ch = curl_init();
          $info = pathinfo($request->file);
          $mime = 'application/pdf';
          $name = $info['basename'];
          $outputfile = new \CURLFile($request->file, $mime, $name);
          $arraypost = [
            'file' => $outputfile,
            'nik' => '0803202100007062',
            'passphrase' => $request->passphrase,
            'tampilan' => 'invisible',
            'jenis_response' => 'BASE64'
          ];
          curl_setopt_array($ch, [
            CURLOPT_URL => "https://ds-dev.kukarkab.go.id/api/sign/pdf",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 1600,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $arraypost,
            CURLOPT_USERPWD => "anjab:25c44befb8194569a25dec4d488044c8",
            CURLOPT_SSL_CIPHER_LIST => 'DEFAULT@SECLEVEL=1',
          ]);
          $result = curl_exec($ch);
          $err = curl_error($ch);
          curl_close($ch);
          $result = json_decode($result, true);
          if ($err || isset($result['error'])) {
            $result = ['status' => false, 'error' => json_encode($err), 'error2' => json_encode($result['error'])];
            return response()->json($result);
          } else {
            $result['status'] = true;
            return response()->json($result);
          }
      } catch (Exception $error) {
        $result['status'] = true;
        $result['error'] = $error;
        return response()->json(json_decode($result));
      }
    }
}
