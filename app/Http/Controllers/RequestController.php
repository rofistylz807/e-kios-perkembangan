<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Validator;
use Session;
use App\Model\Menu;
use App\Model\Format;
use App\Model\Req;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class RequestController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $req=Req::where('request.user_id', Auth::User()->id)->join('menu', 'menu.id', '=', 'request.menu_id')->select('menu.*', 'request.*', 'menu.description as mdesc')->orderBy('request.id', 'DESC')->get();
        $reqcount=Req::where('menu.skpd_id', Auth::User()->skpd_id)->where('request.status', 'pending')->where('request.sign_id', null)->join('menu', 'menu.id', '=', 'request.menu_id')->select('menu.*', 'request.*', 'menu.description as mdesc')->count();
        return view('dashboard.request.index', compact('req','reqcount'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id=null, $data=null)
    {
        try {
            $data=Crypt::decryptString($data);
            $data=json_decode($data, true);
            $menu=Menu::where('id', $data['id'])->first();
            $form=Format::where('id', $menu->format_id)->first();
            if ($form) {
                $req=Req::where('id', $id)->first();
                $reqcount=Req::where('menu.skpd_id', Auth::User()->skpd_id)->where('request.status', 'pending')->where('request.sign_id', null)->join('menu', 'menu.id', '=', 'request.menu_id')->select('menu.*', 'request.*', 'menu.description as mdesc')->count();
                return view('dashboard.request.detail', ['data'=>$data['data'],'id'=>$data['id']], compact('menu', 'req', 'form','reqcount'));
            } else {
                return view('dashboard.error', ['error'=>'Data Format Tidak Tersedia']);
            }
        } catch (DecryptException $e) {
            return redirect('/request')->with('alert', json_encode(['status'=>'warning','data'=>'Kesalahan Sistem']));
        }
    }

    public function getFormValidate(Request $request)
    {
        $data=$request->all()['form'];
        $req=[];
        foreach ($data as $key => $value) {
            $req[$value['name']]=$value['value'];
        }
        $val=$request->all()['validate'];
        foreach ($val as $key => $value) {
            if ($value==null) {
                $val[$key]='';
            }
        }
        $validator = Validator::make($req, $val);
        if ($validator->fails()) {
            return json_encode(['status'=>false,'validation'=>$validator->errors()]);
        } else {
            return json_encode(['status'=>true]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $data=$request->except(['_token']);
            $idreq=Crypt::decryptString($data['x']);
            $id=Crypt::decryptString($id);
            $req=Req::where('id', $idreq)->first();
            if ($req->status=="pending") {
                $currentdata=json_decode(Crypt::decryptString($req->request), true)['data'];
                foreach ($currentdata as $key => $value) {
                    if ($request->hasFile($key)) {
                        if (file_exists(public_path('file/'.$req->id.'/'.$value))) {
                            unlink(public_path('file/'.$req->id.'/'.$value));
                        }
                        $resorce=$request->file($key);
                        $name_file=substr(md5(mt_rand()), 0, 5).'_'.$resorce->getClientOriginalName();
                        if (!file_exists(public_path('file/'.$req->id.'/'.$name_file))) {
                            $resorce->move('file/'.$req->id, $name_file);
                        }
                        $currentdata[$key]=$name_file;
                    } else {
                        $currentdata[$key]=$data[$key];
                    }
                }
                $data=[
                'id'=>$id,
                'data'=>$currentdata
            ];
                $data=Crypt::encryptString(json_encode($data));
                Req::where('id', $idreq)->update([
                'request'=>$data,
            ]);
                return redirect('/request')->with('alert', json_encode(['status'=>'success','data'=>'Permohonan Telah Diupdate']));
            } else {
                return redirect('/request')->with('alert', json_encode(['status'=>'warning','data'=>'Status Tidak Pending']));
            }
        } catch (DecryptException $e) {
            return redirect('/request')->with('alert', json_encode(['status'=>'warning','data'=>'Kesalahan Sistem']));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function pdf($id)
    {
        try {
            $id=Crypt::decryptString($id);
            return response()->file(public_path('pdf/'.$id.'.pdf'));
        } catch (DecryptException $e) {
            return redirect('/')->with('alert', json_encode(['status'=>'warning','data'=>'Data Tidak Ditemukan']));
        }
    }
}
