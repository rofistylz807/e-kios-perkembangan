<?php

namespace App\Http\Controllers;

use App\Survey;
use App\Model\Req;
use Illuminate\Http\Request;

class SurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        
        $survey=Survey::all();
        $tidaksesuai=survey::where('ans1', '1')->get();
        $kurangsesuai=Survey::where('ans1', '2')->get();
        $sesuai=Survey::where('ans1', '3')->get();
        $sangatsesuai=Survey::where('ans1', '4')->get();

        $tidakmudah=survey::where('ans2', '1')->get();
        $kurangmudah=Survey::where('ans2', '2')->get();
        $mudah=Survey::where('ans2', '3')->get();
        $sangatmudah=Survey::where('ans2', '4')->get();

        $tidakcepat=survey::where('ans3', '1')->get();
        $kurangcepat=Survey::where('ans3', '2')->get();
        $cepat=Survey::where('ans3', '3')->get();
        $sangatcepat=Survey::where('ans3', '4')->get();

        $sangatmahal=survey::where('ans4', '1')->get();
        $cukupmahal=Survey::where('ans4', '2')->get();
        $murah=Survey::where('ans4', '3')->get();
        $gratis=Survey::where('ans4', '4')->get();

        $tidaksesuailima=survey::where('ans5', '1')->get();
        $kurangsesuailima=Survey::where('ans5', '2')->get();
        $sesuailima=Survey::where('ans5', '3')->get();
        $sangatsesuailima=Survey::where('ans5', '4')->get();

        $tidakkompeten=survey::where('ans6', '1')->get();
        $kurangkompeten=Survey::where('ans6', '2')->get();
        $kompeten=Survey::where('ans6', '3')->get();
        $sangatkompeten=Survey::where('ans6', '4')->get();
        
        $tidaksopan=survey::where('ans7', '1')->get();
        $kurangsopan=Survey::where('ans7', '2')->get();
        $sopan=Survey::where('ans7', '3')->get();
        $sangatsopan=Survey::where('ans7', '4')->get();

        $buruk=survey::where('ans8', '1')->get();
        $cukup=Survey::where('ans8', '2')->get();
        $baik=Survey::where('ans8', '3')->get();
        $sangatbaik=Survey::where('ans8', '4')->get();

        $tidakada=survey::where('ans9', '1')->get();
        $ada=Survey::where('ans9', '2')->get();
        $berfungsi=Survey::where('ans9', '3')->get();
        $dikelola=Survey::where('ans9', '4')->get();

        $data=[
            'page_title'=>'DiSAPA I-DAMAN',
         
            'dataSurvey' => survey::orderBy('id', 'asc')->get(),
            'tidaksesuai'=>$tidaksesuai->count(),
            'kurangsesuai'=>$kurangsesuai->count(),
            'sesuai'=>$sesuai->count(),
            'sangatsesuai'=>$sangatsesuai->count(),

            'tidakmudah'=>$tidakmudah->count(),
            'kurangmudah'=>$kurangmudah->count(),
            'mudah'=>$mudah->count(),
            'sangatmudah'=>$sangatmudah->count(),

            'tidakcepat'=>$tidakcepat->count(),
            'kurangcepat'=>$kurangcepat->count(),
            'cepat'=>$cepat->count(),
            'sangatcepat'=>$sangatcepat->count(),

            'sangatmahal'=>$sangatmahal->count(),
            'cukupmahal'=>$cukupmahal->count(),
            'murah'=>$murah->count(),
            'gratis'=>$gratis->count(),

            'tidaksesuailima'=>$tidaksesuailima->count(),
            'kurangsesuailima'=>$kurangsesuailima->count(),
            'sesuailima'=>$sesuailima->count(),
            'sangatsesuailima'=>$sangatsesuailima->count(),

            'tidakkompeten'=>$tidakkompeten->count(),
            'kurangkompeten'=>$kurangkompeten->count(),
            'kompeten'=>$kompeten->count(),
            'sangatkompeten'=>$sangatkompeten->count(),

            'tidaksopan'=>$tidaksopan->count(),
            'kurangsopan'=>$kurangsopan->count(),
            'sopan'=>$sopan->count(),
            'sangatsopan'=>$sangatsopan->count(),

            'buruk'=>$buruk->count(),
            'cukup'=>$cukup->count(),
            'baik'=>$baik->count(),
            'sangatbaik'=>$sangatbaik->count(),

            'tidakada'=>$tidakada->count(),
            'ada'=>$ada->count(),
            'berfungsi'=>$berfungsi->count(),
            'dikelola'=>$dikelola->count(),
       
        ];
   
        return view('dashboard.survey.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data = [
            'page_title'   => 'DiSAPA I-DAMAN',
        ];
        return view('frontend.survey.create', $data);
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        // dd($request->all());
        $save = $request->validate([
            'jenis_kelamin'      => 'required',
            'pendidikan' => 'required',
            'pekerjaan' => 'required',
            'jenis_layanan' => 'required',
            'ans1' => 'required',
           'ans2' =>'required',
           'ans3' =>'required',
           'ans4' =>'required',
           'ans5' =>'required',
           'ans6' =>'required',
           'ans7' =>'required',
           'ans8' =>'required',
           'ans9' =>'required',
  
          ]);
        //  dd($request->all());
        survey::create($save);
        Req::where('id', $id)->update([
             'survey'=>'1'
         ]);
        return redirect('/request')->with('alert', json_encode(['status'=>'success','data'=>'Berhasil Kirim Survey, Terimakasih']));
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Survey  $survey
     * @return \Illuminate\Http\Response
     */
    public function show(Survey $survey)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Survey  $survey
     * @return \Illuminate\Http\Response
     */
    public function edit(Survey $survey)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Survey  $survey
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Survey $survey)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Survey  $survey
     * @return \Illuminate\Http\Response
     */
    public function destroy(Survey $survey)
    {
        //
    }
}
