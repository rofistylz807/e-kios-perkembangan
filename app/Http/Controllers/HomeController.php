<?php

namespace App\Http\Controllers;

use Auth;
use App\User;
use Validator;
use Session;
use App\Model\Menu;
use App\Model\Format;
use App\Model\Req;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function goToHome($id=null)
    {
        if ($id==null) {
            $menu=Menu::where('level', 1)->get();
        } else {
            $menu=Menu::where('menu_id', $id)->get();
        }
        return view('landingpage.index', compact('menu'));
    }

    public function goToForm($id =null)
    {
        $menu=Menu::where('id', $id)->first();
        $form=Format::where('id', $menu->format_id)->first();
        return view('landingpage.form', compact('menu', 'form'));
    }

    public function getForm(Request $request, $id=null)
    {
        $data=$request->except('_token');
        $req=Req::create([
            'menu_id'=>$id,
            'user_id'=>Auth::User()->id,
            'status'=>'pending'
        ]);
        foreach ($data as $key => $value) {
            if ($request->hasFile($key)) {
                $resorce=$request->file($key);
                $name_file=substr(md5(mt_rand()), 0, 5).'_'.$resorce->getClientOriginalName();
                if (!file_exists(public_path('file/'.$req->id.'/'.$name_file))) {
                    $resorce->move('file/'.$req->id, $name_file);
                }
                $data[$key]=$name_file;
            }
        }
        $data=[
            'id'=>$id,
            'data'=>$data
        ];
        $data=Crypt::encryptString(json_encode($data));
        $req=Req::where('id', $req->id)->update([
            'request'=>$data,
        ]);
        
        
        return redirect('/request')->with('alert', json_encode(['status'=>'success','data'=>'Permohonan Sedang Diperoses']));
    }

    public function getFormValidate(Request $request)
    {
        if (array_key_exists('validation', $request->all())) {
            $data=$request->all()['form'];
            $req=[];
            foreach ($data as $key => $value) {
                $req[$value['name']]=$value['value'];
            }
            $val=$request->all()['validation'];
            $validator = Validator::make($req, $val);
            if ($validator->fails()) {
                return json_encode(['status'=>false,'validation'=>$validator->errors()]);
            } else {
                return json_encode(['status'=>true]);
            }
        } else {
            return json_encode(['status'=>true]);
        }
    }

    public function contact()
    {
        $data=json_decode(file_get_contents(base_path('resources/views/contact.json')));
        return view('landingpage.contact', ['data'=>$data]);
    }

    public function login(Request $r)
    {
        $validator = Validator::make($r->all(), [
                'username' => 'required|exists:users,username',
                ]);
        if ($validator->fails()) {
            return redirect()->back()->with('alert', json_encode(['status'=>'warning','data'=>'Username Tidak Ditemukan']));
        }
        if (Auth::attempt(['username' => $r->username, 'password' => $r->password])) {
            return redirect()->intended('/dashboard');
        }
        return redirect()->back()->with('alert', json_encode(['status'=>'warning','data'=>'Password Salah']));
    }
}
