<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Model\Skpd;
use App\Model\Req;
use Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user=User::leftjoin('ref_skpd', 'ref_skpd.skpd_id', '=', 'users.skpd_id')->select('users.*', 'ref_skpd.*')->orderBy('users.id','DESC')->get();
        $reqcount=Req::where('menu.skpd_id', Auth::User()->skpd_id)->where('request.status', 'pending')->where('request.sign_id', null)->join('menu', 'menu.id', '=', 'request.menu_id')->select('menu.*', 'request.*', 'menu.description as mdesc')->count();
        return view('dashboard.user.index', compact('user','reqcount'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$data)
    {
        try {
            $data=Crypt::decryptString($data);
            $data=json_decode($data, true);
            if ($data['status']==1) {
                User::create([
                    'username'=>$request->username,
                    'nik'=>$request->nik,
                    'name'=>$request->nama,
                    'email'=>$request->email,
                    'phone'=>$request->nomor_hp,
                    'skpd_id'=>$request->skpd,
                    'role'=>$request->role,
                    'password'=>Hash::make($request->password)
                ]);
                return redirect('/user')->with('alert', json_encode(['status'=>'success','data'=>'Data Berhasil Ditambah']));
            } else {
                $arr=[
                    'username'=>$request->username,
                    'nik'=>$request->nik,
                    'name'=>$request->nama,
                    'email'=>$request->email,
                    'phone'=>$request->nomor_hp,
                    'skpd_id'=>$request->skpd,
                    'role'=>$request->role,
                ];
                if($request->password!=''){
                    $arr['password']=Hash::make($request->password);
                }
                User::where('id',$data['id'])->update($arr);
                return redirect('/user')->with('alert', json_encode(['status'=>'success','data'=>'Data Berhasil Diubah']));
            }
            
        } catch (DecryptException $e) {
            return redirect('/dashboard')->with('alert', json_encode(['status'=>'warning','data'=>'Kesalahan Sistem']));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('dashboard.account.setting');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($data)
    {
        $skpd=Skpd::all();
        try {
            $data=Crypt::decryptString($data);
            $data=json_decode($data, true);
            if ($data['status']==1) {
                return view('dashboard.user.show', ['skpd'=>$skpd]);
            } else {
                $user=User::where('id', $data['id'])->first();
                return view('dashboard.user.show', ['skpd'=>$skpd], compact('user'));
            }
        } catch (DecryptException $e) {
            return redirect('/dashboard')->with('alert', json_encode(['status'=>'warning','data'=>'Kesalahan Sistem']));
        }
    }

    public function validateForm(Request $request, $data)
    {
        try {
            $data=Crypt::decryptString($data);
            $data=json_decode($data, true);
            if($data['status']==1){
                $val=[
                    'username'=>'unique:users,username',
                    'nik'=>'min:16|unique:users,nik',
                    'nama'=>'required',
                    'email'=>'required|unique:users,email',
                    'nomor_hp'=>'required|min:10|unique:users,phone',
                    'role'=>'required',
                    'password'=>'required|min:6',
                ];
            }else{
                $val=[
                    'username'=>'unique:users,username,'.$data['id'],
                    'nik'=>'min:16|unique:users,nik,'.$data['id'],
                    'nama'=>'required',
                    'email'=>'required|unique:users,email,'.$data['id'],
                    'nomor_hp'=>'required|min:10|unique:users,phone,'.$data['id'],
                    'role'=>'required',
                ];
            }
            $validator = Validator::make($request->all(),$val);
            if ($validator->fails()) {
                return json_encode(['status'=>false,'validation'=>$validator->errors()]);
            } else {
                return json_encode(['status'=>true]);
            }
        } catch (DecryptException $e) {
            return redirect('/dashboard')->with('alert', json_encode(['status'=>'warning','data'=>'Kesalahan Sistem']));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $data=$request->except('_token');
        if ($data['x']==0) {
            $val=[
                'name'=>$data['nama'],
                'email'=>$data['email'],
                'phone'=>$data['nomor_hp'],
                'nik'=>$data['nik'],
            ];
            if(Auth::User()->role!='user'){
                $val['username']=$data['username'];
            }
            User::where('id', Auth::User()->id)->update($val);
            return redirect('/setting')->with('alert', json_encode(['status'=>'success','data'=>'Data Diri Sudah di Ubah']));
        } else {
            User::where('id', Auth::User()->id)->update([
                'password'=>Hash::make($data['password_baru']),
            ]);
            return redirect('/setting')->with('alert', json_encode(['status'=>'success','data'=>'Data Password Sudah di Ubah']));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($data)
    {
        try {
            $data=Crypt::decryptString($data);
            $data=json_decode($data, true);
            User::where('id',$data)->delete();
            Req::where('user_id',$data)->delete();
            return redirect('/user')->with('alert', json_encode(['status'=>'success','data'=>'Data sudah di Hapus']));
        } catch (DecryptException $e) {
            return redirect('/dashboard')->with('alert', json_encode(['status'=>'warning','data'=>'Kesalahan Sistem']));
        }
    }

    public function validateSetting(Request $request)
    {
        $id=Auth::User()->id;
        $data=$request->all();
        if ($request->x=='0') {
            $val=[
                'nama'=>'required',
                'email'=>'required|unique:users,email,'.$id,
                'nomor_hp'=>'required|min:10|unique:users,phone,'.$id,
                'nik'=>'required|unique:users,nik,'.$id,
            ];
            if(Auth::User()->role!='user'){
                $val['username']='required|unique:users,username,'.$id;
            }
            $validator = Validator::make($data,$val);
        } else {
            $validator = Validator::make($data, [
                'password_lama'=>'required|min:6',
                'password_baru'=>'required|min:6',
                'konfirmasi_password_baru'=>'required|min:6|same:password_baru'
            ]);
            if (!(Hash::check($data['password_lama'], Auth::user()->password))) {
                $validator->getMessageBag()->add('password_lama', 'Your current password does not matches with the password you provided. Please try again.');
            }
        }
        if (count(json_decode(json_encode($validator->errors()), true))) {
            return json_encode(['status'=>false,'validation'=>$validator->errors()]);
        } else {
            return json_encode(['status'=>true]);
        }
    }
}
