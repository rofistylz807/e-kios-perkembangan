<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Validator;
use Session;
use File;
use App\Model\Sign;
use App\Model\skpd;
use App\Model\Req;
use App\Model\Format;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Storage;
use QrCode;
use DB;

class FormatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id=null)
    {
        $format=Format::all();
        $reqcount=Req::where('menu.skpd_id', Auth::User()->skpd_id)->where('request.status', 'pending')->where('request.sign_id', null)->join('menu', 'menu.id', '=', 'request.menu_id')->select('menu.*', 'request.*', 'menu.description as mdesc')->count();
        return view('dashboard.format.index', compact('format','reqcount'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $js='synadmin/assets/js/format.js';
        return view('dashboard.format.create', ['js'=>$js]);
    }

    public function edit($id)
    {
        try {
            $id=Crypt::decryptString($id);
            $js='synadmin/assets/js/format.js';
            $format=Format::where('id', $id)->first();
            $format->document = str_replace('{{', '{', $format->document);
            $format->document = str_replace('}}', '}', $format->document);
            $format->document = str_replace(public_path('/'), url('/').'/', $format->document);
         
            return view('dashboard.format.edit', ['js'=>$js], compact('format'));
        } catch (DecryptException $e) {
            return redirect('formats')->with('alert', json_encode(['status'=>'warning','data'=>'Kesalahan Sistem']));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $req=$request->except('_token');
        // dd($req);
        $data=[];
        $sign=[];
        $data['form']=[];
        $data['validate']=[];
        $data['response']=[];
        $data['validate_response']=[];
        foreach ($req['request'] as $key => $value) {
            $value['option']=array_key_exists('option', $value)?$value['option']:'';
            if ($value['type']=='select') {
                foreach (explode("|", $value['option']) as $opt) {
                    $value['option'][$opt]=$opt;
                }
            }
            $form=[
                'name'=>$value['name'],
                'field'=>$value['type'],
                'option'=>$value['option'],
            ];
            if (array_key_exists('validate', $value)) {
                $val=$value['validate'];
                if ($val!="") {
                    $data['validate'][$value['variable']]=$val;
                }
            }
            $data['form'][$value['variable']]=$form;
        }

        foreach ($req['response'] as $key => $value) {
            $value['option']=array_key_exists('option', $value)?$value['option']:'';
            if ($value['type']=='select') {
                foreach (explode("|", $value['option']) as $opt) {
                    $value['option'][$opt]=$opt;
                }
            }
            if ($value['type']=='nip'||$value['type']=='name') {
                $sign[$value['option']][$value['type']]=$value['variable'];
            }
            $form=[
                'name'=>$value['name'],
                'field'=>$value['type'],
                'option'=>$value['option'],
            ];
            if (array_key_exists('validate', $value)) {
                $val=$value['validate'];
                if ($val!="") {
                    $data['validate_response'][$value['variable']]=$val;
                }
            }
            $data['response'][$value['variable']]=$form;
        }
        $format=Format::create([
            'name'=>$req['name'],
            'data'=>count($data)?json_encode($data):"",
            'sign'=>count($sign)?json_encode($sign):""
        ]);
        
        if (Session::has('image')) {
            foreach (json_decode(Session::get('image'), true) as $key => $value) {
                $exists = strpos($req['document'], $value);
                if ($exists == false) {
                    if (file_exists(public_path('image/'.Auth::User()->id.'/'.$value))) {
                        unlink(public_path('image/'.Auth::User()->id.'/'.$value));
                    }
                } else {
                    if (file_exists(public_path('image/'.Auth::User()->id.'/'.$value))) {
                        $path = public_path('format/'.$format->id);
                        if (!is_dir($path)) {
                            File::makeDirectory($path);
                        }
                        File::move(public_path('image/'.Auth::User()->id.'/'.$value), public_path('format/'.$format->id.'/'.$value));
                    }
                }
            }
            $req['document']=str_replace('image/'.Auth::User()->id, public_path('format/'.$format->id), $req['document']);
            $req['document']=str_replace('../', '', $req['document']) ;
            Session::forget('image');
        }


        $req['document'] = str_replace('{', '{{', $req['document']);
        $req['document'] = str_replace('}', '}}', $req['document']);

        Format::where('id', $format->id)->update([
            'document'=>$req['document']
        ]);

        return redirect('formats')->with('alert', json_encode(['status'=>'success','data'=>'Format Surat Berhasil ditambah']));
    }

    public function update(Request $request, $id)
    {
        try {
            $id=Crypt::decryptString($id);
            $req=$request->except('_token');
            $data=[];
            $sign=[];
            $data['form']=[];
            $data['validate']=[];
            $data['response']=[];
            $data['validate_response']=[];
            foreach ($req['request'] as $key => $value) {
                $value['option']=array_key_exists('option', $value)?$value['option']:'';
                if ($value['type']=='select') {
                    foreach (explode("|", $value['option']) as $opt) {
                        $value['option'][$opt]=$opt;
                    }
                }
                $form=[
                    'name'=>$value['name'],
                    'field'=>$value['type'],
                    'option'=>$value['option'],
                ];
                if (array_key_exists('validate', $value)) {
                    $val=$value['validate'];
                    if ($val!="") {
                        $data['validate'][$value['variable']]=$val;
                    }
                }
                $data['form'][$value['variable']]=$form;
            }
    
            foreach ($req['response'] as $key => $value) {
                $value['option']=array_key_exists('option', $value)?$value['option']:'';
                if ($value['type']=='select') {
                    foreach (explode("|", $value['option']) as $opt) {
                        $value['option'][$opt]=$opt;
                    }
                }
                if ($value['type']=='nip'||$value['type']=='name') {
                    $sign[$value['option']][$value['type']]=$value['variable'];
                }
                $form=[
                    'name'=>$value['name'],
                    'field'=>$value['type'],
                    'option'=>$value['option'],
                ];
                if (array_key_exists('validate', $value)) {
                    $val=$value['validate'];
                    if ($val!="") {
                        $data['validate_response'][$value['variable']]=$val;
                    }
                }
                $data['response'][$value['variable']]=$form;
            }
            
            if (Session::has('image')) {
                foreach (json_decode(Session::get('image'), true) as $key => $value) {
                    $exists = strpos($req['document'], $value);
                    if ($exists == false) {
                        if (file_exists(public_path('image/'.Auth::User()->id.'/'.$value))) {
                            unlink(public_path('image/'.Auth::User()->id.'/'.$value));
                        }
                    } else {
                        if (file_exists(public_path('image/'.Auth::User()->id.'/'.$value))) {
                            $path = public_path('format/'.$id);
                            if (!is_dir($path)) {
                                File::makeDirectory($path);
                            }
                            File::move(public_path('image/'.Auth::User()->id.'/'.$value), public_path('format/'.$id.'/'.$value));
                        }
                    }
                }
                $req['document']=str_replace('image/'.Auth::User()->id, public_path('format/'.$id), $req['document']);
                $req['document']=str_replace('../', '', $req['document']) ;
                Session::forget('image');
            }
    
    
            $req['document'] = str_replace('{', '{{', $req['document']);
            $req['document'] = str_replace('}', '}}', $req['document']);
            $req['document']=str_replace('src="format', 'src="'.public_path('format'), $req['document']);
            Format::where('id', $id)->update([
                'document'=>$req['document'],
                'name'=>$req['name'],
                'data'=>count($data)?json_encode($data):"",
                'sign'=>count($sign)?json_encode($sign):""
            ]);
    
            return redirect('formats')->with('alert', json_encode(['status'=>'success','data'=>'Format Surat Berhasil diubah']));
        } catch (DecryptException $e) {
            return redirect('formats')->with('alert', json_encode(['status'=>'warning','data'=>'Kesalahan Sistem']));
        }
    }

    public function upload(Request $request)
    {
        $resorce= $request->file('file');
        $name   = substr(md5(mt_rand()), 0, 5).'_'.$resorce->getClientOriginalName();
        $path = public_path('image/'.Auth::User()->id);
        if (!is_dir($path)) {
            File::makeDirectory($path);
        }
        $resorce->move($path, $name);
        if (Session::has('image')) {
            $ar=json_decode(Session::get('image'), true);
            array_push($ar, $name);
            Session::put('image', json_encode($ar));
        } else {
            $ar=[];
            array_push($ar, $name);
            Session::put('image', json_encode($ar));
        }
        return response()->json(['location'=>"/image/".Auth::User()->id."/".$name]);
    }


    public function addForm($code)
    {
        if ($code==1) {
            $code='request';
        } elseif ($code==2) {
            $code='response';
        }
        $id=substr(md5(rand()), 0, 16);

        return view('dashboard.format.add_form', ['id'=>$id,'code'=>$code]);
    }
    public function destroy($id)
    {
        try {
            $id=Crypt::decryptString($id);
            $format=Format::where('id', $id)->delete();
            File::deleteDirectory(public_path('format/'.$id));
            if ($format) {
                return redirect('/formats')->with('alert', json_encode(['status'=>'success','data'=>'Data sudah di Hapus']));
            }
        } catch (DecryptException $e) {
            return redirect('formats')->with('alert', json_encode(['status'=>'warning','data'=>'Kesalahan Sistem']));
        }
    }
}
