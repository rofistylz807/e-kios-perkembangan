<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Validator;
use Session;
use App\Model\Menu;
use App\Model\Format;
use App\Model\Http;
use App\Model\Req;
use App\Model\Sign;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use PDF;
use File;
use QrCode;
use Illuminate\Support\Facades\View;
use Illuminate\Contracts\Encryption\DecryptException;

class ResponseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd(public_path());
        $req=Req::where('menu.skpd_id', Auth::User()->skpd_id)->where('request.status', 'pending')->where('request.sign_id', null)->join('menu', 'menu.id', '=', 'request.menu_id')->select('menu.*', 'request.*', 'menu.description as mdesc')->orderBy('request.id', 'DESC')->get();
        $reqcount=Req::where('menu.skpd_id', Auth::User()->skpd_id)->where('request.status', 'pending')->where('request.sign_id', null)->join('menu', 'menu.id', '=', 'request.menu_id')->select('menu.*', 'request.*', 'menu.description as mdesc')->count();
        return view('dashboard.response.index', compact('req','reqcount'));
    }

    public function history()
    {
        $req=Req::where('menu.skpd_id', Auth::User()->skpd_id)->where('request.response', '!=', '')->join('menu', 'menu.id', '=', 'request.menu_id')->select('menu.*', 'request.*', 'menu.description as mdesc')->orderBy('request.id', 'DESC')->get();
        $reqcount=Req::where('menu.skpd_id', Auth::User()->skpd_id)->where('request.status', 'pending')->where('request.sign_id', null)->join('menu', 'menu.id', '=', 'request.menu_id')->select('menu.*', 'request.*', 'menu.description as mdesc')->count();
        return view('dashboard.response.index', compact('req','reqcount'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id=null, $data=null)
    {
        try {
            $data=Crypt::decryptString($data);
            $data=json_decode($data, true);
            $menu=Menu::where('id', $data['id'])->first();
            $form=Format::where('id', $menu->format_id)->first();
            $req=Req::where('id', $id)->first();
            $reqform=$req->response!=''?json_decode(Crypt::decryptString($req->response), true):[];
            $reqcount=Req::where('menu.skpd_id', Auth::User()->skpd_id)->where('request.status', 'pending')->where('request.sign_id', null)->join('menu', 'menu.id', '=', 'request.menu_id')->select('menu.*', 'request.*', 'menu.description as mdesc')->count();
            return view('dashboard.response.detail', ['data'=>$data['data'],'id'=>$data['id'],'reqform'=>$reqform], compact('menu', 'req', 'form','reqcount'));
        } catch (DecryptException $e) {
        }
    }

    public function getFormValidate(Request $request)
    {
        if (array_key_exists('validate_response', $request->all())) {
            $data=$request->all()['response'];
            $nip=[];
            foreach ($data as $key => $value) {
                $req[$value['name']]=$value['value'];
            }
            $validator = Validator::make($req, $request->all()['validate_response']);
            if ($validator->fails()) {
                return json_encode(['status'=>false,'validation'=>$validator->errors()]);
            } else {
                return json_encode(['status'=>true]);
            }
        } else {
            return json_encode(['status'=>true]);
        }
    }

    public function checkNIP(Request $request)
    {
        $res=Http::getNIP($request->nip);
        
        $front=Http::getTitle($request->nip, 0);
        $back=Http::getTitle($request->nip, 1);
        if ($front['status_code']=="200") {
            $res['data']['nama']=$front['data']['gelar_depan'].'. '.$res['data']['nama'];
        }
        if ($back['status_code']=="200") {
            $res['data']['nama']=$res['data']['nama'].', '.$back['data']['gelar_belakang'];
        }
        return json_encode($res);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=$request->except(['_token','status','description']);
        $req=Req::where('id', $id)->first();
        $format=Format::where('menu.id', $req->menu_id)->join('menu', 'menu.format_id', '=', 'format.id')->join('ref_skpd', 'ref_skpd.skpd_id', '=', 'menu.skpd_id')->select('format.*', 'ref_skpd.img', 'menu.skpd_id', 'ref_skpd.skpd_name')->first();
        // dd($format);
        $re=Crypt::decryptString($req->request);
        $re=json_decode($re, true);
        $res=$data;
        $merge=array_merge($re['data'], $res);
        $qr=QrCode::format('svg')->size(140)->generate(url('pdf/'.Crypt::encryptString($id)));
        $codeqr='<img style="margin-top: -145px; margin-left: 150px;" src="data:image/svg+xml;base64,'.base64_encode($qr).'">';
        File::put(resource_path().'/views/landingpage/documents/'.$req->menu_id.'.blade.php', $format->document);
        $file=view('landingpage.documents.'.$req->menu_id, ['data'=>$merge,'id'=>$re['id']])->render();
        $file=str_replace('../../format',public_path('format'),$file);
        $html='<img src="'.public_path('/img/').$format->img.'" width="100%"><br>'.$file.$codeqr.'<img src="'.public_path("img/footer-bsre.jpeg").'" style="position: fixed; 
        bottom: -40px; 
        left: 0px; 
        right: 0px;
        height: 50px; " width="100%">';
        // dd($html);
        $pdf = PDF::loadHTML($html);
        // return $pdf->stream();
        File::put(public_path().'/pdf/'.$id.'.pdf', $pdf->output());
        $data=Crypt::encryptString(json_encode($data));
        if ($request->status=='reject') {
            $desc=$request->description;
        } else {
            if ($request->description==null) {
                $desc="menunggu Tandatangan dari ".$format->skpd_name;
            } else {
                $desc=$request->description;
            }
        }
        $sign=Sign::where('menu_id', $req->menu_id)->first();
        $res=[
            'response'=>$data,
            'status'=>$request->status,
            'description'=>$desc,
            'sign_id'=>$sign?$sign->skpd_id:NULL,
        ];
        Req::where('id', $id)->update($res);
        
        return redirect('/response')->with('alert', json_encode(['status'=>'success','data'=>'Permohonan Sedang Telah Diupdate']));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
