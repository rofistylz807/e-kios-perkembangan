<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $table = 'menu';
    protected $primaryKey = 'id';
    protected $fillable = [
        'title',
        'img',
        'description',
        'url',
        'level',
        'menu_id',
        'skpd_id',
        'format_id'
    ];
}
