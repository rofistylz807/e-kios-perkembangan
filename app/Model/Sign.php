<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Sign extends Model
{
    protected $table = 'request_sign';
    protected $primaryKey = 'id';
    protected $fillable = [
        'menu_id',
        'skpd_id',
        'keyword'
    ];
}
