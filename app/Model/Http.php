<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Http extends Model
{
    protected function post($url, $data)
    {
        try {
            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => $data
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            $response=json_decode($response,true);
            return is_array($response)?$response['content'][0]:null;
        } catch (RequestException $e) {
            return null;
        }
    }

    protected function postsign($url, $data)
    {
        try {
            $curl = curl_init();
            $user='anjab';
            $pass='25c44befb8194569a25dec4d488044c8';
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'POST',
                CURLOPT_POSTFIELDS => $data,
                CURLOPT_USERPWD =>$user.":".$pass,
                CURLOPT_SSL_CIPHER_LIST => 'DEFAULT@SECLEVEL=1',
            ));

            $response = curl_exec($curl);

            curl_close($curl);
            $response=json_decode($response,true);
            return is_array($response)?$response:null;
        } catch (RequestException $e) {
            return null;
        }
    }

    protected function getNIP($nik)
    {
        try {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'http://api-simpeg.kukarkab.go.id/api/asn/biodata/'.$nik,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
                  'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiZWJhZmEzNTkxYjg1YzU3NDNjZmQ5YzAyNmNhNGM1ZGUwNzQ5NzJlNWJlMDY5M2Y0MWU5OTFhYzc5ODQxNDM4ZmNlMDhkNGU3MTQyODQ2YzIiLCJpYXQiOjE2MjgwNDAzOTgsIm5iZiI6MTYyODA0MDM5OCwiZXhwIjoxNjU5NTc2Mzk4LCJzdWIiOiIxMSIsInNjb3BlcyI6W119.F9JAtRbJXiTE8heq9obcsbtQiDxKEg-aDQaThOrEddpIjU8TPqeDdYgX5xG0-9BYNekXX7iIVdG8WGOAKnAzONXkjf9PtAeoYyltbW17JlbY2uFzTFDGQB4TJQXusn1--qZAnJGJlsfQxlB9IdGwreML6oXi_asSeYFRDdW7d1_qrH-ouUX9v6pNK0HYA-5kqxjo9kFfEsQx23eFl9SogL_Z4VNZy7GKMra90eEXcA7qdTABwn_kYFgw5EXLmahkkwhFPfCVXcZ4lVwiyOFxWtlq6mzarnCsFTBR8Gin2zRRxs5no-E1bvzggW7rhCkDUEcTxKQc9Bxl-yZIaRA0wd3yOS85TpeWb__aUKLBlCMQtV02NeVX7EgmvqJYWf1egNTtdG4hbXczozHUHbggLV5DiPyHpr29Acv8t7LH4cASaWB6a2cCP04vnsozSzrLsdjg8suFjxeZrpfgMnKfwFiusI47lErAb2FEf4uz0uiP0NCun_FdFdDjuWU5r-En0Ff2prrvtjbrzBoydYVBuHrYKbKlD0SCFlg5ALTkI87fGaVcMyKoYI-ECT0saax3QeQlaKHDckU84qb6K-3HcexktryQNrDsCxH8Y1Uj6kkmAr8EiKC-v5BBMdXNq0nBAMQ6QMdWwRQkx6MNcY7EGORDB6X7GRxnEprAqLoGUWo',
                ),
              ));

            $response = curl_exec($curl);

            curl_close($curl);
            $response=json_decode($response,true);
            return is_array($response)?$response:[
                'message'=>"Server Error",
                'status_code'=>"404"
            ];
        } catch (RequestException $e) {
            return [
                'message'=>"Server Error",
                'status_code'=>"404"
            ];
        }
    }
    protected function getTitle($nik,$status)
    {
        try {
            $curl = curl_init();
            $url=$status==0?'http://api-simpeg.kukarkab.go.id/api/asn/gelar_depan/'.$nik:'http://api-simpeg.kukarkab.go.id/api/asn/gelar_belakang/'.$nik;
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
                  'Authorization: Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiZWJhZmEzNTkxYjg1YzU3NDNjZmQ5YzAyNmNhNGM1ZGUwNzQ5NzJlNWJlMDY5M2Y0MWU5OTFhYzc5ODQxNDM4ZmNlMDhkNGU3MTQyODQ2YzIiLCJpYXQiOjE2MjgwNDAzOTgsIm5iZiI6MTYyODA0MDM5OCwiZXhwIjoxNjU5NTc2Mzk4LCJzdWIiOiIxMSIsInNjb3BlcyI6W119.F9JAtRbJXiTE8heq9obcsbtQiDxKEg-aDQaThOrEddpIjU8TPqeDdYgX5xG0-9BYNekXX7iIVdG8WGOAKnAzONXkjf9PtAeoYyltbW17JlbY2uFzTFDGQB4TJQXusn1--qZAnJGJlsfQxlB9IdGwreML6oXi_asSeYFRDdW7d1_qrH-ouUX9v6pNK0HYA-5kqxjo9kFfEsQx23eFl9SogL_Z4VNZy7GKMra90eEXcA7qdTABwn_kYFgw5EXLmahkkwhFPfCVXcZ4lVwiyOFxWtlq6mzarnCsFTBR8Gin2zRRxs5no-E1bvzggW7rhCkDUEcTxKQc9Bxl-yZIaRA0wd3yOS85TpeWb__aUKLBlCMQtV02NeVX7EgmvqJYWf1egNTtdG4hbXczozHUHbggLV5DiPyHpr29Acv8t7LH4cASaWB6a2cCP04vnsozSzrLsdjg8suFjxeZrpfgMnKfwFiusI47lErAb2FEf4uz0uiP0NCun_FdFdDjuWU5r-En0Ff2prrvtjbrzBoydYVBuHrYKbKlD0SCFlg5ALTkI87fGaVcMyKoYI-ECT0saax3QeQlaKHDckU84qb6K-3HcexktryQNrDsCxH8Y1Uj6kkmAr8EiKC-v5BBMdXNq0nBAMQ6QMdWwRQkx6MNcY7EGORDB6X7GRxnEprAqLoGUWo',
                ),
              ));

            $response = curl_exec($curl);

            curl_close($curl);
            $response=json_decode($response,true);
            return is_array($response)?$response:[
                'message'=>"Server Error",
                'status_code'=>"404"
            ];
        } catch (RequestException $e) {
            return [
                'message'=>"Server Error",
                'status_code'=>"404"
            ];
        }
    }
}
