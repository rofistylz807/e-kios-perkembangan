<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Skpd extends Model
{
    protected $table = 'ref_skpd';
    protected $primaryKey = 'skpd_id';
    protected $fillable = [
        'skpd_name',
        'skpd_address',
        'skpd_phone',
        'skpd_email',
        'skpd_fax',
        'skpd_postal_code',
        'skpd_sub_district',
        'skpd_head_name',
        'skpd_head_nip',
        'skpd_head_position',
        'skpd_head_level',
        'img'
    ];
}
