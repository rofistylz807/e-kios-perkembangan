<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $table = 'log';
    protected $primaryKey = 'id';
    protected $fillable = [
        'request_id',
        'skpd_id',
        'response',
        'message',
        'status',
    ];
}
