<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Req extends Model
{
    protected $table = 'request';
    protected $primaryKey = 'id';
    protected $fillable = [
        'menu_id',
        'user_id',
        'request',
        'status',
        'response',
        'description',
        'sign_id',
        'sign_count',
        'survey'
    ];

    public function sign() {
        return $this->hasMany(Sign::class,'menu_id','menu_id');
    }
}
