<?php

namespace App\Model;
use Mail;

use Illuminate\Database\Eloquent\Model;

class Mails extends Model
{
    protected function sendEmail($data)
    {
        Mail::send($data['path'], $data['data'], function ($message) use ($data)
        {
            $message->subject($data['subject']);
            $message->from('noreplay@ekios.kukarkab.go.id', $data['emailfrom']);
            $message->to($data['emailto']);
        });
    }
}
