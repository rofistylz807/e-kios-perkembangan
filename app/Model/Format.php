<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Format extends Model
{
    protected $table = 'format';
    protected $primaryKey = 'id';
    protected $fillable = [
        'name',
        'data',
        'document',
        'sign',
    ];
}
