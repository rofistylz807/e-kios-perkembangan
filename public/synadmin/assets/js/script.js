$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
var base_url = window.location.origin + '/';

$('#datatable').DataTable({
    "order": [],
});

function del(title, url) {
    url = base_url + url;
    $('.del-title').text(title);
    $('.del-url').attr('href', url);
}

function checknip(data) {
    var nip=$(data).val();
    var name=$(data).attr('name');
    var name_id=$(data).attr('data');
    if(nip.length>15){
        $.ajax({
            url: base_url + 'response/checknip',
            data: {
                nip: nip,
                name: name
            },
            type: 'post',
            success: function (res) {
                res = JSON.parse(res);
                if(res.status_code == "200"){
                    $('#'+name).text('');
                    $('#'+name_id).val(res.data.nama);
                }else{
                    $('#'+name).text(res.message);    
                    $('#'+name_id).val('');                
                }    
            }
        });
    }else{
        if(nip.length>0){
            $('#'+name).text("Data tidak ditemukan");    
            $('#'+name_id).val('');
        }else{
            $('#'+name).text('');
            $('#'+name_id).val('');
        }
    }
    
}
$(document).ready(function () {
    $("#sign-1").click(function () {
        $("#sign-1").hide();
        $("#loader").show(); 
    });
});

function detail(url, place_id) {
    $.ajax({
        url: base_url + url,
        type: 'get',
        success: function (res) {
            $('.' + place_id).html(res);
        }
    });
}

function detailSurvey(url) {
    $('.survey-form').attr('action',url);
}

function validateForm(validation) {
    $('.alert').text('');
    var formData = $('form').serializeArray();
    validation = JSON.parse(validation);
    var ret = false;
    $.ajax({
        url: base_url + 'response/validate',
        async: false,
        data: {
            response: formData,
            validate_response: validation
        },
        type: 'post',
        success: function (res) {
            res = JSON.parse(res);
            var validator = res.validation;
            $.each(validator, function (key, val) {
                $('#' + key).text(val);
            });
            ret = res.status;

        }
    });
    return ret;
}

function validateFormReq(validation) {
    $('.alert').text('');
    var formData = $('form').serializeArray();
    validation = JSON.parse(validation);
    var ret = false;
    $.ajax({
        url: base_url + 'request/validate',
        async: false,
        data: {
            form: formData,
            validate: validation
        },
        type: 'post',
        success: function (res) {
            res = JSON.parse(res);
            var validator = res.validation;
            $.each(validator, function (key, val) {
                $('#' + key).text(val);
            });
            ret = res.status;

        }
    });
    return ret;
}

function validateRegister() {
    $('.alert').text('');
    var formData = $('form').serializeArray();
    var ret = false;
    $.ajax({
        url: base_url + 'register/validate',
        async: false,
        data: formData,
        type: 'post',
        success: function (res) {
            res = JSON.parse(res);
            var validator = res.validation;
            $.each(validator, function (key, val) {
                $('#' + key).text(val);
            });
            ret = res.status;

        }
    });
    return ret;
}

function validate(url) {
    $('.alert').text('');
    var formData = $('form').serializeArray();
    var ret = false;
    $.ajax({
        url: base_url + url,
        async: false,
        data: formData,
        type: 'post',
        success: function (res) {
            res = JSON.parse(res);
            var validator = res.validation;
            $.each(validator, function (key, val) {
                $('#' + key).text(val);
            });
            ret = res.status;

        }
    });
    return ret;
}

function validationSetting(key) {
    $('.alert').text('');
    var formData = $('#' + key).serializeArray();
    var ret = false;
    $.ajax({
        url: base_url + 'setting/validate',
        async: false,
        data: formData,
        type: 'post',
        success: function (res) {
            res = JSON.parse(res);
            var validator = res.validation;
            console.log(validator);
            $.each(validator, function (key, val) {
                $('#' + key).text(val);
            });
            ret = res.status;

        }
    });
    return ret;
}

function cls(id) {
    $("." + id).html('');
}

function clsdel() {
    $(".del-title").html('');
    $('.del-url').attr('href', '');
}
