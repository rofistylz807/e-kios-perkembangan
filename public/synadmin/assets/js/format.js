
$(document).ready(function () {
    tinymce.init({
        selector: ".textarea",
        theme: "modern",
        paste_data_images: true,
        plugins: [
            "advlist autolink lists link image charmap print preview hr anchor pagebreak",
            "searchreplace wordcount visualblocks visualchars code fullscreen",
            "insertdatetime media nonbreaking save table contextmenu directionality",
            "emoticons template paste textcolor colorpicker textpattern"
        ],
        toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
        toolbar2: "print preview media | forecolor backcolor emoticons",
        // image_advtab: true,
        automatic_uploads: true,
        images_upload_url: 'upload',
        file_picker_types: 'image',
        file_picker_callback: function (cb, value, meta) {
            var input = document.createElement('input');
            input.setAttribute('type', 'file');
            input.setAttribute('accept', 'image/*');
            input.onchange = function () {
                var file = this.files[0];
                var reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = function () {
                    var id = 'blobid' + (new Date()).getTime();
                    var blobCache = tinymce.activeEditor.editorUpload.blobCache;
                    var base64 = reader.result.split(',')[1];
                    var blobInfo = blobCache.create(id, file, base64);
                    blobCache.add(blobInfo);
                    cb(blobInfo.blobUri(), {
                        title: file.name
                    });
                };
            };
            input.click();
        },
        templates: [{
            title: 'Test template 1',
            content: 'Test 1'
        }, {
            title: 'Test template 2',
            content: 'Test 2'
        }]
    });
});

function addform(id, code) {
    $.ajax({
        url: base_url + 'formats/form/add/' + code,
        type: 'get',
        success: function (res) {
            $('#' + id).append(res);
        }
    });
}

function delform(id) {
    $('#' + id).remove();
}

function selectOpt(e,id) {
    val=$(e).val();
    if(val=='select'||val=='name'||val=='nip'){
        $("#option_"+id).prop('disabled', false);
        $("#option_"+id).prop('required', true);
    }else{
        $("#option_"+id).prop('disabled', true);
        $("#option_"+id).prop('required', false);
        $("#option_"+id).val('');
    }

    if(val=='file'){
        $("#validate_"+id).prop('disabled', true);
        $("#validate_"+id).val('');
    }else{
        $("#validate_"+id).prop('disabled', false);
    }
}

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
var base_url = window.location.origin + '/';
