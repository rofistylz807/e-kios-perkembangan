$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
var base_url = window.location.origin +'/';


function validateForm(validation) {
    $('.alert').text('');
    var formData = $('form').serializeArray();
    validation = JSON.parse(validation);
    var ret=false;
    $.ajax({
        url: base_url + 'validate',
        async: false,
        data: {
            form: formData,
            validation: validation
        },
        type: 'post',
        success: function (res) {
            res = JSON.parse(res);
            var validator = res.validation;
            $.each(validator, function (key, val) {
                $('#'+ key).text(val);
            });
            ret=res.status;

        }
    });
    return ret;
}

function validateUrl(url) {
    console.log(url);
    $('.alert').text('');
    var formData = $('form').serializeArray();
    var ret=false;
    $.ajax({
        url: base_url + url,
        async: false,
        data: formData,
        type: 'post',
        success: function (res) {
            res = JSON.parse(res);
            var validator = res.validation;
            $.each(validator, function (key, val) {
                $('#'+ key).text(val);
            });
            ret=res.status;

        }
    });
    return ret;
}

function closePrint() {
    document.body.removeChild(this.__container__);
}

function setPrint() {
    this.contentWindow.__container__ = this;
    this.contentWindow.onbeforeunload = closePrint;
    this.contentWindow.onafterprint = closePrint;
    this.contentWindow.focus(); // Required for IE
    this.contentWindow.print();
}

function printPage(sURL) {
    var oHiddFrame = document.createElement("iframe");
    oHiddFrame.onload = setPrint;
    oHiddFrame.style.position = "fixed";
    oHiddFrame.style.right = "0";
    oHiddFrame.style.bottom = "0";
    oHiddFrame.style.width = "0";
    oHiddFrame.style.height = "0";
    oHiddFrame.style.border = "0";
    oHiddFrame.src = sURL;
    document.body.appendChild(oHiddFrame);
}
